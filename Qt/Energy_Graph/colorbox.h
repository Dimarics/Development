#ifndef COLORBOX_H
#define COLORBOX_H

#include <QComboBox>

class ColorBox : public QComboBox
{
    Q_OBJECT
public:
    ColorBox(QWidget* = 0);
    void addColor(const QColor&, const QString&);
    QColor color(int = -1);
    int colorIndex(const QColor&);
};

#endif // COLORBOX_H
