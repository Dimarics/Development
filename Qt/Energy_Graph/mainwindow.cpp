#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
//#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    period = ui->period->value() * 1000;
    ui->stopButton->close();

    ui->colorBox->addColor(QColor(Qt::red), "красный");
    ui->colorBox->addColor(QColorConstants::Svg::orange, "оранжевый");
    ui->colorBox->addColor(QColor(0, 200, 50), "зелёный");
    ui->colorBox->addColor(QColor(Qt::cyan), "голубой");
    ui->colorBox->addColor(QColor(Qt::blue), "синий");
    ui->colorBox->addColor(QColor(QColorConstants::Svg::darkviolet), "фиолетовый");
    ui->colorBox->addColor(QColor(Qt::black), "чёрный");
    connect(ui->colorBox, &QComboBox::currentIndexChanged, this, &MainWindow::setColor);

    drawGraphics[0] = ui->voltageGraph;
    ui->voltageGraph->header = "Напряжение";
    ui->voltageGraph->plotArea->demens = "B";
    ui->voltageGraph->plotArea->color = ui->colorBox->color(5);

    drawGraphics[1] = ui->amperageGraph;
    ui->amperageGraph->header = "Сила тока";
    ui->amperageGraph->plotArea->demens = "A";
    ui->amperageGraph->plotArea->color = ui->colorBox->color(0);

    drawGraphics[2] = ui->powerGraph;
    ui->powerGraph->header = "Мощность";
    ui->powerGraph->plotArea->demens = "Вт";
    ui->powerGraph->plotArea->color = ui->colorBox->color(6);

    connect(ui->fullScreen, &QAction::toggled, this, &MainWindow::fullScreen);
    connect(ui->settingsView, &QAction::toggled, this, &MainWindow::settingsView);
    connect(ui->open, &QAction::triggered, this, &MainWindow::open);
    connect(ui->saveAs, &QAction::triggered, this, &MainWindow::saveAs);
    connect(ui->saveConfig, &QAction::triggered, this, &MainWindow::saveConfig);

    connect(ui->chartBox, &QComboBox::currentIndexChanged, this, &MainWindow::chartChanged);
    connect(ui->timeStep, &QTimeEdit::timeChanged, this, &MainWindow::setTimeStep);
    connect(ui->period, &QSpinBox::valueChanged, this, &MainWindow::setPeriod);
    connect(ui->stretchX, &QSpinBox::valueChanged, this, &MainWindow::setStretch);
    connect(ui->minY, &QSpinBox::valueChanged, this, &MainWindow::setMinY);
    connect(ui->maxY, &QSpinBox::valueChanged, this, &MainWindow::setMaxY);
    connect(ui->countY, &QSpinBox::valueChanged, this, &MainWindow::setCountY);
    connect(ui->startButton, &QPushButton::clicked, this, &MainWindow::start);
    connect(ui->scrollBegin, &QPushButton::clicked, this, &MainWindow::scrollBegin);
    connect(ui->scrollEnd, &QPushButton::clicked, this, &MainWindow::scrollEnd);
    connect(ui->stopButton, &QPushButton::clicked, this, &MainWindow::stop);

    connect(ui->voltageAction, &QAction::toggled, this, &MainWindow::hideVoltage);
    connect(ui->amperageAction, &QAction::toggled, this, &MainWindow::hideAmperage);
    connect(ui->powerAction, &QAction::toggled, this, &MainWindow::hidePower);

    checkPorts();
    connect(ui->portBox, &PortBox::menuOpened, this, &MainWindow::checkPorts);
    serial_port = new QSerialPort(this);
    connect(serial_port, &QSerialPort::readyRead, this, &MainWindow::readPort);
    connect(ui->portBox, &QComboBox::currentTextChanged, this, &MainWindow::portChanged);

    QByteArray splitter_save = (ui->splitter->saveState());
    setConfig();
    chartChanged(0);
    for (DrawGraphics *chart : drawGraphics)
        chart->scrollBar->close();
    ui->splitter->restoreState(splitter_save);
    showMaximized();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::setConfig()
{
    QFile file("config");
    if (file.open(QIODevice::ReadOnly))
    {
        QByteArrayList settings = file.readLine().split(',');
        ui->period->setValue(settings.at(0).toInt());
        ui->settingsView->setChecked(settings.at(1).toInt());
        ui->voltageAction->setChecked(settings.at(2).toInt());
        ui->amperageAction->setChecked(settings.at(3).toInt());
        ui->powerAction->setChecked(settings.at(4).toInt());

        if (!bool(settings.at(1).toInt())) ui->settings->close();
        for (DrawGraphics *chart : drawGraphics)
        {
            QByteArrayList config = file.readLine().split(',');
            chart->plotArea->color = ui->colorBox->color(config.at(0).toInt());
            chart->plotArea->time_step = QTime(config.at(1).toInt(), config.at(2).toInt());
            chart->plotArea->stretch_x = config.at(3).toInt();
            chart->plotArea->min_y = config.at(4).toInt();
            chart->plotArea->max_y = config.at(5).toInt();
            chart->count_y = config.at(6).toInt();
            chart->updateView();
        }
        file.close();
    }
}

void MainWindow::saveConfig()
{
    QFile file("config");
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        out << ui->period->value() << ','
            << ui->settings->isVisible() << ','
            << ui->voltageGraph->isVisible() << ','
            << ui->amperageGraph->isVisible() << ','
            << ui->powerGraph->isVisible() << ',';
        for (DrawGraphics *chart : drawGraphics)
        {
            out << '\n'
                << ui->colorBox->colorIndex(chart->plotArea->color) << ','
                << chart->plotArea->time_step.hour() << ','
                << chart->plotArea->time_step.minute() << ','
                << chart->plotArea->stretch_x << ','
                << chart->plotArea->min_y << ','
                << chart->plotArea->max_y << ','
                << chart->count_y << ',';
        }
        file.close();
    }
}

void MainWindow::open()
{
    QFile file(QFileDialog::getOpenFileName(this, "Открыть файл", "/", "Диагаммы (*.ch)"));
    if (file.open(QIODevice::ReadOnly))
    {
        ui->stopButton->show();
        for (DrawGraphics *chart : drawGraphics)
        {
            QByteArray read = file.readLine();
            chart->plotArea->points.clear();
            while (read != "#\n" && file.canReadLine())
            {
                QByteArrayList data = read.split(',');
                chart->plotArea->addPoint(data.at(0).toFloat(), data.at(1).toFloat());
                read = file.readLine();
            }
            chart->updateView();
        }
    }
}

void MainWindow::saveAs()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    QFile file(dialog.getSaveFileName(this, "Сохранить файл", "/", "Диагаммы (*.ch)"));
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        for (DrawGraphics *chart : drawGraphics)
        {
            for(QPointF &point : chart->plotArea->points)
                out << point.y() << ',' << point.x() << '\n';
            out << "#\n";
        }
        file.close();
    }
}

void MainWindow::fullScreen(bool full_screen)
{
    static bool maximized = true;
    if (full_screen)
    {
        maximized = isMaximized();
        showFullScreen();
    }
    else
        maximized ? showMaximized() : showNormal();
}

void MainWindow::settingsView(bool visible)
{
    if (visible) ui->settings->show();
    else ui->settings->close();
}

void MainWindow::portChanged(const QString &port_name)
{
    quint8 i = 0;
    serial_port->close();
    while (port_list.at(i).portName() != port_name) ++i;
    serial_port->setPort(port_list.at(i));
    qDebug() << serial_port->open(QIODeviceBase::ReadOnly);
}

void MainWindow::checkPorts()
{
    ui->portBox->blockSignals(true);
    ui->portBox->clear();
    port_list = QSerialPortInfo::availablePorts();
    for (QSerialPortInfo &port_info : port_list)
        ui->portBox->addItem(port_info.portName());
    ui->portBox->blockSignals(false);
}

void MainWindow::readPort()
{
    static QVector<qreal> voltage_list;
    static QVector<qreal> amperage_list;
    static uint delay = period;
    uint current_time = QTime::currentTime().msecsSinceStartOfDay();
    QByteArrayList data = serial_port->readLine().split(',');

    voltage_list << data.at(0).toFloat();
    amperage_list << data.at(1).toFloat();
    if (current_time >= delay)
    {
        qreal voltage = average(voltage_list);
        qreal amperage = average(amperage_list);
        qreal power = voltage * amperage;

        voltage_list.clear();
        amperage_list.clear();
        ui->voltageGraph->plotArea->addPoint(voltage, time_point);
        ui->voltageGraph->updateView();
        ui->amperageGraph->plotArea->addPoint(amperage, time_point);
        ui->amperageGraph->updateView();
        ui->powerGraph->plotArea->addPoint(power, time_point);
        ui->powerGraph->updateView();
        delay = current_time + period;
        time_point += period / 1000;
    }
}

qreal MainWindow::average(const QVector<qreal> &args)
{
    qreal result = 0;
    for (qreal value : args) result += value;
    return result / args.size();
}

void MainWindow::start()
{
    if (!ui->stopButton->isChecked())
    {
        if (!ui->portBox->currentText().isEmpty())
        {
            quint8 i = 0;
            while (port_list.at(i).portName() != ui->portBox->currentText()) ++i;
            serial_port->setPort(port_list.at(i));
            if(serial_port->open(QIODeviceBase::ReadOnly))
                ui->stopButton->close();
            else
                ui->startButton->setChecked(false);
        }
        else
            ui->startButton->setChecked(false);
    }
    else
    {
        serial_port->close();
        ui->stopButton->show();
    }
}

void MainWindow::scrollBegin()
{
    for (DrawGraphics *chart : drawGraphics)
    {
        chart->scrollBar->setValue(0);
        chart->scrollGraphic(0);
    }
}

void MainWindow::scrollEnd()
{
    for (DrawGraphics *chart : drawGraphics)
    {
        chart->scrollBar->setValue(chart->scrollBar->maximum());
        chart->scrollGraphic(chart->scrollBar->maximum());
    }
}

void MainWindow::stop()
{
    time_point = 0;
    ui->stopButton->close();
    for (DrawGraphics *chart : drawGraphics)
    {
        chart->plotArea->points.clear();
        chart->plotArea->tool_tips.clear();
        chart->updateView();
    }
}

void MainWindow::chartChanged(quint8 index)
{
    ui->colorBox->setCurrentIndex(ui->colorBox->colorIndex(drawGraphics[index]->plotArea->color));
    ui->timeStep->setTime(drawGraphics[index]->plotArea->time_step);
    ui->stretchX->setValue((drawGraphics[index]->plotArea->stretch_x - 30) / 10);
    ui->minY->setValue(drawGraphics[index]->plotArea->min_y);
    ui->maxY->setValue(drawGraphics[index]->plotArea->max_y);
    ui->countY->setValue(drawGraphics[index]->count_y);
}

void MainWindow::setColor()
{
    drawGraphics[ui->chartBox->currentIndex()]->setColor(ui->colorBox->color(ui->colorBox->currentIndex()));
    drawGraphics[ui->chartBox->currentIndex()]->updateView();
}

void MainWindow::setTimeStep(QTime time_step)
{
    drawGraphics[ui->chartBox->currentIndex()]->plotArea->time_step = time_step;
    drawGraphics[ui->chartBox->currentIndex()]->updateView();
}

void MainWindow::setPeriod(quint16 value)
{
    period = value * 1000;
    drawGraphics[ui->chartBox->currentIndex()]->updateView();
}

void MainWindow::setStretch(quint8 stretch)
{
    drawGraphics[ui->chartBox->currentIndex()]->plotArea->stretch_x = stretch * 10 + 30;
    drawGraphics[ui->chartBox->currentIndex()]->updateView();
}

void MainWindow::setMinY(int min_y)
{
    drawGraphics[ui->chartBox->currentIndex()]->plotArea->min_y = min_y;
    drawGraphics[ui->chartBox->currentIndex()]->update();
}

void MainWindow::setMaxY(int max_y)
{
    drawGraphics[ui->chartBox->currentIndex()]->plotArea->max_y = max_y;
    drawGraphics[ui->chartBox->currentIndex()]->update();
}

void MainWindow::setCountY(int count_y)
{
    drawGraphics[ui->chartBox->currentIndex()]->count_y = count_y;
    drawGraphics[ui->chartBox->currentIndex()]->update();
}

void MainWindow::hideVoltage(bool visible)
{
    if (visible) ui->voltageGraph->show();
    else ui->voltageGraph->close();
}

void MainWindow::hideAmperage(bool visible)
{
    if (visible) ui->amperageGraph->show();
    else ui->amperageGraph->close();
}

void MainWindow::hidePower(bool visible)
{
    if (visible) ui->powerGraph->show();
    else ui->powerGraph->close();
}
