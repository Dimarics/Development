#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

#if QT_CONFIG(translation)
    QTranslator translator;
    if (translator.load(QLocale::system(), u"qtbase"_qs, u"_"_qs, QLibraryInfo::path(QLibraryInfo::TranslationsPath)))
        app.installTranslator(&translator);
#endif

    MainWindow window;
    window.show();
    return app.exec();
}
