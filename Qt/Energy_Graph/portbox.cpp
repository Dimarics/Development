#include "portbox.h"

PortBox::PortBox(QWidget *parent) : QComboBox(parent) {}

void PortBox::mousePressEvent(QMouseEvent *event)
{
    emit menuOpened();
    QComboBox::mousePressEvent(event);
}
