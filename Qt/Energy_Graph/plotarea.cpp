#include "plotarea.h"
#include <QDebug>
#include <QWidget>

PlotArea::PlotArea() : QGraphicsItem()
{
    time_step = QTime(0, 1);
    start_time = QTime::currentTime();
    start_time.setHMS(start_time.hour(), start_time.minute(), 0);
}

void PlotArea::addPoint(qreal value, quint32 seconds)
{
    ToolZone *toolZone = new ToolZone(color, this);
    QString time_info = start_time.addSecs(seconds).toString("hh:mm");
    toolZone->setToolTip(QByteArray::number(value, 'f', 1) + " " + demens + "\n" + time_info);
    tool_tips << toolZone;
    points << QPointF(seconds, value);
}

void PlotArea::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    QTime time = start_time;
    QList<QPointF> coordinates;
    for (quint16 i = 0; i < points.size(); ++i)
    {
        qreal x = points.at(i).x() == 0 ? 0 : stretch_x / (time_step.msecsSinceStartOfDay() / 1000.0 / points.at(i).x());
        qreal y = height - height / (max_y - min_y) * (points.at(i).y() - min_y);
        coordinates << QPointF(x, y);
        tool_tips.at(i)->setPos(x - 4, y - 4);
    }
    bounding_width = coordinates.isEmpty() ? 0 : coordinates.last().x() + 100;
    if (bounding_width < width) bounding_width = width - 60; // w

    for (quint16 i = stretch_x; i < bounding_width; i += stretch_x) // значения оси x
    {
        painter->setPen(Qt::lightGray);
        painter->drawLine(i, 0, i, height + 6);
        painter->setPen(Qt::black);
        time = time.addMSecs(time_step.msecsSinceStartOfDay());
        QString time_info = time.toString("hh:mm");
        quint16 font_width = painter->fontMetrics().size(Qt::TextSingleLine, time_info).width() / 2;
        painter->drawText(i - font_width, height + 22, time_info);
    }
    QPen pen = QPen(Qt::lightGray);
    pen.setWidth(2);
    painter->setPen(pen);
    painter->drawLine(0, 0, 0, height + 6);

    pen.setWidthF(1.5);
    pen.setColor(color);
    painter->setPen(pen);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawPolyline(coordinates);
}

QRectF PlotArea::boundingRect() const {
    return QRectF(0, 0, bounding_width, height);
}

ToolZone::ToolZone(const QColor &point_color, QGraphicsItem *parent) : QGraphicsItem(parent)
{
    color = point_color;
    setAcceptHoverEvents(true);
}

void ToolZone::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    painter->setPen(color);
    painter->setBrush(color);
    painter->setRenderHint(QPainter::Antialiasing);
    if (draw_point) painter->drawEllipse(1, 1, 6, 6);
}

void ToolZone::hoverEnterEvent(QGraphicsSceneHoverEvent*)
{
    draw_point = true;
    update();
}

void ToolZone::hoverLeaveEvent(QGraphicsSceneHoverEvent*)
{
    draw_point = false;
    update();
}

QRectF ToolZone::boundingRect() const {
    return QRectF(0, 0, 8, 8);
}
