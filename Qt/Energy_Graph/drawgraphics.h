#ifndef DRAW_H
#define DRAW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPainter>
#include <QScrollBar>
//#include <QTime>
#include "plotarea.h"

class DrawGraphics : public QGraphicsView
{
    Q_OBJECT

public:
    QByteArray header;
    quint8 count_y = 4;
    PlotArea *plotArea;
    QGraphicsScene *scene;
    QScrollBar *scrollBar;

    void updateView();
    void setScrollBar();
    void setColor(const QColor&);
    explicit DrawGraphics(QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent*);
    void drawForeground(QPainter*, const QRectF&);
    void drawBackground(QPainter*, const QRectF&);
    void wheelEvent(QWheelEvent*);

private:
    quint16 plot_height = 0;

public slots:
    void scrollGraphic(int);
};

#endif // DRAW_H
