#include "colorbox.h"
#include <QPainter>

ColorBox::ColorBox(QWidget *parent) : QComboBox(parent){}

void ColorBox::addColor(const QColor &color, const QString &name)
{
    QPixmap pixmap(20, 20);
    QPainter painter(&pixmap);
    painter.setPen(Qt::black);
    painter.setBrush(color);
    painter.drawRect(0, 0, 19, 19);
    addItem(QIcon(pixmap), " " + name, color);
}

QColor ColorBox::color(int index)
{
    if (index == -1) index = currentIndex();
    return itemData(index, Qt::UserRole).value<QColor>();
}

int ColorBox::colorIndex(const QColor &color)
{
    for (int i = 0; i < count(); ++i)
        if (itemData(i).value<QColor>() == color)
            return i;
    return -1;
}
