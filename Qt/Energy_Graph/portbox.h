#ifndef PORTBOX_H
#define PORTBOX_H

#include <QComboBox>

class PortBox : public QComboBox
{
    Q_OBJECT
public:
    explicit PortBox(QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent*);

signals:
    void menuOpened();
};

#endif // PORTBOX_H
