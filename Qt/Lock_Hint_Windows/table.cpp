#include "table.h"
#include "settings.h"

int text_color = 0;

Table::Table(QWidget *parent) : QTableWidget(parent)
{
    setRowCount(10);
    setColumnCount(4);
    setStyleSheet(Style::table);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    verticalHeader()->setDefaultSectionSize(55);
    verticalHeader()->setVisible(false);

    QStringList header;
    header << "Сервис" << "Логин" << "Пароль" << "Ссылка"; //заголовок таблицы
    setHorizontalHeaderLabels(header);
    horizontalHeader()->setFixedHeight(55);
    horizontalHeader()->setDefaultSectionSize(334);
    horizontalHeader()->hideSection(3);
    horizontalHeader()->setStyleSheet(Style::tableHeader);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    horizontalHeader()->setSectionsClickable(false);
    verticalScrollBar()->setStyleSheet(Style::tableScrollBar);
    verticalScrollBar()->setTracking(true);
    setItemDelegate(new TableDelegate);

    setMouseTracking(true);
    //connect(verticalScrollBar(), &QAbstractSlider::valueChanged, this, &Table::stopScroll);

    option = new QMenu(this);
    option->setStyleSheet(Style::contextMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested, this, &Table::contextMenu);
    QAction *copy = new QAction("Копировать", this);
    option->addAction(copy);
    connect(copy, &QAction::triggered, this, &Table::copyText);
    edit = new QAction("Изменить", this);
    option->addAction(edit);
    del = new QAction("Удалить", this);
    option->addAction(del);
}

void Table::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    clearSelection();
}

void Table::wheelEvent(QWheelEvent* event)
{
    int step;
    event->angleDelta().y() > 0 ? step = -1 : step = 1;
    verticalScrollBar()->setValue(verticalScrollBar()->value() + step);
    verticalScrollBar()->setMaximum(rowCount() - 10);
}

void Table::contextMenu(QPoint pos)
{
    option->popup(viewport()->mapToGlobal(pos));
}

void Table::copyText()
{
    if (item(currentRow(), currentColumn()) != 0)
    {
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(item(currentRow(), currentColumn())->text());
    }
}

void TableDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const
{
    QStyleOptionViewItem itemOption(option);
    if (itemOption.state & QStyle::State_HasFocus)
    {
        itemOption.state = itemOption.state ^ QStyle::State_HasFocus;
        painter->fillRect(option.rect, QColor(255, 255, 255, 40));
    }
    if (text_color == 0)
        itemOption.palette.setColor(QPalette::HighlightedText, Qt::white);
    else if (text_color == 1)
        itemOption.palette.setColor(QPalette::HighlightedText, Qt::yellow);
    else if (text_color == 2)
        itemOption.palette.setColor(QPalette::HighlightedText, QColor(0, 255, 0));
    QStyledItemDelegate::paint(painter, itemOption, index);
}
