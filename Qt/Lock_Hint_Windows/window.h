#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QMouseEvent>
#include <QPushButton>
#include <QTimer>
#include <QPalette>

#include "block.h"
#include "menu.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

void decryption();

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent = 0);
    ~Window(); 

private:
    QPoint mp;
    Ui::Window *ui;
    BlockMenu *block_menu;
    MainMenu *main_menu;
    QPalette blockStyle;
    bool window_move = false;
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private slots:
    void checkLang();

public slots:
    void setMenu(bool);

signals:
    void switched(QString);
};
#endif // WINDOW_H
