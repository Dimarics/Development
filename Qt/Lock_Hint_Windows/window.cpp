#include "ui_window.h"
#include "window.h"
#include "settings.h"

Window::Window(QWidget *parent) : QWidget(parent, Qt::Window | Qt::FramelessWindowHint), ui(new Ui::Window)
{
    decryption();
    //ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect(this);
    setGraphicsEffect(shadow);
    shadow->setColor(QColor(0, 0, 0, 200));
    shadow->setBlurRadius(15);
    shadow->setOffset(0);
    resize(1274, 840);

    QLabel *icon = new QLabel(this);
    icon->setPixmap(QPixmap(":/res/windowIcon.png"));
    icon->setGeometry(18, 14, 18, 18);

    QLabel *header = new QLabel(this);
    header->setStyleSheet("color: lightgray; font: 11pt \"Segoe UI\"");
    header->setText("Lock Hint");
    header->setGeometry(42, 16, 100, 16);

    main_menu = new MainMenu(this); //основное меню
    main_menu->setGeometry(10, 36, width() - 20, height() - 46);
    main_menu->close();
    connect(this, &Window::switched, main_menu, &MainMenu::switchLayout); //смена раскладки
    connect(main_menu, &MainMenu::block, this, &Window::setMenu); //закрытие виджета

    block_menu = new BlockMenu(this); //меню блокировки
    block_menu->setGeometry(10, 36, width()-20, height() - 46);
    connect(this, &Window::switched, block_menu, &BlockMenu::switchLayout); //смена раскладки
    connect(block_menu, &BlockMenu::block, this, &Window::setMenu); //закрытие виджета

    QPushButton *btn_close = new QPushButton(this); //кнопка закрытия окна
    btn_close->setGeometry(width() - 55, 10, 45, 26);
    btn_close->setStyleSheet(Style::closeBtn);
    btn_close->setToolTip("Закрыть");
    connect(btn_close, &QPushButton::clicked, qApp, &QApplication::quit);

    QPushButton *btn_fold = new QPushButton(this); //кнопка сворачивания окна
    btn_fold->setGeometry(width() - 100, 10, 45, 26);
    btn_fold->setStyleSheet(Style::foldBtn);
    btn_fold->setToolTip("Свернуть");
    connect(btn_fold, &QPushButton::clicked, this, &Window::showMinimized);

    QTimer *tmr = new QTimer(); //цикл проверки раскладки
    tmr->setInterval(10);
    connect(tmr, &QTimer::timeout, this, &Window::checkLang);
    tmr->start();
}

Window::~Window()
{
    delete ui;
}

void Window::checkLang()
{
    QString lang = QApplication::inputMethod()->locale().name(); //проверка раскладки ru_RU en_EN
    if (lang == "ru_RU")
        emit switched("ru");
    else
        emit switched("en");
}

void Window::setMenu(bool block)
{
    if (block)
    {
        main_menu->close();
        block_menu->show();
        block_menu->string->setFocus();
    }
    else
    {
        block_menu->close();
        main_menu->show();
        main_menu->setFocus();
    }
}

void Window::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QColor(0, 0, 0, 0)); //контур виджета
    painter.setBrush(QColor(60, 68, 84, 255)); //фон виджета
    painter.drawRect(QRect(10, 10, width()-20, 26));
}

void Window::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        if (QRect(10, 10, width()-20, 26).contains(event->pos()))
        {
            mp = event->pos();
            window_move = true;
        }
    }
}

void Window::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    window_move = false;
}

void Window::mouseMoveEvent(QMouseEvent *event)
{
    if (window_move)
    {
        QPointF pos = event->screenPos() - mp;
        setGeometry(pos.x(), pos.y(), width(), height());
    }
}
