#ifndef BROWSER_H
#define BROWSER_H

#include <QWebEngineView>

class Browser :public QWebEngineView
{
    Q_OBJECT
public:
    Browser();

private slots:
    void checkLoad(bool);
};

#endif // BROWSER_H
