#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "browser.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    bool eventFilter(QObject*, QEvent*) override;

protected:
    //void mouseMoveEvent(QMouseEvent*);

private:
    Browser *browser;
    Ui::MainWindow *ui;

private slots:
    void setZoom(int);
    void hideSettings();
};

#endif // MAINWINDOW_H
