#ifndef SERVICEEDITOR_H
#define SERVICEEDITOR_H

#include <QWidget>

namespace Ui {
class ServiceEditor;
}

class ServiceEditor : public QWidget
{
    Q_OBJECT
public:
    ServiceEditor(QWidget* = 0);

private:
    Ui::ServiceEditor *ui;
};

#endif // SERVICEEDITOR_H
