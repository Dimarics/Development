#include "window.h"
#include "windowcontent.h"

#include <QApplication>
#include <QFontDatabase>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QFontDatabase::addApplicationFont(":/res/resourses/Algerian.ttf");
    Window window(new WindowContent);
    window.show();
    return app.exec();
}
