#include "windowcontent.h"
#include <QFile>
#include <QTextStream>

#define CODE(a, b) ch == QString(a) ? out << b :

void WindowContent::cryptography()
{
    QFile file("data.dat");
    if (file.open(QIODevice::WriteOnly))
    {
        int new_line = 0;
        QTextStream out(&file);

        for (QStringList &service : database)
        {
            for (QString &data : service)
            {
                for (QChar &ch : data)
                {
                    CODE("0", "Bl")
                    CODE("1", "dK")
                    CODE("2", "fQ")
                    CODE("3", "Kj")
                    CODE("4", "Vq")
                    CODE("5", "PI")
                    CODE("6", "lb")
                    CODE("7", "Mf")
                    CODE("8", "aZ")
                    CODE("9", "oI")

                    CODE("A", "UE")
                    CODE("a", "Pl")
                    CODE("B", "rX")
                    CODE("b", "ns")
                    CODE("C", "Jz")
                    CODE("c", "yI")
                    CODE("D", "oL")
                    CODE("d", "Xk")
                    CODE("E", "Rb")
                    CODE("e", "Iv")
                    CODE("F", "EA")
                    CODE("f", "xy")
                    CODE("G", "qW")
                    CODE("g", "IC")
                    CODE("H", "aw")
                    CODE("h", "Hx")
                    CODE("I", "sZ")
                    CODE("i", "Ju")
                    CODE("J", "mK")
                    CODE("j", "Sa")
                    CODE("K", "Wi")
                    CODE("k", "kJ")
                    CODE("L", "Di")
                    CODE("l", "Gw")
                    CODE("M", "lV")
                    CODE("m", "Qh")
                    CODE("N", "jx")
                    CODE("n", "Hv")
                    CODE("O", "bh")
                    CODE("o", "VH")
                    CODE("P", "QD")
                    CODE("p", "VB")
                    CODE("Q", "pj")
                    CODE("q", "rJ")
                    CODE("R", "Kd")
                    CODE("r", "AU")
                    CODE("S", "Ou")
                    CODE("s", "jO")
                    CODE("T", "LF")
                    CODE("t", "Ka")
                    CODE("U", "ZG")
                    CODE("u", "sG")
                    CODE("V", "Po")
                    CODE("v", "it")
                    CODE("W", "Fg")
                    CODE("w", "sT")
                    CODE("X", "Mz")
                    CODE("x", "Ae")
                    CODE("Y", "In")
                    CODE("y", "GZ")
                    CODE("Z", "Tm")
                    CODE("z", "hY")

                    CODE("А", "iF")
                    CODE("а", "SP")
                    CODE("Б", "YM")
                    CODE("б", "tM")
                    CODE("В", "Dj")
                    CODE("в", "mN")
                    CODE("Г", "Lg")
                    CODE("г", "Nf")
                    CODE("Д", "uk")
                    CODE("д", "gH")
                    CODE("Е", "pK")
                    CODE("е", "uI")
                    CODE("Ё", "BY")
                    CODE("ё", "nl")
                    CODE("Ж", "zL")
                    CODE("ж", "Fp")
                    CODE("З", "WE")
                    CODE("з", "YL")
                    CODE("И", "HO")
                    CODE("и", "Zs")
                    CODE("Й", "XG")
                    CODE("й", "kM")
                    CODE("К", "Rq")
                    CODE("к", "fX")
                    CODE("Л", "OC")
                    CODE("л", "JI")
                    CODE("М", "Br")
                    CODE("м", "Cy")
                    CODE("Н", "UH")
                    CODE("н", "bD")
                    CODE("О", "vN")
                    CODE("о", "tm")
                    CODE("П", "MQ")
                    CODE("п", "dP")
                    CODE("Р", "gE")
                    CODE("р", "aW")
                    CODE("С", "iD")
                    CODE("с", "KP")
                    CODE("Т", "qf")
                    CODE("т", "BS")
                    CODE("У", "EF")
                    CODE("у", "vp")
                    CODE("Ф", "xS")
                    CODE("ф", "ls")
                    CODE("Х", "Er")
                    CODE("х", "Fn")
                    CODE("Ц", "dR")
                    CODE("ц", "wq")
                    CODE("Ч", "Mt")
                    CODE("ч", "fB")
                    CODE("Ш", "GV")
                    CODE("ш", "Ze")
                    CODE("Щ", "Ha")
                    CODE("щ", "Ts")
                    CODE("Ъ", "DA")
                    CODE("ъ", "AB")
                    CODE("Ы", "NJ")
                    CODE("ы", "zQ")
                    CODE("Ь", "LT")
                    CODE("ь", "SA")
                    CODE("Э", "yd")
                    CODE("э", "wo")
                    CODE("Ю", "oS")
                    CODE("ю", "SK")
                    CODE("Я", "cU")
                    CODE("я", "eR")

                    CODE(" ", "Sp")
                    CODE(".", "vb")
                    CODE(",", "Jd")
                    CODE("+", "mR")
                    CODE("-", "pL")
                    CODE("*", "oP")
                    CODE("M", "tF")
                    CODE("/", "Wl")
                    CODE("|", "nD")
                    CODE("\\", "rV")
                    CODE("<", "Pq")
                    CODE(">", "KW")
                    CODE("?", "Js")
                    CODE("!", "iC")
                    CODE(":", "ek")
                    CODE(";", "uA")
                    CODE("(", "Ol")
                    CODE(")", "KN")
                    CODE("{", "wj")
                    CODE("}", "Yu")
                    CODE("[", "qK")
                    CODE("]", "Us")
                    CODE("\'", "DF")
                    CODE("\"", "Rt")
                    CODE("`", "YQ")
                    CODE("~", "Re")
                    CODE("@", "Qm")
                    CODE("#", "uY")
                    CODE("№", "Gf")
                    CODE("$", "QV")
                    CODE("%", "zT")
                    CODE("&", "eS")
                    CODE("_", "PT")
                    out << "";

                    ++new_line;
                    if (new_line % 20 == 0) out << " \n";
                }
            }
            out << "eY";
            ++new_line;
            if (new_line % 20 == 0) out << " \n";
        }
    }
    file.close();
}
