#include "window.h"
#include "ui_window.h"

#include <QGraphicsDropShadowEffect>
#include <QMouseEvent>
#include <QPainter>

Window::Window(QWidget *widget, const bool dialog, const QColor &color, const QString &title) :
    QWidget(0, Qt::Window | Qt::FramelessWindowHint), ui(new Ui::Window)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    qApp->installEventFilter(this);
    if (!dialog)
    {
        ui->dialogButtonBox->close();
        ui->dialogButtonBox->deleteLater();
        connect(ui->quitButton, &QPushButton::clicked, qApp, &QCoreApplication::quit);
        connect(ui->screenModeButton, &QPushButton::toggled, this, &Window::screenMode);
        connect(ui->hideButton, &QPushButton::clicked, this, &QWidget::showMinimized);
    }
    else
    {
        setAttribute(Qt::WA_DeleteOnClose);
        setWindowModality(Qt::WindowModality::ApplicationModal);
        ui->hideButton->close();
        ui->hideButton->deleteLater();
        ui->screenModeButton->close();
        ui->screenModeButton->deleteLater();
        connect(ui->quitButton, &QPushButton::clicked, this, &QWidget::close);
        connect(ui->cancelButton, &QPushButton::clicked, this, &QWidget::close);
    }
    ui->windowLayout->insertWidget(0, widget);
    setTitle(title);
    setBackgroundColor(color);

    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect(this);
    setGraphicsEffect(shadow);
    shadow->setColor(Qt::black);
    shadow->setBlurRadius(18);
    shadow->setOffset(0);
}

void Window::screenMode(bool maximized) {
    static QRect geometry = normalGeometry();
    if (maximized)
    {
        geometry = normalGeometry();
        setGeometry(-10, -10, screen()->geometry().width() + 20, screen()->geometry().height() + 20);
    }
    else setGeometry(geometry);
}

void Window::setBackgroundColor(const QColor &color)
{
    ui->backgroundWidget->setStyleSheet(QString("#backgroundWidget{background-color:rgba(%1,%2,%3,%4)}")
                                        .arg(color.red()).arg(color.green()).arg(color.blue()).arg(color.alpha()));
}

void Window::setTitle(const QString &title) {
    ui->title->setText(title);
}

bool Window::eventFilter(QObject* target, QEvent* event)
{
    static QPointF mouse_pos;
    static bool mouse_pressed = false;
    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);

    switch (event->type())
    {
    case QEvent::MouseButtonPress:
        if (mouseEvent->button() == Qt::LeftButton)
        {
            mouse_pressed = true;
            if (QRect(10, 10, width() - 161, 26).contains(mouseEvent->pos())) // окно
                mouse_pos = mouseEvent->pos();
        }
        break;

    case QEvent::MouseMove:
        if (mouse_pressed)
        {
            const QPointF global_pos = mouseEvent->globalPosition();
            if (target == ui->windowPanel)
            {
                const QPointF pos = global_pos - mouse_pos;
                if (ui->screenModeButton->isChecked())
                    ui->screenModeButton->setChecked(false);
                move(pos.x(), pos.y());
            }
            else if (target == ui->leftField && width() + x() - global_pos.x() >= minimumWidth())
                setGeometry(global_pos.x(), y(), width() + x() - global_pos.x(), height());
            else if (target == ui->leftTopField)
            {
                const int _width = width() + x() - global_pos.x();
                const int _height = height() + y() - global_pos.y();

                if (_width >= minimumWidth())
                    setGeometry(global_pos.x(), y(), _width, height());
                if (_height >= minimumHeight())
                    setGeometry(x(), global_pos.y(), width(), _height);
            }
            else if (target == ui->topField && height() + y() - global_pos.y() >= minimumHeight())
                setGeometry(x(), global_pos.y(), width(), height() + y() - global_pos.y());
            else if (target == ui->rightTopField)
            {
                const int _width = global_pos.x() - x();
                const int _height = height() + y() - global_pos.y();

                if (_width >= minimumWidth())
                    setGeometry(x(), y(), _width, height());
                if (_height >= minimumHeight())
                    setGeometry(x(), global_pos.y(), width(), _height);
            }
            else if (target == ui->rightField && global_pos.x() - x() >= minimumWidth())
                setGeometry(x(), y(), global_pos.x() - x(), height());
            else if (target == ui->rightBottomField)
            {
                const int _width = global_pos.x() - x();
                const int _height = global_pos.y() - y();

                if (_width >= minimumWidth())
                    setGeometry(x(), y(), _width, height());
                if (_height >= minimumHeight())
                    setGeometry(x(), y(), width(), _height);
            }
            else if (target == ui->bottomField && global_pos.y() - y() >= minimumHeight())
                setGeometry(x(), y(), width(), global_pos.y() - y());
            else if (target == ui->leftBottomField)
            {
                const int _width = width() + x() - global_pos.x();
                const int _height = global_pos.y() - y();

                if (_width >= minimumWidth())
                    setGeometry(global_pos.x(), y(), _width, height());
                if (_height >= minimumHeight())
                    setGeometry(x(), y(), width(), _height);
            }
        }
        break;

    case QEvent::MouseButtonRelease:
        if (mouseEvent->button() == Qt::LeftButton)
            mouse_pressed = false;
        break;

    default:
        break;
    }
    return QWidget::eventFilter(target, event);
}
