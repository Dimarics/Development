#include "windowcontent.h"
#include "ui_windowcontent.h"

#include <QMouseEvent>
#include <QTimer>

WindowContent::WindowContent(QWidget *parent) :
    QWidget(parent, Qt::Window | Qt::FramelessWindowHint), ui(new Ui::WindowContent)
{
    qApp->installEventFilter(this);
    ui->setupUi(this);

    ui->password_Line->setFocus();
    showPassword(false);
    connect(ui->passwordVision, &QPushButton::toggled, this, &WindowContent::showPassword);
    connect(ui->password_Line, &QLineEdit::returnPressed, this, &WindowContent::enterPassword);
    connect(ui->password_Line, &QLineEdit::selectionChanged, ui->password_Line, &QLineEdit::deselect);
    connect(ui->block_btn, &QPushButton::clicked, this, &WindowContent::blockMenu);
    mainWidgetInit();

    QTimer *timer = new QTimer(); // таймер проверки раскладки
    timer->setInterval(50);
    connect(timer, &QTimer::timeout, this, &WindowContent::checkLang);
    timer->start();
    decryption();
    fillTable();
}

void WindowContent::checkLang()
{
    QString lang = QApplication::inputMethod()->locale().name();
    lang.resize(2);
    ui->blockLang->setText(lang);
    ui->searchLang->setText(lang);
}

void WindowContent::screenMode(bool maximized) {
    static QRect geometry = normalGeometry();
    if (maximized)
    {
        geometry = normalGeometry();
        setGeometry(-10, -10, screen()->geometry().width() + 20, screen()->geometry().height() + 20);
    }
    else setGeometry(geometry);
}

void WindowContent::showPassword(bool show)
{
    QFont font = ui->password_Line->font();
    if (show)
    {
        font.setPointSize(20);
        ui->password_Line->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        font.setPointSize(24);
        ui->password_Line->setEchoMode(QLineEdit::Password);
    }
    ui->password_Line->setFont(font);
}

void WindowContent::enterPassword()
{
    if (ui->password_Line->text() == password)
        ui->stackedWidget->setCurrentWidget(ui->mainWidget);
    else
    {
        QLabel *passwordError = new QLabel(this);
        passwordError->setText("Пароль неверный!");
        passwordError->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        passwordError->setStyleSheet("color: red; font-size: 18pt");
        ui->messageLayout->addWidget(passwordError);
        QTimer::singleShot(1500, passwordError, &QObject::deleteLater);
    }
}

void WindowContent::blockMenu() {
    ui->stackedWidget->setCurrentWidget(ui->blockWidget);
}

bool WindowContent::eventFilter(QObject* target, QEvent* event)
{
    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);

    switch (event->type())
    {
    case QEvent::MouseButtonPress:
        if (mouseEvent->button() == Qt::LeftButton && target == ui->search_icon)
            ui->search->setFocus();
        break;

    case QEvent::FocusOut:
        if (target == ui->password_Line && ui->password_Line->isVisible())
            ui->password_Line->setFocus();
        break;

    default:
        break;
    }
    return QWidget::eventFilter(target, event);
}
