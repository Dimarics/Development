#include "windowcontent.h"
#include "ui_windowcontent.h"
#include "window.h"
#include "serviceeditor.h"
#include <QDialog>

void WindowContent::mainWidgetInit()
{
    connect(ui->table, &QTableWidget::cellDoubleClicked, this, &WindowContent::serviceEdit);
}

void WindowContent::fillTable()
{
    for (quint16 row = 0; row < database.size(); ++row)
    {
        for (qint8 column = 1; column < 5; ++column)
        {
            QTableWidgetItem *tableItem = new QTableWidgetItem(database.at(row).at(column));
            if (column == 1) tableItem->setIcon(QIcon(QPixmap("./" + database.at(row).at(0) + ".png")));
            ui->table->setItem(row, column - 1, tableItem);
        }
    }
}

void WindowContent::serviceEdit()
{
    Window *serviceEditor = new Window(new ServiceEditor(), true, QColor(60, 68, 84));
    serviceEditor->show();
}
