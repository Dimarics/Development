#ifndef TABLE_H
#define TABLE_H

#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QWheelEvent>
#include <QMenu>
#include <QApplication>
#include <QClipboard>

class Table : public QTableWidget
{
    Q_OBJECT
public:
    QMenu *option;
    QAction *edit;
    QAction *del;
    Table(QWidget* = 0);
    void setTextColor(const QColor&);

protected:
    void focusOutEvent(QFocusEvent*);
    //void wheelEvent(QWheelEvent*);
    void resizeEvent(QResizeEvent*);

private slots:
    void contextMenu(QPoint);
    void copyText();
};

class TableDelegate : public QStyledItemDelegate
{
public:
    QColor textColor = QColor(Qt::white);
protected:
    void paint(QPainter* painter, const QStyleOptionViewItem &option, const QModelIndex& index) const;
};

#endif // TABLE_H
