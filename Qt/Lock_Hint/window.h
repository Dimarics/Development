#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>

namespace Ui { class Window; }

class Window : public QWidget
{
    Q_OBJECT
public:
    Window(QWidget*, const bool = false, const QColor& = Qt::transparent, const QString& = "Lock Hint");
    void setBackgroundColor(const QColor&);
    void setTitle(const QString&);

private:
    QPoint mouse_pos;
    Ui::Window *ui;
    bool eventFilter(QObject*, QEvent*);

private slots:
    void screenMode(bool);
};

#endif // WINDOW_H
