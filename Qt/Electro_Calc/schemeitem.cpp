#include "schemeitem.h"

#include <QPainter>
#include <QGraphicsEffect>
#include <QGraphicsSceneMouseEvent>
#include <QStaticText>
#include <QInputDialog>
//----------------------------------------------------------------------------------------
SchemeItem::SchemeItem(uint id)
{
    setId(id);
    block = false;
    opacity = 0.5;
    selectionGlow = new SelectionGlow(this);
}

void SchemeItem::setId(uint value) {
    id = value;
    const QString numbers("₀₁₂₃₄₅₆₇₈₉");

    string_id.clear();
    while (value != 0)
    {
        string_id.push_front(numbers.at(value % 10));
        value /= 10;
    }
}

void SchemeItem::setBlocked(bool value) {
    block = value;
}

void SchemeItem::setTransparent(bool transparent) {
    opacity = transparent ? 0.5 : 1;
}

void SchemeItem::setGridPos(const QPointF &point)
{
    const quint8 grid_step = 26;
    const qreal pos_x = grid_step * (int(point.x()) / grid_step + 0.5);
    const qreal pos_y = grid_step * (int(point.y()) / grid_step + 0.5);
    setPos(grid_step * QPointF(int(point.x()) / grid_step + 0.5, int(point.y()) / grid_step + 0.5) - transformOriginPoint());
    //const qreal pos_x = grid_step * (int(point.x()) / grid_step) + grid_step / 2;
    //const qreal pos_y = grid_step * (int(point.y()) / grid_step) + grid_step / 2;

    //setPos(QPointF(pos_x - transformOriginPoint().x(), pos_y - transformOriginPoint().y()));
}

void SchemeItem::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    painter->setOpacity(opacity);
    selectionGlow->update();
}

int SchemeItem::type() const {
    return SchemeType;
}
//----------------------------------------------------------------------------------------
SelectionGlow::SelectionGlow(QGraphicsItem *parent) : QGraphicsItem(parent)
{
    QGraphicsBlurEffect *blur = new QGraphicsBlurEffect();
    blur->setBlurRadius(3);
    setGraphicsEffect(blur);
}

void SelectionGlow::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    if (static_cast<SchemeItem*>(parentItem())->block)
    {
        painter->setPen(QPen(QColor(255, 0, 0, 150), 5));
        painter->drawPath(parentItem()->shape());
    }
    else if (parentItem()->isSelected())
    {
        painter->setPen(QPen(QColor(0, 65, 255, 150), 5));
        painter->drawPath(parentItem()->shape());
    }
}

QRectF SelectionGlow::boundingRect() const {
    return parentItem()->boundingRect();
}
//----------------------------------------------------------------------------------------
Generator::Generator(uint id) : SchemeItem(id)
{
    QPainterPath form;
    rect = QRectF(0, 0, 53, 53);
    form.addRect(rect);
    setPath(form);
    setTransformOriginPoint(rect.width() / 2, rect.height() / 2);
    setZValue(0.2);
}

void Generator::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget*)
{
    SchemeItem::paint(painter, option);

    painter->setPen(QPen(pen_color, 2.5));
    painter->setBrush(Qt::white);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawRect(rect);

    painter->setFont(QFont("Calibri", 26));
    painter->setPen(text_color);
    painter->drawText(rect, Qt::AlignCenter, "Е" + string_id);
}

int Generator::type() const {
    return TypeGen;
}
//----------------------------------------------------------------------------------------
Knot::Knot(uint id) : SchemeItem(id)
{
    QPainterPath form;
    rect = QRect(0, 0, 53, 53);
    form.addEllipse(rect);
    setPath(form);
    setTransformOriginPoint(rect.width() / 2, rect.height() / 2);
    setZValue(0.2);
}

void Knot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget*)
{
    SchemeItem::paint(painter, option);

    painter->setPen(QPen(pen_color, 2.7));
    painter->setBrush(Qt::white);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawEllipse(rect);

    painter->setFont(QFont("Calibri", 26));
    painter->setPen(text_color);
    painter->drawText(rect, Qt::AlignCenter, "У" + string_id);
}

int Knot::type() const {
    return TypeKnot;
}
//----------------------------------------------------------------------------------------
Resistor::Resistor()
{
    const quint16 width = 100;
    const quint16 height = 30;
    const QList<QPointF> polygon
    {
        QPoint(-2, 13), QPoint(18, 13), QPoint(18, 0), QPoint(width - 18, 0),
                QPoint(width - 18, 13), QPoint(width + 2, 13), QPoint(width + 2, height - 13),
                QPoint(width - 18, height - 13),  QPoint(width - 18, height),
                QPoint(18, height), QPoint(18, height - 13), QPoint(-2, height - 13),
    };
    QPainterPath form;
    form.addPolygon(QPolygonF(polygon));
    setPath(form);
    setTransformOriginPoint(width / 2, height / 2);
    setZValue(0.1);
}

void Resistor::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget*)
{
    SchemeItem::paint(painter, option);

    painter->setRenderHint(QPainter::Antialiasing);
    painter->setPen(QPen(pen_color, 5));
    painter->setBrush(Qt::black);
    painter->drawLine(QLineF(0.5, 15.5, 15, 15.5));
    painter->drawLine(QLineF(86, 15.5, 100.5, 15.5));
    painter->setPen(QPen(pen_color, 3.5));
    painter->setBrush(Qt::white);
    painter->drawRect(18, 0, 65, 31);
}

int Resistor::type() const {
    return TypeResist;
}
//----------------------------------------------------------------------------------------
Wire::Wire(const QPointF &point)
{
    line.setP1(point + QPointF(0.5, 0.5));
    setP2(point);
    setPen(QPen(QColor(0, 132, 0), 5, Qt::SolidLine, Qt::RoundCap));
}

void Wire::setP2(const QPointF &point)
{
    QPainterPath form;
    form.moveTo(line.p1());
    form.lineTo(point + QPointF(0.5, 0.5));
    setPath(form);
    line.setP2(point + QPointF(0.5, 0.5));
}

void Wire::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget*)
{
    SchemeItem::paint(painter, option);

    painter->setPen(pen());
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawLine(line.p1(), line.p2());
}

int Wire::type() const {
    return TypeWire;
}
//----------------------------------------------------------------------------------------
Load::Load()
{
    const QList<QPointF> points
    {
        QPointF(5, 0), QPointF(5, height - 18), QPointF(0, height - 18),
                QPointF(width / 2, height), QPointF(width, height - 18),
                QPointF(width - 5, height - 18), QPointF(width - 5, 0)
    };
    QPainterPath form;
    polygon = QPolygonF(points);
    form.addPolygon(polygon);
    setPath(form);
    //load = QInputDialog::getDouble(nullptr, QString(), "Нагрузка:", 1, -2147483647, 2147483647, 9);
    setAcceptHoverEvents(true);
    setTransformOriginPoint(width / 2 - 0.5, 0);
    setZValue(0.1);
}

void Load::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget*)
{
    SchemeItem::paint(painter, option);

    painter->setPen(Qt::NoPen);
    painter->setBrush(pen_color);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawPolygon(polygon);

    painter->setPen(text_color);
    QString text = "";
    QFont font = painter->font();
    font.setPointSize(16);
    painter->setFont(font);
    qreal pos_x = (width - painter->fontMetrics().boundingRect(text).width()) / 2;
    qreal pos_y = height + painter->fontMetrics().boundingRect(text).height() / 2 + 3;

    switch (int(rotation()))
    {
    case 90:
        pos_x = -(height + painter->fontMetrics().boundingRect(text).width()) / 2;
        pos_y = 0;
        painter->rotate(270);
        break;
    case 180:
        pos_x = -(width + painter->fontMetrics().boundingRect(text).width()) / 2;
        pos_y = -height - 3;
        painter->rotate(180);
        break;
    case 270:
        pos_x = (height - painter->fontMetrics().boundingRect(text).width()) / 2;
        pos_y = -width;
        painter->rotate(90);
        break;
    default:
        break;
    }
    painter->drawText(pos_x, pos_y, text);
}

void Load::mouseDoubleClickEvent(QGraphicsSceneMouseEvent*) {
    load = QInputDialog::getDouble(nullptr, QString(), "Нагрузка:", load, -2147483647, 2147483647, 9);
}

int Load::type() const {
    return TypeLoad;
}
//----------------------------------------------------------------------------------------
