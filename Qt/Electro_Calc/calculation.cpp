#include "calculation.h"

Calculation::Calculation(Layout* layout, bool det)
{
    this->iprint = false;
    this->zprint = false;
    if (!det)
    {
        int size = layout->getQuanityVertex();
        smatrix.resize(size);
        for (int i=0; i<size; i++)
        {
            smatrix[i].resize(size);
            for (int j=0;j<size;j++)
            {
                if (i==j)
                    smatrix[i][j] =
                         (layout->getVertex(i+1)->getSumLoads() +
                          layout->getVertex(i+1)->getSumCondactons())*complexnum(-1,0);
                else
                    smatrix[i][j] =
                          layout->getVertex(i+1)->getCondacton(layout->getVertex(j+1));
            }
        }
        usualMatrix = smatrix;
    }
    else
    {
        int size = layout->getQuanityVertex();
        smatrix.resize(size);
        for (int i=0; i<size; i++)
        {
            smatrix[i].resize(size);
            for (int j=0;j<size;j++)
            {
                if (i==j)
                {
                    for (int k=1; k<=(layout->getQuanityVertex()+
                                     layout->getQuanityGener()); k++)
                        if (layout->getVertex(i+1)->getCondacton((*layout)[k])!=complexnum(0))
                            smatrix[i][j] += complexnum(1)/layout->getVertex(i+1)->getCondacton((*layout)[k]);
                    for (loadDict::iterator load=layout->getVertex(i+1)->loads.begin();load!=layout->getVertex(i+1)->loads.end();++load)
                        if (load.value()!=complexnum(0))
                            smatrix[i][j] += complexnum(1)/load.value();
                    smatrix[i][j] *= complexnum(-1);
                }
                else if (layout->getVertex(i+1)->getCondacton(layout->getVertex(j+1))!=complexnum(0))
                    smatrix[i][j] =
                          complexnum(1)/layout->getVertex(i+1)->getCondacton(layout->getVertex(j+1));
            }
        }
        usualMatrix = smatrix;
    }
    inverse();
    //Zcalk(layout);
    //Icalc(layout);
    ZC(*layout);
    IC(*layout);
}

const IDict& Calculation::getI(){return I;}

const reIDict& Calculation::getresultI(){return reI; }

const ZDict& Calculation::getZLoad(){return ZLoad;  }

const ZDict& Calculation::getZGener(){return  ZGener;}

const ZDict& Calculation::getZBranch(){return ZBranch;}

const Matrix& Calculation::getMatrix(){return smatrix;}

const Matrix& Calculation::getUsualMatrix(){return usualMatrix;}

Calculation &Calculation::invers()
{
    iprint = false;
    zprint = false;
    smatrix=invMatrix;
    return *this;
}

Calculation &Calculation::usual()
{
    iprint = false;
    zprint = false;
    smatrix=usualMatrix;
    return *this;
}

Calculation &Calculation::ZPrint()
{
    zprint = true;
    iprint = false;
    return *this;
}

Calculation &Calculation::IPrint()
{
    iprint = true;
    zprint = false;
    return *this;
}

QMap<QString, std::complex<double> > Calculation::getZ(){return ZBranch;}

void Calculation::inverse()
{
    int N=smatrix.length();
    complexnum **matrix = new complexnum *[N];
    for (int i = 0; i < N; i++)
        matrix[i] = new complexnum [N];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            matrix[i][j] = smatrix[i][j];

    complexnum temp;
    complexnum **E = new complexnum *[N];
    for (int i = 0; i < N; i++)
        E[i] = new complexnum [N];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            E[i][j] = 0.0;
            if (i == j)
                E[i][j] = 1.0;
        }
    for (int k = 0; k < N; k++)
    {
        temp = matrix[k][k];
        for (int j = 0; j < N; j++)
        {
            matrix[k][j] /= temp;
            E[k][j] /= temp;
        }
        for (int i = k + 1; i < N; i++)
        {
            temp = matrix[i][k];
            for (int j = 0; j < N; j++)
            {
                matrix[i][j] -= matrix[k][j] * temp;
                E[i][j] -= E[k][j] * temp;
            }
        }
    }
    for (int k = N - 1; k > 0; k--)
    {
        for (int i = k - 1; i >= 0; i--)
        {
            temp = matrix[i][k];
            for (int j = 0; j < N; j++)
            {
                matrix[i][j] -= matrix[k][j] * temp;
                E[i][j] -= E[k][j] * temp;
            }
        }
    }
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            matrix[i][j] = E[i][j];
    for (int i = 0; i < N; i++)
        delete [] E[i];
    delete [] E;

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            smatrix[i][j] = matrix[i][j];

    for (int i = 0; i < N; i++)
        delete [] matrix[i];
    delete [] matrix;

    invMatrix=smatrix;
}

void Calculation::Zcalk(Layout *layout)
{
    complexnum z;
    QString trans;
    for (linksDict::Iterator p = layout->branchs.begin();p!=layout->branchs.end();++p)
    {
        if (p.key().value(0)->isGener() || p.key().value(1)->isGener())
        {
            int gener;
            int vertex;
            if (p.key()[0]->isGener())
            {
                gener = 0;
                vertex =1;
            }
            else
            {
                gener = 1;
                vertex = 0;
            }

            z = pow(p.key()[gener]->getCondacton(p.key()[vertex]),2)/
                    (p.key()[gener]->getCondacton(p.key()[vertex]) +
                    invMatrix[p.key()[vertex]->getId()-1][p.key()[vertex]->getId()-1]);
            ZGener.insert(p.key()[gener]->getIndex()+"->"+p.key()[vertex]->getIndex(), z);
            I[p.key()]<<p.key()[gener]->getVoltage()/z;

            for (int i=1;i<layout->getQuanityGener()+1;i++)
            {
                if (layout->getGener(i)==p.key()[gener]) continue;
                for (branchDict::iterator l = layout->getGener(i)->branchs.begin();l!=layout->getGener(i)->branchs.end();++l)
                {
                    z = (layout->getGener(i)->getCondacton(l.key()) * p.value())/
                            invMatrix[p.key()[vertex]->getId()-1][l.key()->getId()-1];
                    ZGener.insert(layout->getGener(i)->getIndex()+"["+l.key()->getIndex()+"]"+";"+p.key()[vertex]->getIndex(), z);
                    I[p.key()]<<layout->getGener(i)->getVoltage()/z;
                }
            }
        }
        else
        {
            for (int i=1;i<layout->getQuanityGener()+1;i++)
            {
                for (branchDict::iterator l = layout->getGener(i)->branchs.begin();l!=layout->getGener(i)->branchs.end();++l)
                {
                    z = (layout->getGener(i)->getCondacton(l.key())*p.key()[0]->getCondacton(p.key()[1]))/
                            (invMatrix[p.key()[0]->getId()-1][layout->getId(l.key())-1] - invMatrix[p.key()[1]->getId()-1][l.key()->getId()-1]);
                    ZBranch.insert(layout->getGener(i)->getIndex()+"["+l.key()->getIndex()+"]"+";"+ p.key()[0]->getIndex()+"->"+p.key()[1]->getIndex(), z);
                    I[p.key()]<<layout->getGener(i)->getVoltage()/z;
                }
            }
        }
    }

    for (nodeDict::iterator v=layout->vertexes.begin();v!=layout->vertexes.end();++v)
    {
        if (v.key()->getLoads().contains(2))
        {
            for (loadDict::iterator l=v.key()->loads.begin();l!=v.key()->loads.end();++l)
            {
                for (int i=1;i<layout->getQuanityGener()+1;i++)
                {
                    for (branchDict::iterator b = layout->getGener(i)->branchs.begin();b!=layout->getGener(i)->branchs.end();++b)
                    {
                        QString trans;
                        z = (layout->getGener(i)->getCondacton(b.key())*l.value())/
                                invMatrix[v.key()->getId()-1][v.key()->getId()-1];
                        ZLoad.insert(layout->getGener(i)->getIndex()+"["+b.key()->getIndex()+"]"+";"+v.key()->getIndex()+"load["+trans.setNum(l.key())+"]", z);
                        QVector<Node*> temp;temp<<v.key();
                        I[temp]<<layout->getGener(i)->getVoltage()/z;
                    }
                }
            }
        }
        else
        {
            for (int i=1;i<layout->getQuanityGener()+1;i++)
            {
                for (branchDict::iterator b = layout->getGener(i)->branchs.begin();b!=layout->getGener(i)->branchs.end();++b)
                {
                    z = (layout->getGener(i)->getCondacton(b.key())*v.key()->getLoad())/
                            invMatrix[v.key()->getId()-1][b.key()->getId()-1];
                    ZLoad.insert(layout->getGener(i)->getIndex()+"["+b.key()->getIndex()+"]"+";"+v.key()->getIndex()+"load", z);
                    QVector<Node*> temp;temp<<v.key();
                    I[temp]<<layout->getGener(i)->getVoltage()/z;
                }
            }
        }
    }
}

void Calculation::Icalc(Layout *layout)
{
    for (IDict::iterator i=I.begin();i!=I.end();i++)
    {
       if (i.key().size()>1)
           for (auto j : i.value())
               reI[i.key()[0]->getIndex()+"->"+i.key()[1]->getIndex()] += j;
       else
           for (auto j : i.value())
           {QString trans;
               if (i.key()[0]->getLoads().contains(2))
                   for (loadDict::iterator te=i.key()[0]->loads.begin();te!=i.key()[0]->loads.end();te++)
                       for (int it=0;it<layout->getQuanityGener();it++)
                            reI[i.key()[0]->getIndex()+"load["+trans.setNum(te.key())+"]"] += j;
               else
                   reI[i.key()[0]->getIndex()+"load"] += j;
           }
    }
}

void Calculation::ZC(Layout &layout)
{
    for (int i=1; i<=layout.getQuanityVertex();++i)
    {
        QMap<QString, complexnum> map;
        for (generDict::iterator gen=layout.geners.begin();gen!=layout.geners.end();++gen)
        {
            if (layout[i]->isLink(gen.key()))
            {
                map.insert(z_name_E(gen.key()), z(*gen.key()));
                for (generDict::iterator mutualgen=layout.geners.begin();mutualgen!=layout.geners.end();++mutualgen)
                {
                    if (gen==mutualgen) continue;
                    map.insert(z_name_mutualE(gen.key(),mutualgen.key()), z(*gen.key(),*mutualgen.key()));
                }
                for (branchDict::iterator link=layout[i]->branchs.begin();link!=layout[i]->branchs.end();++link)
                    if(!link.key()->isGener())
                        map.insert(z_name_branch(layout[i], link.key(), gen.key()), z(*layout[i], *link.key(), *gen.key()));
            }
            else
                for (branchDict::iterator link=layout[i]->branchs.begin();link!=layout[i]->branchs.end();++link)
                    if(!link.key()->isGener())
                        map.insert(z_name_branch(layout[i], link.key(), gen.key()), z(*layout[i], *link.key(), *gen.key()));
            if (layout[i]->getSumLoads()!=complexnum(0))
            {
                if (layout[i]->quanityLoad==1)
                    map.insert(z_name_load(layout[i],gen.key()),z(*layout[i],*gen.key()));
                else
                    for (loadDict::iterator load=layout[i]->loads.begin();load!=layout[i]->loads.end();++load)
                        if (load.value()!=complexnum(0))
                            map.insert(z_name_load(layout[i],gen.key(),load.key()),z(*layout[i],*gen.key(),load.key()));
            }
        }
        Z<<map;
    }
}

void Calculation::IC(Layout &layout)
{
    for (auto vec : Z)
    {
        QMap<QString, complexnum> map;
        for(QMap<QString, complexnum>::iterator m=vec.begin();m!=vec.end();++m)
        {
            int i=0;
            QString index;
            for(i=m.key().size()-1;i!=-1;i--)
                if (m.key()[i]==';' || m.key()[i]==',')
                    break;
            for(int j=i+1;j<m.key().size()-1;j++)
                index+=m.key()[j];
            complexnum ifirst;
            ifirst = layout.getGener(index)->getVoltage()/m.value();
            index = 'I';
            for (int j=1;j<m.key().size();j++)
                index += m.key()[j];
            map.insert(index,ifirst);
        }
        I_first<<map;
    }

    for (auto vec : I_first)
    {
        QMap<QString, complexnum> map;
        for(QMap<QString, complexnum>::iterator m=vec.begin();m!=vec.end();++m)
        {
            int i=0;
            QString index,indexgen;
            for(i=2;i<m.key().size();i++)
            {
                if (m.key()[i]==';' || m.key()[i]==',')
                    break;
                index += m.key()[i];
            }
            for(int j=i+1;j<m.key().size()-1;j++)
                indexgen+=m.key()[j];
            map["I("+index+')'] += m.value();
        }
        I_end<<map;
    }
}

complexnum Calculation::z(Gener &gen)
{
    return
                    pow(gen.getCondacton(),2)
                                /
   (gen.getCondacton() + invMatrix[gen.getMatrIndexLink()][gen.getMatrIndexLink()]);
}

complexnum Calculation::z(Gener &gen, Gener &mutualgen)
{
    return
            (gen.getCondacton() * mutualgen.getCondacton())
                                /
     invMatrix[gen.getMatrIndexLink()][mutualgen.getMatrIndexLink()];
}

complexnum Calculation::z(Node &ver, Gener &mutualgen, int key)
{
    return
            (ver.getLoad(key) * mutualgen.getCondacton())
                                /
     invMatrix[ver.getId()-1][mutualgen.getMatrIndexLink()];
}

complexnum Calculation::z(Node &ver, Node &linkver, Gener &mutualgen)
{
    return
            (ver.getCondacton(&linkver) * mutualgen.getCondacton())
                                /
(invMatrix[ver.getId()-1][mutualgen.getMatrIndexLink()] - invMatrix[linkver.getId()-1][mutualgen.getMatrIndexLink()]);
}

QString Calculation::z_name_E(const Gener *gen)
{
    QString s_gen = gen->index;
    return "Z("+s_gen+','+s_gen+')';
}

QString Calculation::z_name_mutualE(const Gener *gen, const Gener *mutualgen)
{
    QString s_gen= gen->index;
    QString s_mutualgen= mutualgen->index;
    return "Z("+s_gen+','+s_mutualgen+')';
}

QString Calculation::z_name_load(const Node *linkver, const Gener *mutualgen, const int key)
{
    QString s_linkver = linkver->index;
    QString s_mutualgen= mutualgen->index;
    QString s_key; s_key = s_key.setNum(key);
    if (key==-1)
        return "Z("+s_linkver+"0;"+s_mutualgen+')';
    return "Z("+s_linkver+'['+s_key+']'+"0;"+s_mutualgen+')';
}

QString Calculation::z_name_branch(const Node *ver, const Node *linkver, const Gener *mutualgen)
{
    QString s_ver = ver->index;
    QString s_linkver = linkver->index;
    QString s_mutualgen= mutualgen->index;
    return "Z("+s_ver+"->"+s_linkver+";"+s_mutualgen+')';
}



const Matrix& Calculation::getInvMatrix(){return invMatrix;}

std::ostream &operator<<(std::ostream &output, Calculation &obj)
{
    if (!obj.zprint && !obj.iprint)
    {
        for (auto p : obj.getMatrix())
        {
            for (std::complex<double> p2 : p)
                output<<p2;
            output<<std::endl;
        }
    }
    else if(obj.zprint)
    {int k=0;
        output<<"Generator branchs:"<<std::endl;
        for (ZDict::iterator p=obj.ZGener.begin();p!=obj.ZGener.end();++p)
            output<<"    "<<++k<<": Z "<<p.key().toStdString()<<" = "<<p.value()<<std::endl;
        output<<"Network branchs:"<<std::endl;
        for (ZDict::iterator p=obj.ZBranch.begin();p!=obj.ZBranch.end();++p)
            output<<"    "<<++k<<": Z "<<p.key().toStdString()<<" = "<<p.value()<<std::endl;
        output<<"Load branchs:"<<std::endl;
        for (ZDict::iterator p=obj.ZLoad.begin();p!=obj.ZLoad.end();++p)
            output<<"    "<<++k<<": Z "<<p.key().toStdString()<<" = "<<p.value()<<std::endl;
    }
    else if (obj.iprint)
    {int k=0;
        for (reIDict::iterator p=obj.reI.begin();p!=obj.reI.end();++p)
            output<<++k<<": I "<<p.key().toStdString()+" = "<<p.value()<<std::endl;

    }
    return output;
}


