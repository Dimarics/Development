#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "schemegraphics.h"
#include <QMainWindow>
#include <QButtonGroup>
#include <QProcess>
#include "vertex.h"
#include "calculation.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget* = 0);

private:
    QButtonGroup *modeButtonGroup;
    SchemeGraphics *schemeGraphics;
    Ui::MainWindow *ui;

protected:
    void mousePressEvent(QMouseEvent*);
    void keyPressEvent(QKeyEvent*);

private slots:
    void setMode(QAbstractButton*, bool);
    void calculateScheme();
    void findItem(Wire*);
};
#endif // MAINWINDOW_H
