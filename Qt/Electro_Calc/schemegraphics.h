#ifndef SCHEMEGRAPHICS_H
#define SCHEMEGRAPHICS_H

#include "schemeitem.h"

#include <QGraphicsScene>
#include <QGraphicsRectItem>

class SchemeGraphics : public QGraphicsScene
{
    Q_OBJECT
public:
    enum Mode
    {
        None,
        Grab,
        Select,
        InsertGenerator,
        InsertKnot,
        InsertResistor,
        InsertWire,
        InsertLoad,
    };
    bool mouse_pressed;
    Mode mode;
    QList<SchemeItem*> generators;
    QList<SchemeItem*> knots;

    SchemeGraphics();
    void setMode(Mode);

private:
    const quint16 sceneSize = 8060;
    const quint16 grid_step = 26;

    SchemeItem *insertedItem;
    QPointF gridPoint(const QPointF&);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    void mouseMoveEvent(QGraphicsSceneMouseEvent*);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*);

public slots:
    void clearScene();
    void removeSelectedItems();
    void turn();
};
#endif // SCHEMEGRAPHICS_H
