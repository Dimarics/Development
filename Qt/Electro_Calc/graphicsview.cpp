#include "graphicsview.h"
#include "schemegraphics.h"

#include <QGraphicsItem>
#include <QWheelEvent>

GraphicsView::GraphicsView(QWidget *parent) : QGraphicsView(parent)
{
    setMouseTracking(true);
    setScene(new SchemeGraphics);
    //setCacheMode(QGraphicsView::CacheBackground);
    centerOn(sceneRect().width() / 2, sceneRect().height() / 2);
    showGrid(true);
}

/*void GraphicsView::setChoiseMode(bool checked)
{
    if (checked)
    {
        setDragMode(QGraphicsView::NoDrag);
        static_cast<SchemeGraphics*>(scene())->setMode(SchemeGraphics::None);
    }
}

void GraphicsView::setGrabMode()
{
    setDragMode(QGraphicsView::ScrollHandDrag);
    scene()->clearSelection();
    static_cast<SchemeGraphics*>(scene())->setMode(SchemeGraphics::None);
    for (QGraphicsItem* item : scene()->items())
    {
        if (item->type() > QGraphicsItem::UserType)
        {
            item->setFlag(QGraphicsItem::ItemIsSelectable, false);
            item->setFlag(QGraphicsItem::ItemIsMovable, false);
        }
    }
}

void GraphicsView::setSelectMode()
{
    setDragMode(QGraphicsView::RubberBandDrag);
    static_cast<SchemeGraphics*>(scene())->setMode(SchemeGraphics::None);
    for (QGraphicsItem* item : scene()->items())
    {
        if (item->type() > QGraphicsItem::UserType)
        {
            item->setFlag(QGraphicsItem::ItemIsSelectable);
            item->setFlag(QGraphicsItem::ItemIsMovable, false);
        }
    }
}*/

void GraphicsView::showGrid(bool visible) {
    visible ? setBackgroundBrush(QPixmap(":/res/grid.png")) : setBackgroundBrush(Qt::NoBrush);
}

void GraphicsView::wheelEvent(QWheelEvent *)
{
    //event->angleDelta().y() > 0 ? scale(0.9, 0.9) : scale(1.1, 1.1);
    //qDebug() << transform().m11() << transform().m22();
}
