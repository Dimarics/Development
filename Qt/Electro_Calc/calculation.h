#ifndef CALCULATION_H
#define CALCULATION_H

#include <complex>
#include <QVector>
#include "vertex.h"
#include <ostream>
#include <QMap>
//#include "inverse.h"


typedef QVector<QVector<std::complex<double>>> Matrix;
typedef QMap<QString, std::complex<double>> ZDict;
typedef QMap<QVector<Node*>, QVector<std::complex<double>>> IDict;
typedef QMap<QString, std::complex<double>> reIDict;

typedef QVector<QMap<QString, complexnum>> zVertex;
typedef QVector<QMap<QString, complexnum>> iVertex;

class Calculation final
{
public:
    Calculation(Layout* layout, bool det=true);
    const reIDict& getresultI();
    const IDict& getI();
    const ZDict& getZLoad();
    const ZDict& getZGener();
    const ZDict& getZBranch();
    const Matrix& getMatrix();
    const Matrix& getInvMatrix();
    const Matrix& getUsualMatrix();
    Calculation& invers();
    Calculation& usual();
    Calculation& ZPrint();
    Calculation& IPrint();
    QMap<QString, std::complex<double>> getZ();
    const iVertex& getI_first(){return I_first;}
    const iVertex& getI_end(){return I_end;}
    const zVertex& getZ2(){return Z;}
    friend std::ostream& operator<< (std::ostream& output, Calculation& obj);
private:
    bool iprint;
    bool zprint;
    void inverse();
    void Zcalk(Layout* layout);
    void Icalc(Layout* layout);
    void ZC(Layout& layout);
    void IC(Layout& layout);

    complexnum z(QString index);

    complexnum z(Gener& gen);
    complexnum z(Gener& gen, Gener& mutualgen);
    complexnum z(Node& ver, Gener& mutualgen, int key = 1);
    complexnum z(Node& ver, Node& linkver, Gener& mutualgen);

    QString z_name_E(const Gener* gen);
    QString z_name_mutualE(const Gener* gen, const Gener* mutualgen);
    QString z_name_load(const Node* linkver,const Gener* mutualgen,const int key = -1);
    QString z_name_branch(const Node* ver,const Node* linkver,const Gener* mutualgen);

    Matrix usualMatrix;
    Matrix invMatrix;
    Matrix smatrix;
    ZDict ZBranch;
    ZDict ZGener;
    ZDict ZLoad;
    reIDict reI;
    IDict I;
    iVertex I_first, I_end;
    zVertex Z;
    //QVector<> zVertex;
};

#endif // CALCULATION_H
