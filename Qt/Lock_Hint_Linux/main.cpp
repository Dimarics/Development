#include <QApplication>
#include <QIcon>
#include "window.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow window;

    window.setWindowIcon(QPixmap(":/res/icon.png"));
    window.show();

    return app.exec();
}
