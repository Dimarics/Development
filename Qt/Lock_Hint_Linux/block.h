#ifndef BLOCK_H
#define BLOCK_H

#include <QWidget>
#include <QFocusEvent>
#include <QPainter>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QTimer>
#include <QProxyStyle>
#include "passwordline.h"

class BlockMenu : public QWidget
{
    Q_OBJECT

public:
    PasswordLine *string;
    QPushButton *btn_vision;
    BlockMenu(QWidget *parent = 0);

private:
    QLabel *hint;
    QLabel *layout;
    QLabel *passwordError;
    QTimer *errTime;
    void paintEvent(QPaintEvent *event);

public slots:
    void switchLayout(bool);

private slots:
    void enterPassword();
    void textDeselect();
    void toolTip(bool);

signals:
    void block(bool);
};

class NoCursorStyle : public QProxyStyle
{
public:
    virtual int pixelMetric(PixelMetric metric, const QStyleOption* option, const QWidget* widget) const
    {
        if (metric == QStyle::PM_TextCursorWidth)
            return 0;
        return QProxyStyle::pixelMetric(metric, option, widget);
    }
};

#endif // BLOCK_H
