#ifndef PASSEDIT_H
#define PASSEDIT_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QPainter>
#include "passwordline.h"

class PasswordEdit : public QWidget
{
    Q_OBJECT

public:
    QPushButton *btn_ok;
    QPushButton *btn_close;
    PasswordLine *passwordLine;
    PasswordEdit(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // PASSEDIT_H
