#include "passwordline.h"

PasswordLine::PasswordLine(QWidget *parent) : QLineEdit (parent)
{
    setMaxLength(26);
    setFocus();
    showPassword(false);
}

void PasswordLine::showPassword(bool show)
{
    if (show)
    {
        setFont(QFont("Arial", 18));
        setEchoMode(QLineEdit::Normal); 
    }
    else
    {
        setFont(QFont("Arial", 32));
        setEchoMode(QLineEdit::Password);
    }
}

void PasswordLine::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    if (this->isVisible())
        setFocus();
}
