#include "settings.h"

const char *const Style::closeBtn =
        "QPushButton {"
        //"outline: none;"
        "background-color: rgba(0, 0, 0, 0);"
        "icon: url(:/res/cross.png);"
        "border: none;"
        "}"
        "QPushButton:hover {"
        "background-color: rgb(255, 50, 50);"
        "}"
        "QPushButton:pressed {"
        "background-color: rgba(255, 100, 100, 220)"
        "}"
        "QToolTip {"
        "background-color: rgb(65, 65, 65);"
        "color: white;"
        "border: 1px solid gray"
        "}";

const char *const Style::foldBtn =
        "QPushButton {"
        "background-color: rgba(0, 0, 0, 0);"
        "icon: url(:/res/showBtn.png);"
        "border: none;"
        "}"
        "QPushButton:hover {"
        "background-color: rgba(255, 255, 255, 30);"
        "}"
        "QPushButton:pressed {"
        "background-color: rgba(255, 255, 255, 50)"
        "}"
        "QToolTip {"
        "background-color: rgb(65, 65, 65);"
        "color: white;"
        "border: 1px solid gray"
        "}";

const char *const Style::visionBlockPassword =
        "QPushButton {"
        "background-color: rgb(160, 160, 160);"
        "image: url(:/res/passwordEditShow.png);"
        "icon-size: 45px;"
        "border: 2px groove gray;"
        "}"
        "QPushButton:hover {"
        "background-color: rgb(140, 140, 140);"
        "}"
        "QPushButton:checked {"
        "image: url(:/res/passwordEditClose.png);"
        "}"
        "QToolTip {"
        "background-color: rgb(65, 65, 65);"
        "color: white;"
        "border: 1px solid gray"
        "}";

const char *const Style::stringPassword =
        "QLineEdit {"
        "background-color: rgb(160, 160, 160);"
        "selection-color: rgb(50, 50, 50);"
        "color: rgb(50, 50, 50);"
        "border: 2px groove gray;"
        "}";

const char *const Style::blockHint =
        "QLabel {"
        "color: rgb(210, 210, 105);"
        "font: 14pt \"Arial\";"
        "}";

const char *const Style::passwordError =
        "QLabel {"
        "color: rgb(255, 20, 20);"
        "font: 15pt \"Arial\";"
        "}";

const char *const Style::layoutFirst =
        "QLabel {"
        "border: 2px groove gray;"
        "}";

const char *const Style::panelBtn =
        "QPushButton {"
        "background-color: rgba(33, 40, 48, 255);"
        "border: 2px groove gray;"
        "}"
        "QPushButton:hover {"
        "background-color: rgba(255, 255, 255, 40);"
        "}"
        "QPushButton:pressed {"
        "background-color: rgba(255, 255, 255, 70)"
        "}"
        "QPushButton::menu-indicator {"
        "image: none;"
        "}"
        "QToolTip {"
        "background-color: rgb(65, 65, 65);"
        "color: white;"
        "border: 1px solid gray"
        "}";

const char *const Style::contextMenu =
        "QMenu {"
        "background-color: rgb(60, 68, 84);"
        "color: white;"
        "font: 14pt \"Arial\";"
        "border: 1px solid rgb(120, 120, 130);"
        "padding-top: 3px;"
        "}"
        "QMenu::item:selected {"
        "background-color: rgba(255, 255, 255, 70);"
        "}"
        "QMenu::separator {"
        "color: white;"
        "}";

const char *const Style::searchBar =
        "QLineEdit {"
        "background-color: rgb(33, 40, 48);"
        "color: white;"
        "padding-top: 1px;"
        "padding-left: 44px;"
        "padding-right: 32px;"
        "border: 2px groove gray;"
        "font: 15pt \"Arial\";"
        "}";

const char *const Style::clearTextBtn =

        "QPushButton {"
        "background-color: rgb(160, 160, 160);"
        "icon: url(:/res/texClearBtn.png);"
        "icon-size: 9px;"
        "border-radius: 9px;"
        "}"
        "QPushButton:hover {"
        "background-color: rgb(200, 200, 200);"
        "}";

const char *const Style::layoutSecond =
        "QLabel {"
        "border: 2px groove gray;"
        "}";

const char *const Style::table =
        "QTableWidget {"
        "background-color: rgba(0, 0, 0, 0);"
        "color: white;"
        "font: 15pt \"Arial\";"
        "gridline-color: rgb(150, 150, 150);"
        "border: 1px solid rgb(150, 150, 150);"
        "selection-background-color: rgba(255, 255, 255, 30);"
        "}"
        "QTableWidget::item {"
        "border: 0px;"
        "padding-top: 2px;"
        "padding-left: 15px;"
        "}";

const char *const Style::tableHeader =
        "QHeaderView::section {"
        "background-color: rgba(33, 40, 48, 255);"
        "color: rgb(156, 220, 254);"
        "font: 15pt \"Arial\";"
        "padding-top: 5px;"
        "padding-right: 150px;"
        "border-top: none;"
        "border-left: none;"
        "border-right: 1px solid rgb(150, 150, 150);"
        "border-bottom: 1px solid rgb(150, 150, 150);"
        "}";

const char *const Style::tableScrollBar =
        "QScrollBar::vertical {"
        "background-color: rgba(0, 0, 0, 0);"
        "border: none;"
        "margin: 19px 0 19px 0;"
        "}"
        "QScrollBar::handle:vertical {"
        "background-color: rgb(60, 68, 83);"
        "border: none;"
        "}"
        "QScrollBar::sub-line:vertical {"
        "background-color: rgb(60, 68, 83);"
        "border-top: none;"
        "border-left: none;"
        "border-right: none;"
        "border-bottom: 1px solid rgb(150, 150, 150);"
        "height: 18px;"
        "subcontrol-position: top;"
        "subcontrol-origin: margin;"
        "}"
        "QScrollBar::add-line:vertical {"
        "background-color: rgb(60, 68, 83);"
        "border-top: 1px solid rgb(150, 150, 150);"
        "border-left: none;"
        "border-right: none;"
        "border-bottom: none;"
        "height: 18px;"
        "subcontrol-position: bottom;"
        "subcontrol-origin: margin;"
        "}"
        "QScrollBar::up-arrow:vertical {"
        "image: url(:/res/up_arrow.png);"
        "}"
        "QScrollBar::down-arrow:vertical {"
        "image: url(:/res/down_arrow.png);"
        "}"
        "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {"
        "background: none;"
        "}";

const char *const Style::okBtn =
        "QPushButton {"
        "background-color: rgb(50, 57, 70);"
        "color: white;"
        "font: 17pt \"Arial\";"
        "border: 1px solid rgb(150, 150, 150);"
        "border-radius: 5px;"
        "padding-right: 3px;"
        "padding-bottom: 5px;"
        "}"
        "QPushButton:hover {"
        "background-color: rgb(100, 114, 140);"
        "}";

const char *const Style::editLine =
        "QLineEdit {"
        "background-color: rgb(43, 50, 60);"
        "color: white;"
        "font: 15pt \"Arial\";"
        "border: 1px solid rgb(150, 150, 150);"
        "border-radius: 7px;"
        "padding-left: 15px;"
        "padding-top: 1px;"
        "}";

const char *const Style::editPasswordHint =
        "QLabel {"
        "color: rgb(156, 220, 254);"
        "font: 17pt \"Arial\";"
        "}";

const char *const Style::editPasswordString =
        "QLineEdit {"
        "background-color: rgb(180, 180, 180);"
        "color: rgb(45, 45, 45);"
        "border: 2px groove gray;"
        "border-top-left-radius: 9px;"
        "border-bottom-left-radius: 9px;"
        "padding-left: 15px;"
        "}";

const char *const Style::visionEditPassword =
        "QPushButton {"
        "background-color: rgb(180, 180, 180);"
        "image: url(:/res/passwordEditShow.png);"
        "border: 2px groove gray;"
        "border-top-right-radius: 9px;"
        "border-bottom-right-radius: 9px;"
        "}"
        "QPushButton:hover {"
        "background-color: rgb(140, 140, 140);"
        "}"
        "QPushButton:checked {"
        "image: url(:/res/passwordEditClose.png);"
        "}";

const char *const Style::manual =
        "QTextBrowser {"
        "background-color: rgb(33, 40, 48);"
        "color: white;"
        "font: 15pt \"Arial\";"
        "margin: 15px 100px 20px 100px;"
        "border: none;"
        "}";

const char *const Style::backBtn =
        "QPushButton {"
        "background-color: rgba(0, 0, 0, 0);"
        "color: rgb(175, 175, 175);"
        "font: 13pt \"Arial\";"
        "text-align: right;"
        "padding-right: 15px;"
        "border: none;"
        "}"
        "QPushButton:hover {"
        "background-color: rgba(255, 255, 255, 30);"
        "}"
        "QPushButton:pressed {"
        "background-color: rgba(255, 255, 255, 50)"
        "}";
