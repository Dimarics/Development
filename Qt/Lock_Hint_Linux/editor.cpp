#include "editor.h"
#include "settings.h"

EditWidget::EditWidget(QWidget *parent) : QWidget (parent)
{
    QPushButton *btn_close = new QPushButton(this);
    btn_close->setStyleSheet(Style::closeBtn);
    btn_close->setGeometry(642, 0, 28, 28);
    connect(btn_close, &QPushButton::clicked, this, &QWidget::close);

    btn_ok = new QPushButton(this);
    btn_ok->setStyleSheet(Style::okBtn);
    btn_ok->setShortcut(tr("Enter"));
    btn_ok->setText("ок");
    btn_ok->setGeometry(298, 340, 74, 32);

    service_enter = new QLineEdit(this);
    service_enter->setStyleSheet(Style::editLine);
    service_enter->setGeometry(160, 50, 448, 40);
    QLabel *service_txt = new QLabel(this);
    service_txt->setAlignment(Qt::AlignmentFlag::AlignRight);
    service_txt->setStyleSheet("color: white; font: 15pt \"Arial\";");
    service_txt->setText("Сервис:");
    service_txt->setGeometry(27, 57, 100, 38);

    login_enter = new QLineEdit(this);
    login_enter->setStyleSheet(Style::editLine);
    login_enter->setGeometry(160, 125, 448, 40);
    QLabel *login_txt = new QLabel(this);
    login_txt->setAlignment(Qt::AlignmentFlag::AlignRight);
    login_txt->setStyleSheet("color: white; font: 15pt\"Arial\";");
    login_txt->setText("Логин:");
    login_txt->setGeometry(27, 132, 100, 38);

    password_enter = new QLineEdit(this);
    password_enter->setStyleSheet(Style::editLine);
    password_enter->setGeometry(160, 200, 448, 40);
    QLabel *password_txt = new QLabel(this);
    password_txt->setAlignment(Qt::AlignmentFlag::AlignRight);
    password_txt->setStyleSheet("color: white; font: 15pt \"Arial\";");
    password_txt->setText("Пароль:");
    password_txt->setGeometry(27, 207, 100, 38);

    link_enter = new QLineEdit(this);
    link_enter->setStyleSheet(Style::editLine);
    link_enter->setGeometry(160, 275, 448, 40);
    QLabel *link_txt = new QLabel(this);
    link_txt->setAlignment(Qt::AlignmentFlag::AlignRight);
    link_txt->setStyleSheet("color: white; font: 15pt \"Arial\";");
    link_txt->setText("Ссылка:");
    link_txt->setGeometry(27, 282, 100, 38);

    setTabOrder(service_enter, link_enter);
    setTabOrder(login_enter, service_enter);
    setTabOrder(password_enter, login_enter);
    setTabOrder(link_enter, password_enter);
}

void EditWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QColor(120, 120, 130, 255)); //контур виджета
    painter.setBrush(QColor(60, 68, 84, 255)); //фон виджета
    painter.drawRect(QRect(0, 0, width() - 1, height() - 1));
}

void EditWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Down)
    {
        if (service_enter->hasFocus())
            login_enter->setFocus();
        else if (login_enter->hasFocus())
            password_enter->setFocus();
        else if (password_enter->hasFocus())
            link_enter->setFocus();
    }
    else if (event->key() == Qt::Key_Up)
    {
        if (link_enter->hasFocus())
            password_enter->setFocus();
        else if (password_enter->hasFocus())
            login_enter->setFocus();
        else if (login_enter->hasFocus())
            service_enter->setFocus();
    }
}
