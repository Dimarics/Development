#include <QtAndroidExtras/QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QPainter>
#include "menuwidget.h"

#include <QDebug>

MenuWidget::MenuWidget(QWidget *parent) : QWidget(parent)
{
    setAttribute(Qt::WA_TranslucentBackground);
    resize(parent->size());
    button = new QPushButton(this);
    button->setGeometry(width() - 402, 40, 362, 132);
    button->setText("Настройки");
    connect(button, &QPushButton::clicked, this, &MenuWidget::openConfigFolder);
}

void MenuWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter paint(this);
    paint.setPen (Qt::NoPen);
    paint.setBrush(QColor(0, 0, 0, 75));
    paint.drawRect(0, 0, width(), height());
}

void MenuWidget::setTheme(const QString &colors)
{
    const QString style("border-radius: 35px; padding-left: 40px; font-size: 17pt; text-align: left;");
    button->setStyleSheet(style + colors);
}

void MenuWidget::openConfigFolder()
{
    QAndroidJniObject action = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Intent", "ACTION_VIEW");
    QAndroidJniObject intent("android/content/Intent","(Ljava/lang/String;)V", action.object<jstring>());
    QAndroidJniObject path = QAndroidJniObject::fromString("/storage/emulated/0/Chronometer");
    QAndroidJniObject uri = QAndroidJniObject::callStaticObjectMethod("android/net/Uri", "parse", "(Ljava/lang/String;)Landroid/net/Uri;", path.object<jstring>());
    QAndroidJniObject mime_type = QAndroidJniObject::fromString("vnd.android.document/directory");
    //QAndroidJniObject documentProvider("android/provider/DocumentsContract$Document");
    //QAndroidJniObject mime_type = QAndroidJniObject::getStaticObjectField<jstring>("android/provider/DocumentsContract$Document", "MIME_TYPE_DIR");
    intent.callObjectMethod("setDataAndType", "(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;", uri.object(), mime_type.object<jstring>());
    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject packageManager = activity.callObjectMethod("getPackageManager","()Landroid/content/pm/PackageManager;");
    QAndroidJniObject componentName = intent.callObjectMethod("resolveActivity", "(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;", packageManager.object());
    if (componentName.isValid())
        QtAndroid::startActivity(intent, 0);
}

void MenuWidget::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    close();
}
