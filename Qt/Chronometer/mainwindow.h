#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QLabel>
#include "menuwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Chronometer; }
QT_END_NAMESPACE

class Chronometer : public QMainWindow
{
    Q_OBJECT
public:
    Chronometer(QWidget *parent = 0);
    ~Chronometer();

private:
    float rate = 1;
    int startTime;
    int mainTime;
    QTimer *tmr;
    QLabel *display;
    MenuWidget *menuWidget;
    Ui::Chronometer *ui;
    void writeConfig(const QString&);
    const char *const darkButton =
            "QPushButton {"
            "border-radius: 56px;"
            "background-color: rgb(36, 36, 36);"
            "}"
            "QPushButton:pressed {"
            "background-color: rgb(55, 55, 55);"
            "}";
    const char *const lightButton =
            "QPushButton {"
            "border-radius: 56px;"
            "background-color: rgb(212, 212, 212);"
            "}"
            "QPushButton:pressed {"
            "background-color: rgb(188, 188, 188);"
            "}";

private slots:
    void startPause(bool);
    void runTime();
    void setFlag();
    void stop();
    void setTheme(bool);
    void openMenu();
};
#endif // MAINWINDOW_H
