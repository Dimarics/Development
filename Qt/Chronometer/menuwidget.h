#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QPaintEvent>
#include <QMouseEvent>

class MenuWidget : public QWidget
{
    Q_OBJECT

public:
    MenuWidget(QWidget *parent = 0);

private:
    QPushButton *button;
    void paintEvent(QPaintEvent*);
    void mousePressEvent(QMouseEvent*);
public slots:
    void setTheme(const QString&);

private slots:
    void openConfigFolder();
};

#endif // MENUWIDGET_H
