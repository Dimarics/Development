#include <QScreen>
#include <QFontDatabase>
#include <QHeaderView>
#include <QScroller>
#include <QTime>
#include <QFile>
#include <QDir>
#include <QTextStream>
//#include <QDebug>

#include "mainwindow.h"
#include "ui_mainwindow.h"

Chronometer::Chronometer(QWidget *parent) : QMainWindow(parent), ui(new Ui::Chronometer)
{
    ui->setupUi(this);
    ui->flagWidget->close();
    ui->flagButton->close();
    ui->stopButton->close();
    display = ui->baseDisplay;
    resize(QWidget::screen()->availableGeometry().size());
    connect(ui->startPauseButton, &QPushButton::toggled, this, &Chronometer::startPause);
    connect(ui->flagButton, &QPushButton::clicked, this, &Chronometer::setFlag);
    connect(ui->stopButton, &QPushButton::clicked, this, &Chronometer::stop);
    connect(ui->styleButton, &QPushButton::toggled, this, &Chronometer::setTheme);
    connect(ui->menuButton, &QPushButton::clicked, this, &Chronometer::openMenu);

    ui->tableWidget->horizontalHeader()->resizeSection(0, 190);
    ui->tableWidget->horizontalHeader()->resizeSection(1, 250);

    QFontDatabase::addApplicationFont(":/res/Roboto-Light.ttf");
    ui->baseDisplay->setFont(QFont("Roboto Light", 67, 50));
    ui->flagDisplay->setFont(QFont("Roboto Light", 50, 50));
    ui->label->setFont(QFont("Roboto Light", 15, 50));
    ui->tableWidget->setFont(QFont("Roboto Light", 17, 50));

    QScrollerProperties scrollProperties;
    scrollProperties.setScrollMetric(QScrollerProperties::DragStartDistance, 0);
    scrollProperties.setScrollMetric(QScrollerProperties::OvershootDragDistanceFactor, 0);
    scrollProperties.setScrollMetric(QScrollerProperties::OvershootScrollDistanceFactor, 0);
    QScroller::scroller(ui->tableWidget)->setScrollerProperties(scrollProperties);
    QScroller::grabGesture(ui->tableWidget, QScroller::TouchGesture);

    tmr = new QTimer(this);
    tmr->setInterval(10);
    connect(tmr, &QTimer::timeout, this, &Chronometer::runTime);

    menuWidget = new MenuWidget(this);
    menuWidget->move(0, 0);
    menuWidget->close();

    QFile file("/storage/emulated/0/Chronometer/settings.txt");
    if (file.exists() && file.open(QIODevice::ReadOnly))
    {
        QList<QByteArray> settings = file.readAll().split(' ');
        if (settings.at(0) == "Dark") ui->styleButton->setChecked(true);
        else setTheme(false);
        rate = settings.at(1).toFloat();
        file.close();
    }
    else
    {
        QDir dir("/storage/emulated/0/Chronometer");
        if (!dir.exists())
            dir.mkpath(".");
        setTheme(false);
    }
}

Chronometer::~Chronometer()
{
    delete ui;
}

void Chronometer::startPause(bool start)
{
    if (start && !ui->stopButton->isVisible())
    {
        tmr->start();
        startTime = QTime::currentTime().msecsSinceStartOfDay();
        ui->flagButton->show();
    }
    else if (start)
    {
        tmr->start();
        startTime = QTime::currentTime().msecsSinceStartOfDay() - mainTime;
        ui->stopButton->close();
        ui->flagButton->show();
    }
    else
    {
        tmr->stop();
        ui->flagButton->close();
        ui->stopButton->show();
    }
}

void Chronometer::runTime()
{
    mainTime = QTime::currentTime().msecsSinceStartOfDay() - startTime;
    QString data(QTime::fromMSecsSinceStartOfDay(rate * mainTime).toString("mm:ss.zzz"));
    data.chop(1);
    display->setText(data);
}

void Chronometer::setFlag()
{
    QString number;
    QString deltaTime;

    if (ui->displaylWidget->isVisible())
    {
        display = ui->flagDisplay;
        ui->displaylWidget->close();
        ui->flagWidget->show();
        ui->flagDisplay->setText(ui->baseDisplay->text());
        ui->tableWidget->setRowCount(0);
    }
    int row = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(row);
    ui->tableWidget->scrollToBottom();
    //item 1
    if (row < 9)
        number = " 0" + QString::number(row + 1);
    else
        number = " " + QString::number(row + 1);
    ui->tableWidget->setItem(row, 0, new QTableWidgetItem(QIcon(":/res/flag.png"), number));
    ui->tableWidget->item(row, 0)->setFlags(Qt::NoItemFlags);
    ui->tableWidget->item(row, 0)->setForeground(QColor(Qt::gray));
    //item 2
    if (row != 0)
    {
        int currentTime = QTime::fromString(display->text() + "0", "mm:ss.zzz").msecsSinceStartOfDay();
        int lastTime = QTime::fromString(ui->tableWidget->item(row - 1, 2)->text() + "0", "mm:ss.zzz").msecsSinceStartOfDay();
        deltaTime = QTime::fromMSecsSinceStartOfDay(currentTime - lastTime).toString("mm:ss.zzz");
        deltaTime.chop(1);
    }
    else
        deltaTime = display->text();
    ui->tableWidget->setItem(row, 1, new QTableWidgetItem("+ " + deltaTime));
    ui->tableWidget->item(row, 1)->setFlags(Qt::NoItemFlags);
    ui->tableWidget->item(row, 1)->setForeground(QColor(Qt::gray));
    //item 3
    ui->tableWidget->setItem(row, 2, new QTableWidgetItem(display->text()));
    ui->tableWidget->item(row, 2)->setFlags(Qt::NoItemFlags);
    ui->tableWidget->item(row, 2)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
}

void Chronometer::stop()
{
    if (ui->flagWidget->isVisible())
    {
        display = ui->baseDisplay;
        ui->flagWidget->close();
        ui->displaylWidget->show();
    }
    tmr->stop();
    display->setText("00:00.00");
    ui->stopButton->close();
}

void Chronometer::setTheme(bool dark)
{
    QString styleName;
    QPalette palette;
    if (dark)
    {
        palette.setColor(QPalette::Window, QColor(Qt::black));
        palette.setColor(QPalette::WindowText, QColor(Qt::white));
        ui->startPauseButton->setStyleSheet(darkButton);
        ui->flagButton->setStyleSheet(darkButton);
        ui->stopButton->setStyleSheet(darkButton);
        ui->tableWidget->setStyleSheet("background-color: black; color: white");
        menuWidget->setTheme("background-color: rgb(35, 35, 35); color: white");
        writeConfig("Dark");
    }
    else
    {
        palette.setColor(QPalette::Window, QColor(Qt::white));
        palette.setColor(QPalette::WindowText, QColor(Qt::black));
        ui->startPauseButton->setStyleSheet(lightButton);
        ui->flagButton->setStyleSheet(lightButton);
        ui->stopButton->setStyleSheet(lightButton);
        ui->tableWidget->setStyleSheet("background-color: white; color: black");
        menuWidget->setTheme("background-color: white; color: black");
        writeConfig("Light");
    }
    setPalette(palette);
}

void Chronometer::openMenu()
{
    menuWidget->show();
}

void Chronometer::writeConfig(const QString &theme)
{
    QFile file("/storage/emulated/0/Chronometer/settings.txt");
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        out << theme << " " << rate;
        file.close();
    }
}
