#include "mainwindow.h"

#include <QApplication>
#include <QLabel>
#include <QSurfaceFormat>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QSurfaceFormat format;
    format.setSamples(16);
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    MainWindow widget;
    widget.resize(1200, 800);
    widget.show();

    return app.exec();
}
