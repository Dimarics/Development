#ifndef GRID_H
#define GRID_H

#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <QColor>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class Grid
{
public:
    Grid();
    ~Grid();

    void init();
    void setColor(const QColor&);
    void draw(QOpenGLShaderProgram*, QOpenGLFunctions*);

private:
    int m_count;
    float m_step;
    float m_lineWidth;
    QColor m_color;
    QOpenGLBuffer m_arrayBuffer;
    QOpenGLBuffer m_indexBuffer;
    QMatrix4x4 m_modelMatrix;
};

#endif // GRID_H
