#include "grid.h"
#include <QFile>

Grid::Grid() : m_count(60), m_step(120), m_lineWidth(1), m_color(Qt::gray), m_indexBuffer(QOpenGLBuffer::IndexBuffer)
{
    init();
}

Grid::~Grid()
{
    m_arrayBuffer.destroy();
    m_indexBuffer.destroy();
    //delete m_texture;
}

void Grid::init()
{
    QVector<QVector2D> lines;
    QVector<GLuint> indexes;

    for (int i = 0; i < m_count; ++i)
    {
        static float length = m_step * (m_count - 1);
        static float pos = -length / 2;
        lines.append(QVector2D(pos, -length / 2));
        lines.append(QVector2D(pos, length / 2));
        lines.append(QVector2D(-length / 2, pos));
        lines.append(QVector2D(length / 2, pos));
        pos += m_step;
    }
    for (int i = 0; i < lines.size(); ++i) indexes.append(i);

    m_arrayBuffer.create();
    m_arrayBuffer.bind();
    m_arrayBuffer.allocate(lines.constData(), lines.size() * sizeof(QVector2D));
    m_arrayBuffer.release();

    m_indexBuffer.create();
    m_indexBuffer.bind();
    m_indexBuffer.allocate(indexes.constData(), indexes.size() * sizeof(GLuint));
    m_indexBuffer.release();
}

void Grid::setColor(const QColor &color)
{
    m_color = color;
}

void Grid::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *functions)
{
    m_arrayBuffer.bind();
    m_indexBuffer.bind();
    program->setUniformValue("u_color", m_color);
    program->setUniformValue("u_lighting", false);
    program->setUniformValue("u_modelMatrix", m_modelMatrix);
    program->setAttributeBuffer("a_position", GL_FLOAT, 0, 2, sizeof(QVector2D));

    functions->glLineWidth(m_lineWidth);
    functions->glDrawArrays(GL_LINES, 0, m_indexBuffer.size());

    program->setUniformValue("u_lighting", true);
    m_arrayBuffer.release();
    m_indexBuffer.release();
}
