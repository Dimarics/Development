#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->yPlusButton->setMask(QPixmap(":/images/up_Button.png").mask());
    ui->xMinusButton->setMask(QPixmap(":/images/right_Button.png").mask());
    ui->yMinusButton->setMask(QPixmap(":/images/down_Button.png").mask());
    ui->xPlusButton->setMask(QPixmap(":/images/left_Button.png").mask());

    ui->zPlusButton->setMask(QPixmap(":/images/up_Button.png").mask());
    ui->rMinusButton->setMask(QPixmap(":/images/right_Button.png").mask());
    ui->zMinusButton->setMask(QPixmap(":/images/down_Button.png").mask());
    ui->rPlusButton->setMask(QPixmap(":/images/left_Button.png").mask());
}

MainWindow::~MainWindow()
{
    delete ui;
}
