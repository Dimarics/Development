#ifndef CAMERA_H
#define CAMERA_H

#include <QMatrix4x4>>
#include <QOpenGLShaderProgram>
#include <QQuaternion>

class Camera
{
public:
    Camera();
    void view(QOpenGLShaderProgram*);
    void move(const float, const float, const float);
    void translate(const float, const float, const float);
    void relativeRotate(const QQuaternion&);
    void globalRotate(const QQuaternion&);
    void scale(const float);

private:
    float m_scale;
    QVector3D m_move;
    QQuaternion m_rotate;
    QMatrix4x4 m_translate;
};

#endif // CAMERA_H
