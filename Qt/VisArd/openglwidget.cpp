#include "openglwidget.h"
#include <QMouseEvent>

OpenGLWidget::OpenGLWidget(QWidget *parent) : QOpenGLWidget(parent) {}

OpenGLWidget::~OpenGLWidget()
{
    makeCurrent();
    delete grid;
    for (Object3D* object : objects) delete object;
    objects.clear();
    doneCurrent();
}

void OpenGLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    QColor color(33, 40, 48);
    glClearColor(color.redF(), color.greenF(), color.blueF(), color.alphaF());

    initShaders();
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    grid = new Grid;
    camera = new Camera;
    camera->move(0.0, 0.0, -150.0);
    camera->translate(0.0, 0.0, 200.0);

    objects.append(new Object3D("base.obj", Qt::red));
    //objects.append(new Object3D("rotor.obj", Qt::yellow));
    //objects.last()->move(-2800.0, -1100.0, -80);
    //objects.last()->rotate(QQuaternion::fromAxisAndAngle(0.0, 0.0, 1.0, 45));
    //objects.append(new Object3D("shoulder.obj", QColor(0, 127, 255), objects.last()));
    //objects.last()->move(0.0, 0.0, 18.5);
    //objects.last()->setRotationCenter(0.0, 0.0, 11.0);
    //objects.last()->rotate(QQuaternion::fromAxisAndAngle(1.0, 0.0, 0.0, -60));
    //objects.append(new Object3D("brachium.obj", QColor(0, 255, 127), objects.last()));
    //objects.last()->move(0.0, 0.0, 100.3);
    //objects.last()->setRotationCenter(0.0, 0.0, 11.0);
    //objects.last()->rotate(QQuaternion::fromAxisAndAngle(1.0, 0.0, 0.0, 45));
    //objects.append(new Object3D("elbow.obj", QColor(255, 0, 127)));
    //objects.append(new Object3D("wrist.obj", QColor(255, 127, 0)));
    //objects.append(new Object3D("left_finger.obj", Qt::green));
    //objects.append(new Object3D("right_finger.obj", Qt::green));
}

void OpenGLWidget::initShaders()
{
    m_program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vshader.glsl");
    m_program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fshader.glsl");
    m_program.link();
    m_program.bind();

    m_program.enableAttributeArray("a_position");
    m_program.enableAttributeArray("a_normal");
    m_program.setUniformValue("u_lighting", true);
    m_program.setUniformValue("u_lightPower", 1.0f);
}

void OpenGLWidget::resizeGL(int w, int h)
{
    qreal aspect = qreal(w) / qreal(h ? h : 1);
    m_projectionMatrix.setToIdentity();
    m_projectionMatrix.perspective(45.0, aspect, 0.1, 1000000.0);
}

void OpenGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    camera->view(&m_program);
    m_program.setUniformValue("u_projectionMatrix", m_projectionMatrix);
    for (Object3D* object : objects)
    {
        object->draw(&m_program, context()->functions());
    }
    grid->draw(&m_program, context()->functions());
}

void OpenGLWidget::mousePressEvent(QMouseEvent *event)
{
    m_mousePosition = event->button() == Qt::LeftButton ? QVector2D(event->position()) : QVector2D();
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mousePosition != QVector2D())
    {
        QVector2D diff = (QVector2D(event->position()) - m_mousePosition) / 3;
        m_mousePosition = QVector2D(event->position());
        camera->globalRotate(QQuaternion::fromAxisAndAngle(0.0, 0.0, 1.0, diff.x()));
        camera->relativeRotate(QQuaternion::fromAxisAndAngle(1.0, 0.0, 0.0, diff.y()));
        update();
    }
}

void OpenGLWidget::wheelEvent(QWheelEvent *event)
{
    camera->scale(event->angleDelta().y() > 0 ? 1.05 : 0.95);
    update();
}
