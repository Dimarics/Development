#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QtSerialPort/QSerialPort>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    bool eventFilter(QObject*, QEvent*) override;

private:
    Ui::MainWindow *ui;
    quint8 pos;
    quint64 start_session_time;
    QMediaPlayer *player;
    QSerialPort *serial_port;

private slots:
    void portChanged(quint8 = 0);
    void timeStamp(qint64);
    void selectPosition();
    void readPort();
    void hideButtonBox();
    void setTime();
};
#endif // MAINWINDOW_H
