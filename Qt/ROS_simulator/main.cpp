#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOverrideCursor(QCursor(Qt::BlankCursor));
    MainWindow window;
    window.showFullScreen();

    return app.exec();
}
