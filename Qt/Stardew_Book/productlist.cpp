#include "productlist.h"

ProductList::ProductList(QWidget *parent) : QFrame(parent)
{
    layout = new QGridLayout(this);
}

void ProductList::setItems(const QStringList &products, const quint16 price)
{
    for (QString product : products)
    {
        QString icon_path;
        int product_price = 0;
        int row = layout->rowCount();

        if (product == "Джем")
        {
            icon_path = ":/res/resourses/Jelly.png";
            product_price = price * 2 + 50;
        }
        else if (product == "Консервы")
        {
            icon_path = ":/res/resourses/Pickles.png";
            product_price = price * 2 + 50;
        }
        else if (product == "Кофе")
        {
            icon_path = ":/res/resourses/Coffee.png";
            product_price = 150;
        }
        else if (product == "Масло")
        {
            icon_path = ":/res/resourses/Oil.png";
            product_price = 100;
        }
        else if (product == "Мёд")
        {
            icon_path = ":/res/resourses/Honey.png";
            switch (price)
            {
            case 30:
                product_price = 160;
                break;
            case 50:
                product_price = 200;
                break;
            case 80:
                product_price = 260;
                break;
            case 90:
                product_price = 280;
                break;
            case 140:
                product_price = 380;
                break;
            case 290:
                product_price = 680;
                break;
            }
        }
        else if (product == "Мука")
        {
            icon_path = ":/res/resourses/Wheat_Flour.png";
            product_price = 50;
        }
        else if (product == "Рис")
        {
            icon_path = ":/res/resourses/Rice.png";
            product_price = 100;
        }
        else if (product == "Сок")
        {
            icon_path = ":/res/resourses/Juice.png";
            product_price = price * 2.25;
        }
        else if (product == "Осетровая икра")
        {
            icon_path = ":/res/resourses/Black_Roe.png";
            product_price = 130;
        }
        else if (product == "Чёрная икра")
        {
            icon_path = ":/res/resourses/Caviar.png";
            product_price = 500;
        }
        else if (product.contains("Икра"))
        {
            if (product == "Икра красная")
                icon_path = ":/res/resourses/Red_Roe.png";
            else if (product == "Икра оранжевая")
                icon_path = ":/res/resourses/Orange_Roe.png";
            else if (product == "Икра жёлтая")
                icon_path = ":/res/resourses/Yellow_Roe.png";
            else if (product == "Икра зелёная")
                icon_path = ":/res/resourses/Green_Roe.png";
            else if (product == "Икра синяя")
                icon_path = ":/res/resourses/Blue_Roe.png";
            else if (product == "Икра фиолетовая")
                icon_path = ":/res/resourses/Violet_Roe.png";
            else if (product == "Икра коричневая")
                icon_path = ":/res/resourses/Brown_Roe.png";
            else if (product == "Икра серая")
                icon_path = ":/res/resourses/Gray_Roe.png";
            else if (product == "Икра белая")
                icon_path = ":/res/resourses/White_Roe.png";
            else if (product == "Икра бежевая")
                icon_path = ":/res/resourses/Beige_Roe.png";

            product = "Икра";
            product_price = price / 2 + 30;
        }
        else if (product.contains("Вяленая икра"))
        {
            if (product == "Вяленая икра красная")
                icon_path = ":/res/resourses/Red_Aged_Roe.png";
            else if (product == "Вяленая икра оранжевая")
                icon_path = ":/res/resourses/Orange_Aged_Roe.png";
            else if (product == "Вяленая икра жёлтая")
                icon_path = ":/res/resourses/Yellow_Aged_Roe.png";
            else if (product == "Вяленая икра зелёная")
                icon_path = ":/res/resourses/Green_Aged_Roe.png";
            else if (product == "Вяленая икра синяя")
                icon_path = ":/res/resourses/Blue_Aged_Roe.png";
            else if (product == "Вяленая икра фиолетовая")
                icon_path = ":/res/resourses/Violet_Aged_Roe.png";
            else if (product == "Вяленая икра коричневая")
                icon_path = ":/res/resourses/Brown_Aged_Roe.png";
            else if (product == "Вяленая икра серая")
                icon_path = ":/res/resourses/Gray_Aged_Roe.png";
            else if (product == "Вяленая икра белая")
                icon_path = ":/res/resourses/White_Aged_Roe.png";
            else if (product == "Вяленая икра бежевая")
                icon_path = ":/res/resourses/Beige_Aged_Roe.png";

            product = "Вяленая икра";
            product_price = int(price / 2 + 30) * 2;
        }

        QLabel *icon = new QLabel;
        icon->setPixmap(QPixmap(icon_path).scaled(50 * y, 50 * y, Qt::KeepAspectRatio));
        layout->addWidget(icon, row, 0);
        QLabel *product_name = new QLabel(product);
        product_name->setAlignment(Qt::AlignCenter);
        layout->addWidget(product_name, row, 1);
        QLabel *money_label = new QLabel;
        money_label->setText(QString::number(product_price) + coin);
        layout->addWidget(money_label, row, 2);
    }
}

void ProductList::clear()
{
    for (uint8_t i = 0; i < layout->rowCount(); i++)
    {
        for (uint8_t j = 0; j < layout->columnCount(); j++)
        {
            QLayoutItem *item = layout->itemAtPosition(i, j);
            if (item) item->widget()->deleteLater();
        }
    }
}
