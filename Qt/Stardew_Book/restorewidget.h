#ifndef RESTOREWIDGET_H
#define RESTOREWIDGET_H

#include <QWidget>

namespace Ui {
class RestoreWidget;
}

class RestoreWidget : public QWidget
{
    Q_OBJECT

public:
    RestoreWidget(QWidget* = 0);
    ~RestoreWidget();
    void regeneration(const QVector<quint16>&, const QVector<quint16>&);

private:
    Ui::RestoreWidget *ui;
};

#endif // RESTOREWIDGET_H
