#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScreen>
#include <QLabel>
#include <QScrollArea>
#include "listbutton.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* = 0);
    ~MainWindow();

private:
    struct Recipe
    {
        QString icon;
        QString name;
        QString type;
    };

    const qreal y = qreal(screen()->geometry().height() / 1400.0);
    const QString coin = QString("<img src=\":/res/resourses/Money.png\" height=\"%1\""
                                 "style=\"vertical-align:middle;padding-left:10%px\">").arg(36 * y);

    bool memory = true;
    QVector<quint8> page_history{0};
    Ui::MainWindow *ui;

    QString image(const QString&, const quint16, const QString& = "height");
    QString iconLabel(const QString&, const QString&, const quint8 = 0);
    QString link(const QString&, const QString&, const QString&);
    QString tableItem(const QString&, const QString&, bool = false);
    QString emoji(QString, const QString&);
    QString queenOfSauce(const QString&, quint8);
    QString person(const QString&, const QString&, const quint8);

    void buttonList(const QString[][2], const quint16, void (MainWindow::*)());
    void setScroller(QScrollArea*);
    void plantInfoOpen(const QString&, const QString&);
    void fishInfoOpen(const QString&, const QString&);
    void cookingInfoOpen(const QString&, const QString&);

private slots:
    void returnMainMenu();
    void pageChanged(const quint8);
    void returnBack();
    void linkTransfer(const QString&);

    void setSeason(QLabel*, const QStringList&);
    void setWeather(QLabel*, const QStringList&);

    void plantsPageInit();
    void setPlantsPage();

    void fishPageInit();
    void setFishPage();

    void cookingPageInit();
    void setCookingPage();
};
#endif // MAINWINDOW_H
