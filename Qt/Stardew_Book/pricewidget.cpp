#include "pricewidget.h"
#include "ui_pricewidget.h"

PriceWidget::PriceWidget(QWidget *parent) : QWidget(parent), ui(new Ui::PriceWidget)
{
    ui->setupUi(this);
    ui->standard_quality->setMinimumHeight(70 * y);
    ui->standard_price->setMinimumHeight(70 * y);
    ui->silver_quality->setQuality("Silver_Star");
    ui->gold_quality->setQuality("Gold_Star");
    ui->iridium_quality->setQuality("Iridium_Star");
}

void PriceWidget::setItem(const QString &image_path, const quint16 price, const bool iridium)
{
    ui->standard_quality->renderItem(image_path);
    ui->standard_price->setText(QString::number(price) + coin);
    ui->silver_quality->renderItem(image_path);
    ui->silver_price->setText(QString::number(price * 1.25, 'f', 0) + coin);
    ui->gold_quality->renderItem(image_path);
    ui->gold_price->setText(QString::number(price * 1.5, 'f', 0) + coin);
    if (iridium)
    {
        ui->iridium_quality->show();
        ui->iridium_price->show();
        ui->iridium_quality->renderItem(image_path);
        ui->iridium_price->setText(QString::number(price * 2, 'f', 0) + coin);
    }
    else
    {
        ui->iridium_quality->close();
        ui->iridium_price->close();
    }
}

PriceWidget::~PriceWidget()
{
    delete ui;
}
