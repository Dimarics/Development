#ifndef PRICEWIDGET_H
#define PRICEWIDGET_H

#include <QLabel>
#include <QScreen>

namespace Ui {
class PriceWidget;
}

class PriceWidget : public QWidget
{
    Q_OBJECT

public:
    void setItem(const QString&, const quint16, const bool = true);
    PriceWidget(QWidget* = 0);
    ~PriceWidget();

private:
    const qreal y = qreal(screen()->geometry().height() / 1400.0);
    const QString coin = QString("<img src=\":/res/resourses/Money.png\" height=\"%1\" align=\"middle\">").arg(36 * y);
    Ui::PriceWidget *ui;
};

#endif // PRICEWIDGET_H
