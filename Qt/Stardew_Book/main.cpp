#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    qputenv("QT_ENABLE_HIGHDPI_SCALING", "0");
    QApplication app(argc, argv);
    //QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    MainWindow window;
    window.show();
    return app.exec();
}
