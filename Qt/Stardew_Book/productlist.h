#ifndef PRODUCTLIST_H
#define PRODUCTLIST_H

#include <QLabel>
#include <QScreen>
#include <QGridLayout>

class ProductList : public QFrame
{
    Q_OBJECT

public:
    void setItems(const QStringList&, const quint16);
    void clear();
    explicit ProductList(QWidget *parent = nullptr);

private:
    const qreal y = qreal(screen()->geometry().height() / 1400.0);
    const QString coin = QString("<img src=\":/res/resourses/Money.png\" height=\"%1\" align=\"middle\">").arg(36 * y);
    QGridLayout *layout;
};

#endif // PRODUCTLIST_H
