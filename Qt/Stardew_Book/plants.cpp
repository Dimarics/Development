#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include <QDebug>

void MainWindow::plantsPageInit()
{
    const QString plants[][2] = {
        {"Амарант", "Amaranth"},
        {"Артишок", "Artichoke"},
        {"Баклажан", "Eggplant"},
        {"Батат", "Yam"},
        {"Бок-чой", "Bok_Choy"},
        {"Виноград", "Grape"},
        {"Древний плод", "Ancient_Fruit"},
        {"Дыня", "Melon"},
        {"Жгучий перец", "Hot_Pepper"},
        {"Зелёная фасоль", "Green_Bean"},
        {"Карамбола", "Starfruit"},
        {"Картофель", "Potato"},
        {"Клубника", "Strawberry"},
        {"Клюква", "Cranberries"},
        {"Кофейное зерно", "Coffee_Bean"},
        {"Красная капуста", "Red_Cabbage"},
        {"Кудрявая капуста", "Kale"},
        {"Кукуруза", "Corn"},
        {"Летний блестник", "Summer_Spangle"},
        {"Мак", "Poppy"},
        {"Не измельчённый рис", "Unmilled_Rice"},
        {"Пастернак", "Parsnip"},
        {"Подсолнух", "Sunflower"},
        {"Помидор", "Tomato"},
        {"Пшеница", "Wheat"},
        {"Ревень", "Rhubarb"},
        {"Редис", "Radish"},
        {"Свёкла", "Beet"},
        {"Синий яркоцвет", "Blue_Jazz"},
        {"Сказочная роза", "Fairy_Rose"},
        //{"Сладкая ягодка", "Sweet_Gem_Berry"},
        {"Тыква", "Pumpkin"},
        {"Тюльпан", "Tulip"},
        {"Хмель", "Hops"},
        {"Цветная капуста", "Cauliflower"},
        {"Черника", "Blueberry"},
        {"Чеснок", "Garlic"}
    };
    buttonList(plants, sizeof(plants), &MainWindow::setPlantsPage);
    ui->stackedWidget->setCurrentWidget(ui->list_page);
}

void MainWindow::setPlantsPage()
{
    ListButton *button = qobject_cast<ListButton*>(sender());
    plantInfoOpen(button->text().remove(0, 3), button->image_path);
}

void MainWindow::plantInfoOpen(const QString &plant_name, const QString &image_path)
{
    const QString seeds_info = "<table cellpadding=\"3\"><tr>"
                               "<td style=\"vertical-align:middle; horizontal-align:right\">"
                               "%1</td>"
                               "<td style=\"horizontal-align:left\">"
                               "%2</td></tr></table>";
    bool iridium = true;
    QString description;
    QString seeds_icon;
    QString seeds;
    QString grow_time;
    QStringList season;
    QString frequency;
    QString exp;
    QVector<quint16> energy;
    QVector<quint16> health;
    quint16 price = 0;
    QStringList products;
    QString tipple;
    QString tipple_icon;
    quint16 tipple_price = 0;
    QList<Recipe> recipes;

    if (plant_name == "Амарант")
    {
        iridium = false;
        description = "Одна из цивилизаций древности\nкогда-то выращивала это\nфиолетовое растение на крупу.";
        seeds_icon = "Amaranth_Seeds";
        seeds = "Семена<br>амаранта";
        grow_time = "7 дней";
        season << "Осень";
        frequency = "Нет";
        exp = "21";
        energy << 50 << 70 << 90;
        health << 22 << 31 << 40;
        price = 150;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Salmon_Dinner", "Ужин из лосося", "Еда"};
    }
    else if (plant_name == "Артишок")
    {
        iridium = false;
        description = "Нераскрывшаяся корзинка цветка\nартишока. Внутри, за колючими\nлистьям, он сытный и мясистый.";
        seeds_icon = "Artichoke_Seeds";
        seeds = "Семена<br>артишока";
        grow_time = "8 дней";
        season << "Осень";
        frequency = "Нет";
        exp = "22";
        energy << 30 << 42 << 54;
        health << 13 << 18 << 24;
        price = 160;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Artichoke_Dip", "Артишоковый дип", "Еда"}
                << Recipe {"Super_Meal", "Чудо-блюдо", "Еда"};
    }
    else if (plant_name == "Баклажан")
    {
        iridium = false;
        description = "Богатый, насыщенный родственник\nпомидора. Очень вкусен в тушеном и\nжареном видах.";
        seeds_icon = "Eggplant_Seeds";
        seeds = "Семена<br>баклажана";
        grow_time = "5 дней";
        season << "Осень";
        frequency = "Каждые 5 дней";
        exp = "12";
        energy << 20 << 28 << 36;
        health << 9 << 12 << 16;
        price = 60;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Eggplant_Parmesan", "Баклажаны с пармезаном", "Еда"}
                << Recipe {"Survival_Burger", "«Бургер для выживания»", "Еда"};
    }
    else if (plant_name == "Батат")
    {
        iridium = false;
        description = "Крахмалистый клубень со множеством\nкулинарных применений.";
        seeds_icon = "Yam_Seeds";
        seeds = "Семена<br>батата";
        grow_time = "10 дней";
        season << "Осень";
        frequency = "Нет";
        exp = "22";
        energy << 45 << 63 << 81;
        health << 20 << 28 << 36;
        price = 160;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Autumn_Bounty", "«Дары осени»", "Еда"}
                << Recipe {"Glazed_Yams", "Глазированный батат", "Еда"};
    }
    else if (plant_name == "Бок-чой")
    {
        iridium = false;
        description = "Волокнистые стебли и зеленые листья\nвкусны и полезны.";
        seeds_icon = "Bok_Choy_Seeds";
        seeds = "Семена<br>бок-чой";
        grow_time = "4 дня";
        season << "Осень";
        frequency = "Нет";
        exp = "14";
        energy << 25 << 35 << 45;
        health << 11 << 15 << 20;
        price = 80;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Super_Meal", "Чудо-блюдо", "Еда"};
    }
    else if (plant_name == "Виноград")
    {
        description = "Кисть сладких ягод.";
        seeds_icon = "Grape_Starter";
        seeds = "Виноградные<br>черенки";
        grow_time = "10 дней";
        season << "Осень";
        frequency = "Каждые 3 дня";
        exp = "14";
        energy << 38 << 53 << 68 << 98;
        health << 17 << 23 << 30 << 44;
        price = 80;
        products << "Джем";
        tipple = "Вино";
        recipes << Recipe {"Summer_Seeds", "Летние семена", "Семена"};
    }
    else if (plant_name == "Древний плод")
    {
        description = "Он спал целые эры.";
        seeds_icon = "Ancient_Seeds";
        seeds = "Древние<br>семена";
        grow_time = "28 дней";
        season << "Весна" << "Лето" << "Осень";
        frequency = "Каждые 7 дней";
        exp = "38";
        price = 550;
        products << "Джем";
        tipple = "Вино";
    }
    else if (plant_name == "Дыня")
    {
        description = "Холодное, сладкое летнее угощение.";
        seeds_icon = "Melon_Seeds";
        seeds = "Семена<br>тыквы";
        grow_time = "12 дней";
        season << "Лето";
        frequency = "Нет";
        exp = "27";
        energy << 113 << 158 << 203 << 293;
        health << 50 << 71 << 91 << 131;
        price = 250;
        products << "Джем";
        tipple = "Вино";
        recipes << Recipe {"Fruit_Salad", "Фруктовый салат", "Еда"}
                << Recipe {"Pink_Cake", "Розовый торт", "Еда"};
    }
    else if (plant_name == "Жгучий перец")
    {
        description = "Слегка сладковатый и\nпламенно жгучий.";
        seeds_icon = "Pepper_Seeds";
        seeds = "Семена<br>перца";
        grow_time = "5 дней";
        season << "Лето";
        frequency = "Каждые 3 дня";
        exp = "9";
        energy << 13 << 18 << 23 << 33;
        health << 5 << 8 << 10 << 14;
        price = 40;
        products << "Джем";
        tipple = "Вино";
        recipes << Recipe {"Pepper_Poppers", "Перечные бомбочки", "Еда"}
                << Recipe {"Spicy_Eel", "Острый угорь", "Еда"}
                << Recipe {"Tropical_Curry", "Тропическое карри", "Еда"};
    }
    else if (plant_name == "Зелёная фасоль")
    {
        description = "Сочный, хрустящий боб.";
        seeds_icon = "Bean_Starter";
        seeds = "Рассада<br>зелёной<br>фасоли";
        grow_time = "10 дней";
        season << "Весна";
        frequency = "Каждые 3 дня";
        exp = "9";
        energy << 25 << 35 << 45 << 65;
        health << 11 << 15 << 20 << 29;
        price = 40;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Bean_Hotpot", "Жаркое из фасоли", "Еда"};
    }
    else if (plant_name == "Карамбола")
    {
        description = "Чрезвычайно сочный плод, растущий в<br>жарком и влажном климате. Немного<br>сладкий с кислыми нотками.";
        seeds_icon = "Starfruit_Seeds";
        seeds = "Семена<br>карамболы";
        grow_time = "13 дней";
        season << "Лето";
        frequency = "Нет";
        exp = "44";
        energy << 125 << 175 << 225 << 325;
        health << 56 << 78 << 101 << 146;
        price = 750;
        products << "Джем";
        tipple = "Вино";
    }
    else if (plant_name == "Картофель")
    {
        description = "Широко распространённый клубень.";
        seeds_icon = "Potato_Seeds";
        seeds = "Семена<br>картофеля";
        grow_time = "6 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "14";
        energy << 25 << 35 << 45 << 65;
        health << 11 << 15 << 20 << 29;
        price = 80;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Hashbrowns", "Хашбраун", "Еда"};
    }
    else if (plant_name == "Клубника")
    {
        description = "Сладкая и сочная ягода\nярко-красного цвета.";
        seeds_icon = "Strawberry_Seeds";
        seeds = "Семена<br>клубники";
        grow_time = "8 дней";
        season << "Весна";
        frequency = "Каждые 4 дня";
        exp = "18";
        energy << 50 << 70 << 90 << 130;
        health << 22 << 31 << 40 << 58;
        price = 120;
        products << "Джем";
        tipple = "Вино";
    }
    else if (plant_name == "Клюква")
    {
        iridium = false;
        description = "Эти красные кислые ягодки\nобычно едят зимой.";
        seeds_icon = "Cranberry_Seeds";
        seeds = "Семена<br>клюквы";
        grow_time = "7 дней";
        season << "Осень";
        frequency = "Каждые 5 дней";
        exp = "14";
        energy << 38 << 53 << 68;
        health << 17 << 23 << 30;
        price = 75;
        products << "Джем";
        tipple = "Вино";
        recipes << Recipe {"Cranberry_Candy", "Клюквенная сласть", "Еда"}
                << Recipe {"Cranberry_Sauce", "Клюквенный соус", "Еда"}
                << Recipe {"Stuffing", "«Объеденье»", "Еда"}
                << Recipe {"Super_Meal", "Чудо-блюдо", "Еда"};
    }
    else if (plant_name == "Кофейное зерно")
    {
        description = "Сажайте летом, чтобы вырастить\n"
                      "кофейное дерево. Положите\n"
                      "5 зерен в бочонок, чтобы\n"
                      "приготовить кофе.";
        seeds_icon = "Coffee_Bean";
        seeds = "Кофейное<br>зерно";
        grow_time = "10 дней";
        season << "Весна";
        frequency = "Каждые 2 дня";
        exp = "4";
        price = 15;
        products << "Кофе";
    }
    else if (plant_name == "Красная капуста")
    {
        description = "Её часто используют в салатах.\n"
                      "Её цвет варьируется от фиолетового\n"
                      "до синего и желто-зеленого,\n"
                      "в зависимости от условий.";
        seeds_icon = "Red_Cabbage_Seeds";
        seeds = "Семена<br>красной<br>капусты";
        grow_time = "9 дней";
        season << "Лето";
        frequency = "Нет";
        exp = "28";
        energy << 75 << 105 << 135 << 195;
        health << 33 << 47 << 60 << 87;
        price = 260;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Coleslaw", "Капустный салат", "Еда"}
                << Recipe {"Fish_Taco", "Рыбные тако", "Еда"}
                << Recipe {"Red_Plate", "Красное блюдо", "Еда"};
    }
    else if (plant_name == "Кудрявая капуста")
    {
        description = "Ее вощеные листья хороши в\nсупах и стир-фраях.";
        seeds_icon = "Kale_Seeds";
        seeds = "Семена<br>кудрявой<br>капусты";
        grow_time = "6 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "17";
        energy << 50 << 70 << 90 << 130;
        health << 22 << 31 << 40 << 58;
        price = 110;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Salmon_Dinner", "Ужин из лосося", "Еда"}
                << Recipe {"Stir_Fry", "Стир-фрай", "Еда"};
    }
    else if (plant_name == "Кукуруза")
    {
        description = "Один из самых популярных злаков.\nЛетом всегда приятно отведать\nсвежих, сладких початков.";
        seeds_icon = "Corn_Seeds";
        seeds = "Семена<br>кукурузы";
        grow_time = "14 дней";
        season << "Лето" << "Осень";
        frequency = "Каждые 4 дня";
        exp = "10";
        energy << 25 << 35 << 45 << 65;
        health << 11 << 15 << 20 << 29;
        price = 50;
        products << "Сок" << "Консервы" << "Масло";
        recipes << Recipe {"Tortilla", "Тортилья", "Еда"};
    }
    else if (plant_name == "Летний блестник")
    {
        description = "Тропический цветок, который любит\nвлажный летний воздух. У него\nсладкий и терпкий запах.";
        seeds_icon = "Spangle_Seeds";
        seeds = "Семена<br>блестника";
        grow_time = "8 дней";
        season << "Лето";
        frequency = "Нет";
        exp = "10";
        energy << 45 << 63 << 81 << 117;
        health << 20 << 28 << 36 << 52;
        price = 90;
        products << "Мёд";
    }
    else if (plant_name == "Мак")
    {
        iridium = false;
        description = "Он не только красиво цветeт, но и\nиспользуется в медицине и кулинарии.";
        seeds_icon = "Poppy_Seeds";
        seeds = "Семена<br>мака";
        grow_time = "7 дней";
        season << "Лето";
        frequency = "Нет";
        exp = "20";
        energy << 45 << 63 << 81;
        health << 20 << 28 << 36;
        price = 140;
        products << "Мёд";
        recipes << Recipe {"Poppyseed_Muffin", "Маффин с маком", "Еда"};
    }
    else if (plant_name == "Не измельчённый рис")
    {
        description = "Чистый рис. Запустите его через\nмельницу, чтобы увеличить значение.";
        seeds_icon = "Rice_Shoot";
        seeds = "Росток риса";
        grow_time = "6-8 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "7";
        energy << 3 << 4 << 5 << 7;
        health << 1 << 1 << 2 << 3;
        price = 30;
        products << "Сок" << "Консервы" << "Рис";
    }
    else if (plant_name == "Пастернак")
    {
        description = "Весеннее растение, близкий родственник\nморковки. Очень питательный клубень с\nнасыщенным вкусом.";
        seeds_icon = "Parsnip_Seeds";
        seeds = "Семена<br>пастернака";
        grow_time = "4 дня";
        season << "Весна";
        frequency = "Нет";
        exp = "8";
        energy << 25 << 35 << 45 << 65;
        health << 11 << 15 << 20 << 29;
        price = 35;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Farmer_Lunch", "«Обед фермера»", "Еда"}
                << Recipe {"Parsnip_Soup", "Суп из пастернака", "Еда"};
    }
    else if (plant_name == "Подсолнух")
    {
        iridium = false;
        description = "По распространённому заблуждению,\nэтот цветок всегда поворачивается к\nсолнцу.";
        seeds_icon = "Sunflower_Seeds";
        seeds = "Семена<br>подсолнуха";
        grow_time = "8 дней";
        season << "Лето" << "Осень";
        frequency = "Нет";
        exp = "14";
        energy << 45 << 63 << 81;
        health << 20 << 28 << 36;
        price = 80;
        products << "Мёд" << "Масло";
    }
    else if (plant_name == "Помидор")
    {
        iridium = false;
        description = "Насыщенный с кислинкой помидор\nшироко используется в кулинарии.";
        seeds_icon = "Tomato_Seeds";
        seeds = "Семена<br>помидора";
        grow_time = "11 дней";
        season << "Лето";
        frequency = "Каждые 4 дня";
        exp = "12";
        energy << 20 << 28 << 36;
        health << 9 << 12 << 16;
        price = 60;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Bruschetta", "Брускетта", "Еда"}
                << Recipe {"Eggplant_Parmesan", "Баклажаны с пармезаном", "Еда"}
                << Recipe {"Shrimp_Cocktail", "Креветочный коктейль", "Еда"}
                << Recipe {"Vegetable_Medley", "Овощное ассорти", "Еда"}
                << Recipe {"Pizza", "Пицца", "Еда"}
                << Recipe {"Spaghetti", "Спагетти", "Еда"}
                << Recipe {"Fish_Stew", "Уха", "Еда"};
    }
    else if (plant_name == "Пшеница")
    {
        iridium = false;
        description = "Один из самых популярных злаков.\nПшеничная мука отлично\nподходит для выпечки.";
        seeds_icon = "Wheat_Seeds";
        seeds = "Семена<br>пшеницы";
        grow_time = "4 дня";
        season << "Лето" << "Осень";
        frequency = "Нет";
        exp = "6";
        price = 25;
        products << "Консервы" << "Мука";
        tipple = "Пиво";
        tipple_icon = "Beer";
        tipple_price = 200;
    }
    else if (plant_name == "Ревень")
    {
        description = "Стебли очень кислые, но очень хороши в\nдесертах, если их подсластить.";
        seeds_icon = "Rhubarb_Seeds";
        seeds = "Семена<br>ревеня";
        grow_time = "13 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "26";
        price = 220;
        products << "Джем";
        tipple = "Вино";
        recipes << Recipe {"Rhubarb_Pie", "Пирог с ревенем", "Еда"};
    }
    else if (plant_name == "Редис")
    {
        iridium = false;
        description = "Хрустящий и сочный корнеплод,\nв сыром виде слегка острый.";
        seeds_icon = "Radish_Seeds";
        seeds = "Семена<br>редиса";
        grow_time = "6 дней";
        season << "Лето";
        frequency = "Нет";
        exp = "15";
        energy << 45 << 63 << 81;
        health << 20 << 28 << 36;
        price = 90;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Radish_Salad", "Салат из редиса", "Еда"}
                << Recipe {"Red_Plate", "Красное блюдо", "Еда"};
    }
    else if (plant_name == "Свёкла")
    {
        iridium = false;
        description = "Сладкий корнеплод с насыщенным вкусом.\nА еще из её ботвы получается\nотличный салат.";
        seeds_icon = "Beet_Seeds";
        seeds = "Семена<br>свеклы";
        grow_time = "6 дней";
        season << "Осень";
        frequency = "Нет";
        exp = "16";
        energy << 30 << 42 << 54;
        health << 13 << 18 << 24;
        price = 100;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Vegetable_Medley", "Овощное ассорти", "Еда"};
    }
    else if (plant_name == "Синий яркоцвет")
    {
        description = "Этот цветок растeт в виде шарика,\nчтобы привлечь как можно больше\nбабочек.";
        seeds_icon = "Jazz_Seeds";
        seeds = "Семена<br>яркоцвета";
        grow_time = "7 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "10";
        energy << 45 << 63 << 81 << 117;
        health << 20 << 28 << 36 << 52;
        price = 50;
        products << "Мёд";
        recipes << Recipe {"Lucky_Lunch", "Обед на удачу", "Еда"};
    }
    else if (plant_name == "Сказочная роза")
    {
        iridium = false;
        description = "По старому народному сказанию, сладкий\nзапах этого цветка приманивает фей.";
        seeds_icon = "Fairy_Seeds";
        seeds = "Сказочные<br>семена";
        grow_time = "12 дней";
        season << "Осень";
        frequency = "Нет";
        exp = "29";
        energy << 45 << 63 << 81;
        health << 20 << 28 << 36;
        price = 290;
        products << "Мёд";
    }
    else if (plant_name == "Тыква")
    {
        iridium = false;
        description = "Выращивается осенью ради её\n"
                      "хрустящих семян и деликатной мякоти.\n"
                      "А ещё её кожуру можно превратить в\n"
                      "праздничное украшение.";
        seeds_icon = "Pumpkin_Seeds";
        seeds = "Семена<br>тыквы";
        grow_time = "13 дней";
        season << "Осень";
        frequency = "Нет";
        exp = "31";
        price = 320;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Autumn_Bounty", "«Дары осени»", "Еда"}
                << Recipe {"Pumpkin_Pie", "Тыквенный пирог", "Еда"}
                << Recipe {"Pumpkin_Soup", "Тыквенный суп", "Еда"};
    }
    else if (plant_name == "Тюльпан")
    {
        description = "Самый распространeнный весенний\nцветок. У него очень слабый\nсладкий аромат.";
        seeds_icon = "Tulip_Bulb";
        seeds = "Луковица<br>тюльпана";
        grow_time = "6 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "7";
        energy << 45 << 63 << 81 << 117;
        health << 20 << 28 << 36 << 52;
        price = 30;
        products << "Мёд";
    }
    else if (plant_name == "Хмель")
    {
        iridium = false;
        description = "Горький, кисловатый цветок,\nиспользуемый в пивоварении.";
        seeds_icon = "Hops_Starter";
        seeds = "Рассада<br>хмеля";
        grow_time = "11 дней";
        season << "Лето";
        frequency = "Каждый день";
        exp = "6";
        energy << 45 << 63 << 81;
        health << 20 << 28 << 36;
        price = 25;
        products << "Консервы";
        tipple = "Светлый эль";
        tipple_icon = "Pale_Ale";
        tipple_price = 300;
    }
    else if (plant_name == "Цветная капуста")
    {
        description = "Ценная, но растет медленно. Несмотря на\nбледный цвет, ее соцветия полны\nпитательными веществами.";
        seeds_icon = "Cauliflower_Seeds";
        seeds = "Семена<br>цветной<br>капусты";
        grow_time = "12 дней";
        season << "Весна";
        frequency = "Нет";
        exp = "23";
        energy << 75 << 105 << 135 << 195;
        health << 33 << 47 << 60 << 87;
        price = 175;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Цветная капуста с сыром", "Cheese_Cauliflower", "Еда"};
    }
    else if (plant_name == "Черника")
    {
        iridium = false;
        description = "Любимая многими ягода, о пользе\n"
                      "которой написано немало статей.\n"
                      "В ее кожуре сосредоточены\n"
                      "питательные вещества.";
        seeds_icon = "Blueberry_Seeds";
        seeds = "Семена<br>черники";
        grow_time = "13 дней";
        season << "Лето";
        frequency = "Каждые 4 дня";
        exp = "10";
        energy << 25 << 35 << 45;
        health << 11 << 15 << 20;
        price = 50;
        products << "Джем";
        tipple = "Вино";
        recipes << Recipe {"Blueberry_Tart", "Черничный пирог", "Еда"}
                << Recipe {"Fruit_Salad", "Фруктовый салат", "Еда"};
    }
    else if (plant_name == "Чеснок")
    {
        description = "Придает блюдам остроту.\nКачественный чеснок может быть\nочень острым.";
        seeds_icon = "Garlic_Seeds";
        seeds = "Семена<br>чеснока";
        grow_time = "4 дня";
        season << "Весна";
        frequency = "Нет";
        exp = "12";
        energy << 20 << 28 << 36 << 52;
        health << 9 << 12 << 16 << 23;
        price = 60;
        products << "Сок" << "Консервы";
        recipes << Recipe {"Escargot", "Эскарго", "Еда"}
                << Recipe {"Fiddlehead_Risotto", "Ризотто с папоротником", "Еда"};
    }

    ui->plant_name->setText(plant_name);
    ui->plant_picture->setPixmap(QPixmap(":/res/resourses/" + image_path + ".png").
                                 scaled(100 * y, 100 * y, Qt::KeepAspectRatio));
    ui->plant_description->setText(description);
    ui->seeds_info->setText(seeds_info.arg(image(seeds_icon, 42)).arg(seeds));
    ui->growthTime_info->setText(grow_time);
    setSeason(ui->plant_seasons, season);
    ui->frequency_info->setText(frequency);
    ui->experience->setText(exp + " фермерство");

    if (energy.isEmpty())
        ui->plant_restore->close();
    else
    {
        ui->plant_restore->show();
        ui->plants_restoreWidget->regeneration(energy, health);
    }

    ui->plants_priceWidget->setItem(image_path, price, iridium);
    ui->plants_productList->clear();
    ui->plants_productList->setItems(products, price);
    if (!tipple.isEmpty())
    {
        ui->tipple_widget->show();
        ui->tipple_name->setText(tipple);
        if (tipple == "Вино")
            ui->tipple_priceWidget->setItem("Wine", price * 3);
        else
            ui->tipple_priceWidget->setItem(tipple_icon, tipple_price);
    }
    else ui->tipple_widget->close();

    if (recipes.isEmpty())
        ui->plant_recipesWidget->close();
    else
    {
        ui->plant_recipesWidget->show();
        QString recipes_label = "<table>";
        for (Recipe &recipe : recipes)
            recipes_label += tableItem(image(recipe.icon, 50 * y), "☐" + link(recipe.icon, recipe.name, recipe.type));
        ui->plant_recipes->setText(recipes_label + "</table>");
    }

    ui->stackedWidget->setCurrentWidget(ui->plant_info);
}