#include "restorewidget.h"
#include "ui_restorewidget.h"

RestoreWidget::RestoreWidget(QWidget *parent) : QWidget(parent), ui(new Ui::RestoreWidget)
{
    ui->setupUi(this);
    ui->standard_energy_icon->renderItem("Energy");
    ui->silver_energy_icon->setQuality("Silver_Star");
    ui->silver_energy_icon->renderItem("Energy");
    ui->gold_energy_icon->setQuality("Gold_Star");
    ui->gold_energy_icon->renderItem("Energy");
    ui->iridium_energy_icon->setQuality("Iridium_Star");
    ui->iridium_energy_icon->renderItem("Energy");

    ui->standard_health_icon->renderItem("Health");
    ui->silver_health_icon->setQuality("Silver_Star");
    ui->silver_health_icon->renderItem("Health");
    ui->gold_health_icon->setQuality("Gold_Star");
    ui->gold_health_icon->renderItem("Health");
    ui->iridium_health_icon->setQuality("Iridium_Star");
    ui->iridium_health_icon->renderItem("Health");
}

RestoreWidget::~RestoreWidget()
{
    delete ui;
}

void RestoreWidget::regeneration(const QVector<quint16> &energy, const QVector<quint16> &health)
{
    ui->standard_energy->setText(QString::number(energy.at(0)));
    ui->standard_health->setText(QString::number(health.at(0)));
    ui->silver_energy->setText(QString::number(energy.at(1)));
    ui->silver_health->setText(QString::number(health.at(1)));
    ui->gold_energy->setText(QString::number(energy.at(2)));
    ui->gold_health->setText(QString::number(health.at(2)));
    if (energy.size() < 4)
    {
        ui->iridium_energy_icon->close();
        ui->iridium_energy->close();
        ui->label_18->close();
        ui->iridium_health_icon->close();
        ui->iridium_health->close();
        ui->label_16->close();
    }
    else
    {
        ui->iridium_energy_icon->show();
        ui->iridium_energy->show();
        ui->label_18->show();
        ui->iridium_health_icon->show();
        ui->iridium_health->show();
        ui->label_16->show();
        ui->iridium_energy->setText(QString::number(energy.at(3)));
        ui->iridium_health->setText(QString::number(health.at(3)));
    }
}