#include "qualityimage.h"

QualityImage::QualityImage(QWidget *parent) : QLabel(parent)
{
    setAlignment(Qt::AlignCenter);
    //setMinimumSize(70 * y, 70 * y);
    star = new QLabel(this);
    star->setAlignment(Qt::AlignCenter);
    star->setStyleSheet("border: none");
}

void QualityImage::resizeEvent(QResizeEvent*)
{
    star->setGeometry(QRect(QPoint(-2, 2), size()));
}

void QualityImage::setQuality(const QString &quality)
{
    star->setPixmap(QPixmap(":/res/resourses/" + quality + ".png").scaled(48 * y, 48 * y, Qt::KeepAspectRatio));
}


void QualityImage::renderItem(const QString &item)
{
    setPixmap(QPixmap(":/res/resourses/" + item + ".png").scaled(48 * y, 48 * y, Qt::KeepAspectRatio));
}