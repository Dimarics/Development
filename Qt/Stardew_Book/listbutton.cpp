#include "listbutton.h"

ListButton::ListButton(QWidget *parent) : QPushButton(parent){}

void ListButton::setImage(const quint16 size, const QString &icon_path)
{
    image_path = icon_path;
    setIconSize(QSize(size, size));
    setIcon(QPixmap(":/res/resourses/" + icon_path + ".png").scaled(size, size, Qt::KeepAspectRatio));
}

void ListButton::mousePressEvent(QMouseEvent*)
{
    press = true;
}

void ListButton::mouseMoveEvent(QMouseEvent*)
{
    press = false;
}

void ListButton::mouseReleaseEvent(QMouseEvent*)
{
    if (press) emit clicked();
}
