#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::cookingPageInit()
{
    const QString food[][2] = {
        {"Артишоковый дип", "Artichoke_Dip"},
        {"Баклажаны с\n   пармезаном", "Eggplant_Parmesan"},
        {"Банановый пудинг", "Banana_Pudding"},
        {"Биск из омара", "Lobster_Bisque"},
        {"Блины", "Pancakes"},
        {"Брускетта", "Bruschetta"},
        {"«Бургер для выживания»", "Survival_Burger"},
        {"Глазированный батат", "Glazed_Yams"},
        {"«Дары осени»", "Autumn_Bounty"},
        {"Жареные грибы", "Fried_Mushroom"},
        {"Жареный кальмар", "Fried_Calamari"},
        {"Жареный угорь", "Fried_Eel"},
        {"Жареный фундук", "Roasted_Hazelnuts"},
        {"Жаркое из фасоли", "Bean_Hotpot"},
        {"Запечённая рыба", "Baked_Fish"},
        {"Имбирный эль", "Ginger_Ale"},
        {"Капустный салат", "Coleslaw"},
        {"Карповый сюрприз", "Carp_Surprise"},
        {"Кленовый пончик", "Maple_Bar"},
        {"Клюквенная сласть", "Cranberry_Candy"},
        {"Клюквенный соус", "Cranberry_Sauce"},
        {"Коренья на блюде", "Roots_Platter"},
        {"Крабовые котлетки", "Crab_Cakes"},
        {"Красное блюдо", "Red_Plate"},
        {"Креветочный коктейль", "Shrimp_Cocktail"},
        {"Манговый клейкий рис", "Mango_Sticky_Rice"},
        {"Маффин с маком", "Poppyseed_Muffin"},
        {"Мороженое", "Ice_Cream"},
        {"«Морское блюдо»", "Sea_Dish"},
        {"Морской пенный пудинг", "Seafoam_Pudding"},
        {"Обед на удачу", "Lucky_Lunch"},
        {"«Обед фермера»", "Farmer_Lunch"},
        {"«Объеденье»", "Stuffing"},
        {"Овощное ассорти", "Vegetable_Medley"},
        {"Окунь с хрустящей\n   корочкой", "Crispy_Bass"},
        {"Омлет", "Omelet"},
        {"Острый угорь", "Spicy_Eel"},
        {"Перечные бомбочки", "Pepper_Poppers"},
        {"Печенье", "Cookie"},
        {"Пирог с ревенем", "Rhubarb_Pie"},
        {"Пицца", "Pizza"},
        {"Пои", "Poi"},
        {"Полный завтрак", "Complete_Breakfast"},
        {"Ризотто с папоротником", "Fiddlehead_Risotto"},
        {"Рисовый пудинг", "Rice_Pudding"},
        {"Розовый торт", "Pink_Cake"},
        {"Рыбная похлёбка", "Chowder"},
        {"Рыбные тако", "Fish_Taco"},
        {"Салат", "Salad"},
        {"Салат из редиса", "Radish_Salad"},
        {"Сашими", "Sashimi"},
        {"Светлый бульон", "Pale_Broth"},
        {"Сливовый пудинг", "Plum_Pudding"},
        {"Спагетти", "Spaghetti"},
        {"Стир-фрай", "Stir_Fry"},
        {"Странная булочка", "Strange_Bun"},
        {"Суп из водрослей", "Algae_Soup"},
        {"Суп из пастернака", "Parsnip_Soup"},
        {"Суп том кха", "Tom_Kha_Soup"},
        {"Суши-роллы", "Maki_Roll"},
        {"Тортилья", "Tortilla"},
        {"Тройной эспрессо", "Triple_Shot_Espresso"},
        {"Тропическое карри", "Tropical_Curry"},
        {"Тыквенный пирог", "Pumpkin_Pie"},
        {"Тыквенный суп", "Pumpkin_Soup"},
        {"Ужин из лосося", "Salmon_Dinner"},
        {"Уха", "Fish_Stew"},
        {"Уха из форели", "Trout_Soup"},
        {"Фруктовый салат", "Fruit_Salad"},
        {"Хашбраун", "Hashbrowns"},
        {"Хлеб", "Bread"},
        {"Цветная капуста\n   с сыром", "Cheese_Cauliflower"},
        {"Черничный коблер", "Blackberry_Cobbler"},
        {"Черничный пирог", "Blueberry_Tart"},
        {"Чудо-блюдо", "Super_Meal"},
        {"«Шахтерский леденец»", "Miner_Treat"},
        {"Шоколадный торт", "Chocolate_Cake"},
        {"Эскарго", "Escargot"},
        {"Яичница", "Fried_Egg"},
    };
    buttonList(food, sizeof(food), &MainWindow::setCookingPage);
    ui->stackedWidget->setCurrentWidget(ui->list_page);
}

void MainWindow::setCookingPage()
{
    ListButton *button = qobject_cast<ListButton*>(sender());
    cookingInfoOpen(button->text().remove(0, 3), button->image_path);
}

void MainWindow::cookingInfoOpen(const QString &name, const QString &image_path)
{
    struct Ingredient
    {
        QString icon;
        QString name;
        QString type;
        QString count;
    };
    struct Effect
    {
        QString icon;
        QString impact;
    };

    QString meal_name = name == "Баклажаны с\n   пармезаном" ? "Баклажаны с пармезаном" :
                        name == "Окунь с хрустящей\n   корочкой" ? "Окунь с хрустящей корочкой" :
                        name == "Цветная капуста\n   с сыром" ? "Цветная капуста с сыром" : name;
    QString description;
    quint16 price = 0;
    QList<Ingredient> ingredients;
    QString energy;
    QString health;
    QList<Effect> effects;
    quint8 time_impact[2] {0, 0};
    QList<Recipe> recipes;
    QString stores;
    QString sources;

    if (meal_name == "Артишоковый дип")
    {
        description = "Прохладный и освежающий.";
        price = 210;
        ingredients << Ingredient {"Artichoke", "Артишок", "Растение", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"};
        energy = "100";
        health = "45";
        stores = "• Готовка";
        sources = queenOfSauce("осень 28", 1);
    }
    else if (meal_name == "Баклажаны с\n   пармезаном")
    {
        description = "Терпкий, сырный, чудесный.";
        price = 200;
        ingredients << Ingredient {"Eggplant", "Баклажан", "Растение", "1"}
                    << Ingredient {"Tomato", "Помидор", "Растение", "1"};
        energy = "175";
        health = "78";
        effects << Effect {"Mining", "Горное дело (+1)"}
                << Effect {"Defense", "Защита (+3)"};
        time_impact[0] = 4;
        time_impact[1] = 40;
        stores = "• Готовка";
        sources = person("Lewis", "Льюис", 7);
    }
    else if (meal_name == "Банановый пудинг")
    {
        description = "Кремовый десерт с прекрасным\nтропическим вкусом.";
        price = 260;
        ingredients << Ingredient {"Banana", "Банан", "Растение", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "125";
        health = "56";
        effects << Effect {"Mining", "Горное дело (+1)"}
                << Effect {"Luck", "Удача (+1)"}
                << Effect {"Defense", "Защита (+1)"};
        time_impact[0] = 5;
        stores = "• Готовка";
        sources = tableItem(image("Island_Trader_Icon", 38 * y), "☐ Торговец Острова", true);
    }
    else if (meal_name == "Биск из омара")
    {
        description = "Этот деликатный суп -\nфамильный рецепт Вилли.";
        price = 205;
        ingredients << Ingredient {"Lobster", "Лобстер", "Рачок", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"};
        energy = "225";
        health = "101";
        effects << Effect {"Fishing", "Рыбная ловля (+3)"}
                << Effect {"Max_Energy", "Максимальная<br>энергия (+50)"};
        time_impact[0] = 16;
        time_impact[1] = 47;
        stores = "• Готовка";
        sources = queenOfSauce("зима 14", 2) + person("Willy", "Вилли", 9);
    }
    else if (meal_name == "Блины")
    {
        description = "Двойная стопка пышных,\nмягких блинов.";
        price = 80;
        ingredients << Ingredient {"Egg", "Яйцо", "Животный продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"};
        energy = "90";
        health = "40";
        effects << Effect {"Foraging", "Собирательство (+2)"};
        time_impact[0] = 11;
        time_impact[1] = 11;
        recipes << Recipe {"Complete_Breakfast", "Полный завтрак"};
        stores = "• Готовка";
        sources = queenOfSauce("лето 14", 1) + "<br>• Салун — 100" + coin;
    }
    else if (meal_name == "Брускетта")
    {
        description = "Жареные помидоры на\nхрустящем белом хлебе.";
        price = 210;
        ingredients << Ingredient {"Bread", "Хлеб", "Еда", "1"}
                    << Ingredient {"Tomato", "Помидор", "Растение", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "113";
        health = "50";
        stores = "• Готовка";
        sources = queenOfSauce("зима 21", 2);
    }
    else if (meal_name == "«Бургер для выживания»")
    {
        description = "Удобная закуска для исследователя.";
        price = 180;
        ingredients << Ingredient {"Bread", "Хлеб", "Еда", "1"}
                    << Ingredient {"Cave_Carrot", "Пещерная морковка", "Продукт", "1"}
                    << Ingredient {"Eggplant", "Баклажан", "Растение", "1"};
        energy = "125";
        health = "56";
        effects << Effect {"Foraging", "Собирательство (+3)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        stores = "• Готовка";
        sources = tableItem(image("Foraging_Skill_Icon", 38 * y), "☐ Собирательство уровень 2", true);
    }
    else if (meal_name == "Глазированный батат")
    {
        description = "Сладкий и сытный...\nСахар придаёт ему карамельный\nпривкус.";
        price = 200;
        ingredients << Ingredient {"Yam", "Батат", "Растение", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "200";
        health = "90";
        stores = "• Готовка";
        sources = queenOfSauce("осень 21", 1);
    }
    else if (meal_name == "«Дары осени»")
    {
        description = "Попробуй осень на вкус.";
        price = 350;
        ingredients << Ingredient {"Yam", "Батат", "Растение", "1"}
                    << Ingredient {"Pumpkin", "Тыква", "Растение", "1"};
        energy = "220";
        health = "99";
        effects << Effect {"Foraging", "Собирательство (+2)"}
                << Effect {"Defense", "Защита (+2)"};
        time_impact[0] = 7;
        time_impact[1] = 41;
        stores = "• Готовка" + tableItem(image("Bundle_Purple", 38 * y), "☐ Экзотический узелок");
        sources = person("Demetrius", "Деметриус", 7);
    }
    else if (meal_name == "Жареный кальмар")
    {
        description = "Его нужно долго жевать.";
        price = 150;
        ingredients << Ingredient {"Squid", "Кальмар", "Рыба", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "80";
        health = "36";
        stores = "• Готовка";
        sources = person("Jodi", "Джоди", 3);
    }
    else if (meal_name == "Жареный угорь")
    {
        description = "Жирный, но богат вкусом.";
        price = 120;
        ingredients << Ingredient {"Eel", "Угорь", "Рыба", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "75";
        health = "33";
        effects << Effect {"Luck", "Удача (+1)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("George", "Джордж", 3);
    }
    else if (meal_name == "Жареный фундук")
    {
        description = "Жарка придаёт богатый лесной вкус.";
        price = 270;
        ingredients << Ingredient {"Hazelnut", "Фундук", "Собирательство", "3"};
        energy = "175";
        health = "78";
        stores = "• Готовка";
        sources = queenOfSauce("лето 28", 2);
    }
    else if (meal_name == "Жареные грибы")
    {
        description = "Насыщенные и ароматные.";
        price = 200;
        ingredients << Ingredient {"Common_Mushroom", "Шампиньон", "Собирательство", "1"}
                    << Ingredient {"Morel", "Сморчок", "Собирательство", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "135";
        health = "60";
        effects << Effect {"Attack", "Атака (+2)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("Demetrius", "Деметриус", 3);
    }
    else if (meal_name == "Жаркое из фасоли")
    {
        description = "Очень здоровая пища.";
        price = 100;
        ingredients << Ingredient {"Green_Bean", "Зелёная фасоль", "Растение", "2"};
        energy = "125";
        health = "56";
        effects << Effect {"Max_Energy", "Максимальная<br>энергия (+30)"}
                << Effect {"Magnetism", "Магнетизм (+32)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("Clint", "Клинт", 7);
    }
    else if (meal_name == "Запечённая рыба")
    {
        description = "Запечённая рыба на\nподушке из трав.";
        price = 100;
        ingredients << Ingredient {"Sunfish", "Санфиш", "Рыба", "1"}
                    << Ingredient {"Bream", "Лещ", "Рыба", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"};
        energy = "75";
        health = "33";
        stores = "• Готовка";
        sources = queenOfSauce("лето 7", 1);
    }
    else if (meal_name == "Имбирный эль")
    {
        description = "Остренькая содовая, знаменитая своим\nуспокаивающим действием на желудок.";
        price = 200;
        ingredients << Ingredient {"Ginger", "Имбирь", "Собирательство", "3"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "63";
        health = "28";
        effects << Effect {"Luck", "Удача (+1)"};
        time_impact[0] = 5;
        stores = "• Готовка";
        sources = "• Магазин дварфа в вулкане — 1000" + coin;;
    }
    else if (meal_name == "Капустный салат")
    {
        description = "Лёгкий, свежий, и очень полезный.";
        price = 345;
        ingredients << Ingredient {"Red_Cabbage", "Красная капуста", "Растение", "1"}
                    << Ingredient {"Vinegar", "Уксус", "Продукт", "1"}
                    << Ingredient {"Mayonnaise", "Майонез", "Продукт", "1"};
        energy = "213";
        health = "95";
        stores = "• Готовка";
        sources = queenOfSauce("весна 14", 1);
    }
    else if (meal_name == "Карповый сюрприз")
    {
        description = "Жирный и безвкусный.";
        price = 150;
        ingredients << Ingredient {"Carp", "Карп", "Рыба", "4"};
        energy = "90";
        health = "40";
        stores = "• Готовка";
        sources = queenOfSauce("лето 7", 2);
    }
    else if (meal_name == "Кленовый пончик")
    {
        description = "Сладкий пончик с богатой\nкленовой глазурью.";
        price = 300;
        ingredients << Ingredient {"Maple_Syrup", "Кленовый сироп", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"};
        energy = "225";
        health = "101";
        effects << Effect {"Farming", "Фермерство (+1)"}
                << Effect {"Fishing", "Рыбная ловля (+1)"}
                << Effect {"Mining", "Горное дело (+1)"};
        time_impact[0] = 16;
        time_impact[1] = 47;
        stores = "• Готовка";
        sources = queenOfSauce("лето 14", 2);
    }
    else if (meal_name == "Клюквенная сласть")
    {
        description = "Она достаточно сладкая,\nчтобы перебить горечь ягод.";
        price = 175;
        ingredients << Ingredient {"Cranberries", "Клюква", "Растение", "1"}
                    << Ingredient {"Apple", "Яблоко", "Растение", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "125";
        health = "56";
        stores = "• Готовка";
        sources = queenOfSauce("зима 28", 1);
    }
    else if (meal_name == "Клюквенный соус")
    {
        description = "Праздничное угощение.";
        price = 120;
        ingredients << Ingredient {"Cranberries", "Клюква", "Растение", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "125";
        health = "56";
        effects << Effect {"Mining", "Горное дело (+2)"};
        time_impact[0] = 3;
        time_impact[1] = 30;
        stores = "• Готовка";
        sources = person("Gus", "Гас", 7);
    }
    else if (meal_name == "Коренья на блюде")
    {
        description = "Вам захочется добавки!";
        price = 100;
        ingredients << Ingredient {"Cave_Carrot", "Пещерная морковка", "Продукт", "1"}
                    << Ingredient {"Winter_Root", "Зимний корень", "Собирательство", "1"};
        energy = "125";
        health = "56";
        effects << Effect {"Attack", "Атака (+3)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        stores = "• Готовка";
        sources = tableItem(image("Combat_Skill_Icon", 38 * y), "☐ Боевые навыки уровень 3", true);
    }
    else if (meal_name == "Крабовые котлетки")
    {
        description = "Крабовое мясо, хлебные крошки и яйцо\nв форме котлеток, зажаренные до\nзолотистой корочки.";
        price = 275;
        ingredients << Ingredient {"Crab", "Краб", "Рачок", "1"}
                    << Ingredient {"Egg", "Яйцо", "Животный продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "225";
        health = "101";
        effects << Effect {"Speed", "Скорость (+1)"}
                << Effect {"Defense", "Защита (+1)"};
        time_impact[0] = 16;
        time_impact[1] = 47;
        stores = "• Готовка";
        sources = queenOfSauce("осень 21", 2);
    }
    else if (meal_name == "Красное блюдо")
    {
        description = "В нём полно антиоксидантов.";
        price = 400;
        ingredients << Ingredient {"Red_Cabbage", "Красная капуста", "Растение", "1"}
                    << Ingredient {"Radish", "Редис", "Растение", "1"};
        energy = "240";
        health = "108";
        effects << Effect {"Max_Energy", "Максимальная<br>энергия (+50)"};
        time_impact[0] = 3;
        time_impact[1] = 30;
        stores = "• Готовка";
        sources = person("Emily", "Эмили", 7);
    }
    else if (meal_name == "Креветочный коктейль")
    {
        description = "Дорогостоящий аперитив из\nсвежевыловленных креветок.";
        price = 160;
        ingredients << Ingredient {"Shrimp", "Креветка", "Рачок", "1"}
                    << Ingredient {"Tomato", "Помидор", "Растение", "1"}
                    << Ingredient {"Wild_Horseradish", "Дикий хрен", "Собирательство", "1"};
        energy = "225";
        health = "101";
        effects << Effect {"Fishing", "Рыбная ловля (+1)"}
                << Effect {"Luck", "Удача (+1)"};
        time_impact[0] = 10;
        time_impact[1] = 2;
        stores = "• Готовка";
        sources = queenOfSauce("зима 28", 2);
    }
    else if (meal_name == "Манговый клейкий рис")
    {
        description = "Сладкое манго и кокос превращают\nэтот рис во что-то невероятное.";
        price = 250;
        ingredients << Ingredient {"Mango", "Манго", "Растение", "1"}
                    << Ingredient {"Coconut", "Кокос", "Собирательство", "1"}
                    << Ingredient {"Rice", "Рис", "Продукт", "1"};
        energy = "113";
        health = "50";
        effects << Effect {"Defense", "Защита (+13)"};
        time_impact[0] = 5;
        stores = "• Готовка";
        sources = person("Leo", "Лео", 7);
    }
    else if (meal_name == "Маффин с маком")
    {
        description = "Оказывает успокаивающий эффект.";
        price = 250;
        ingredients << Ingredient {"Poppy", "Мак", "Растение", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "150";
        health = "67";
        stores = "• Готовка";
        sources = queenOfSauce("зима 7", 2);
    }
    else if (meal_name == "Мороженое")
    {
        description = "Сложно найти того, кому оно\nне нравится.";
        price = 120;
        ingredients << Ingredient {"Milk", "Ведро молока", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "100";
        health = "45";
        stores = "• Готовка<br>• Магазин мороженого<br>• Оазис";
        sources = person("Jodi", "Джоди", 7);
    }
    else if (meal_name == "«Морское блюдо»")
    {
        description = "Это блюдо согреет, когда подует\nхолодный морской ветер.";
        price = 220;
        ingredients << Ingredient {"Sardine", "Сардина", "Рыба", "2"}
                    << Ingredient {"Hashbrowns", "Хашбраун", "Еда", "1"};
        energy = "125";
        health = "56";
        effects << Effect {"Fishing", "Рыбная ловля (+3)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        stores = "• Готовка";
        sources = tableItem(image("Fishing_Skill_Icon", 38 * y), "☐ Рыбная ловля уровень 3", true);
    }
    else if (meal_name == "Морской пенный пудинг")
    {
        description = "Этот солёный пудинг действительно\nпогрузит вас в морское мышление!";
        price = 300;
        ingredients << Ingredient {"Flounder", "Мелкая камбала", "Рыба", "1"}
                    << Ingredient {"Midnight_Carp", "Полуночный карп", "Рыба", "1"}
                    << Ingredient {"Squid_Ink", "Чернила кальмара", "Продукт", "1"};
        energy = "175";
        health = "78";
        effects << Effect {"Fishing", "Рыбная ловля (+4)"};
        time_impact[0] = 3;
        time_impact[1] = 30;
        stores = "• Готовка";
        sources = tableItem(image("Fishing_Skill_Icon", 38 * y), "☐ Рыбная ловля уровень 9", true);
    }
    else if (meal_name == "Обед на удачу")
    {
        description = "Особое блюдо.";
        price = 250;
        ingredients << Ingredient {"Sea_Cucumber", "Морской огурец", "Рыба", "1"}
                    << Ingredient {"Tortilla", "Тортилья", "Еда", "1"}
                    << Ingredient {"Blue_Jazz", "Синий яркоцвет", "Растение", "1"};
        energy = "100";
        health = "45";
        effects << Effect {"Luck", "Удача (+3)"};
        time_impact[0] = 11;
        time_impact[1] = 11;
        stores = "• Готовка";
        sources = queenOfSauce("весна 28", 2);
    }
    else if (meal_name == "«Обед фермера»")
    {
        description = "Он даст тебе сил.";
        price = 150;
        ingredients << Ingredient {"Omelet", "Омлет", "Еда", "1"}
                    << Ingredient {"Parsnip", "Пастернак", "Растение", "1"};
        energy = "200";
        health = "90";
        effects << Effect {"Farming", "Фермерство (+3)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        stores = "• Готовка";
        sources = tableItem(image("Farming_Skill_Icon", 38 * y), "☐ Фермерство уровень 3", true);
    }
    else if (meal_name == "«Объеденье»")
    {
        description = "Ах, запах свежего хлеба и шалфея.";
        price = 165;
        ingredients << Ingredient {"Bread", "Хлеб", "Еда", "1"}
                    << Ingredient {"Cranberries", "Клюква", "Растение", "1"}
                    << Ingredient {"Hazelnut", "Фундук", "Собирательство", "1"};
        energy = "170";
        health = "76";
        effects << Effect {"Defense", "Защита (+2)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        stores = "• Готовка";
        sources = person("Pam", "Пэм", 7);
    }
    else if (meal_name == "Овощное ассорти")
    {
        description = "Очень питательно.";
        price = 120;
        ingredients << Ingredient {"Tomato", "Помидор", "Растение", "1"}
                    << Ingredient {"Beet", "Свекла", "Растение", "1"};
        energy = "165";
        health = "74";
        stores = "• Готовка";
        sources = person("Caroline", "Кэролайн", 7);
    }
    else if (meal_name == "Окунь с хрустящей\n   корочкой")
    {
        meal_name = "Окунь с хрустящей\nкорочкой";
        description = "Ого, панировка просто безупречна.";
        price = 150;
        ingredients << Ingredient {"Largemouth_Bass", "Большеротый окунь", "Рыба", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "90";
        health = "40";
        effects << Effect {"Magnetism", "Магнетизм (+64)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("Kent", "Кент", 7);
    }
    else if (meal_name == "Омлет")
    {
        description = "Он очень пышный.";
        price = 125;
        ingredients << Ingredient {"Egg", "Яйцо", "Продукт", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"};
        energy = "100";
        health = "45";
        recipes << Recipe {"Farmer_Lunch", "«Обед фермера»"};
        stores = "• Готовка";
        sources = queenOfSauce("весна 28", 1) + "<br>• Салун — 100" + coin;
    }
    else if (meal_name == "Острый угорь")
    {
        description = "Очень острый! Осторожно.";
        price = 175;
        ingredients << Ingredient {"Eel", "Угорь", "Рыба", "1"}
                    << Ingredient {"Hot_Pepper", "Жгучий перец", "Растение", "1"};
        energy = "115";
        health = "51";
        effects << Effect {"Luck", "Удача (+1)"}
                << Effect {"Speed", "Скорость (+1)"};
        time_impact[0] = 7;
        stores = "• Готовка<br>• Змей";
        sources = person("George", "Джордж", 7);
    }
    else if (meal_name == "Перечные бомбочки")
    {
        description = "Панированный жгучий перец,\nфаршированный сыром.";
        price = 200;
        ingredients << Ingredient {"Hot_Pepper", "Жгучий перец", "Растение", "1"}
                    << Ingredient {"Cheese", "Сыр", "Продукт", "1"};
        energy = "130";
        health = "58";
        effects << Effect {"Farming", "Фермерство (+2)"}
                << Effect {"Speed", "Скорость (+1)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("Shane", "Шейн", 3);
    }
    else if (meal_name == "Печенье")
    {
        description = "Очень вязкое.";
        price = 140;
        ingredients << Ingredient {"Egg", "Яйцо", "Животный продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "90";
        health = "40";
        stores = "• Готовка";
        sources = person("Evelyn", "Эвелин", 4);
    }
    else if (meal_name == "Пирог с ревенем")
    {
        description = "М-м, какой терпкий и сладкий!";
        price = 400;
        ingredients << Ingredient {"Rhubarb", "Ревень", "Растение", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "215";
        health = "96";
        stores = "• Готовка";
        sources = person("Marnie", "Марни", 7);
    }
    else if (meal_name == "Пицца")
    {
        description = "Она популярна по многим причинам.";
        price = 300;
        ingredients << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Tomato", "Помидор", "Растение", "1"}
                    << Ingredient {"Cheese", "Сыр", "Продукт", "1"};
        energy = "150";
        health = "67";
        stores = "• Готовка<br>• Салун — 600" + coin;
        sources = queenOfSauce("весна 7", 2);
    }
    else if (meal_name == "Пои")
    {
        description = "Традиционное блюдо с\nтонким сладковатым вкусом.\nЛучше есть его свежим.";
        price = 400;
        ingredients << Ingredient {"Taro_Root", "Корень Таро", "Растение", "4"};
        energy = "75";
        health = "33";
        stores = "• Готовка";
        sources = person("Leo", "Лео", 3);
    }
    else if (meal_name == "Полный завтрак")
    {
        description = "После такого всe по плечу!";
        price = 350;
        ingredients << Ingredient {"Fried_Egg", "Яичница", "Еда", "1"}
                    << Ingredient {"Hashbrowns", "Хашбраун", "Еда", "1"}
                    << Ingredient {"Pancakes", "Блины", "Еда", "1"};
        energy = "200";
        health = "90";
        effects << Effect {"Farming", "Фермерство (+2)"}
                << Effect {"Max_Energy", "Максимальная<br>энергия (+50)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = queenOfSauce("весна 21", 2);
    }
    else if (meal_name == "Ризотто с папоротником")
    {
        description = "Сливочное рисовое блюдо, которое\nподают с обжаренными ростками\nпапоротника. Вкус не очень яркий.";
        price = 350;
        ingredients << Ingredient {"Fiddlehead_Fern", "Съедобный папоротник", "Собирательство", "1"}
                    << Ingredient {"Garlic", "Чеснок", "Растение", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "225";
        health = "101";
        stores = "• Готовка";
        sources = queenOfSauce("осень 28", 2);
    }
    else if (meal_name == "Рисовый пудинг")
    {
        description = "У него сливочный, сладкий вкус.";
        price = 260;
        ingredients << Ingredient {"Milk", "Ведро молока", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"}
                    << Ingredient {"Rice", "Рис", "Продукт", "1"};
        energy = "115";
        health = "51";
        stores = "• Готовка";
        sources = person("Evelyn", "Эвелин", 7);
    }
    else if (meal_name == "Розовый торт")
    {
        description = "Сверху на нём конфеты в\nвиде сердечек.";
        price = 480;
        ingredients << Ingredient {"Egg", "Яйцо", "Продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Melon", "Дыня", "Растение", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "250";
        health = "112";
        stores = "• Готовка<br>• Письмо от мамы" + tableItem(image("Bundle_Red", 38 * y), "☐ Поварский узелок");
        sources = queenOfSauce("лето 21", 2);
    }
    else if (meal_name == "Рыбная похлёбка")
    {
        description = "Отлично согревает после ночи\nна холодном море.";
        price = 135;
        ingredients << Ingredient {"Clam", "Гребешок", "Собирательство", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"};
        energy = "225";
        health = "101";
        effects << Effect {"Fishing", "Рыбная ловля (+1)"};
        time_impact[0] = 16;
        time_impact[1] = 47;
        stores = "• Готовка";
        sources = person("Willy", "Вилли", 3);
    }
    else if (meal_name == "Рыбные тако")
    {
        description = "Пахнет так вкусно.";
        price = 500;
        ingredients << Ingredient {"Tuna", "Тунец", "Рыба", "1"}
                    << Ingredient {"Tortilla", "Тортилья", "Еда", "1"}
                    << Ingredient {"Red_Cabbage", "Красная капуста", "Растение", "1"};
        energy = "165";
        health = "74";
        effects << Effect {"Fishing", "Рыбная ловля (+2)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("Linus", "Линус", 7);
    }
    else if (meal_name == "Салат")
    {
        description = "Полезный огородный салат.";
        price = 110;
        ingredients << Ingredient {"Leek", "Лук-порей", "Собирательство", "1"}
                    << Ingredient {"Dandelion", "Одуванчик", "Собирательство", "1"}
                    << Ingredient {"Vinegar", "Уксус", "Продукт", "1"};
        energy = "113";
        health = "50";
        stores = "• Готовка<br>• Салун — 220" + coin;
        sources = person("Emily", "Эмили", 3);
    }
    else if (meal_name == "Салат из редиса")
    {
        description = "Редиска такая хрустящая!";
        price = 300;
        ingredients << Ingredient {"Radish", "Редис", "Растение", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"}
                    << Ingredient {"Vinegar", "Уксус", "Продукт", "1"};
        energy = "200";
        health = "90";
        stores = "• Готовка";
        sources = queenOfSauce("весна 21", 1);
    }
    else if (meal_name == "Сашими")
    {
        description = "Сырая рыба, порезанная\nна тонкие кусочки.";
        price = 75;
        ingredients << Ingredient {"Fish", "Любая рыба", "Любая рыба", "1"};
        energy = "75";
        health = "33";
        stores = "• Готовка";
        sources = person("Linus", "Линус", 3);
    }
    else if (meal_name == "Светлый бульон")
    {
        description = "Нежный бульон с серными нотками.";
        price = 150;
        ingredients << Ingredient {"White_Algae", "Белая водоросль", "Продукт", "2"};
        energy = "125";
        health = "56";
        stores = "• Готовка";
        sources = person("Marnie", "Марни", 3);
    }
    else if (meal_name == "Сливовый пудинг")
    {
        description = "Традиционное праздничное лакомство.";
        price = 260;
        ingredients << Ingredient {"Wild_Plum", "Чёрная слива", "Растение", "2"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "175";
        health = "78";
        stores = "• Готовка";
        sources = queenOfSauce("зима 7", 1);
    }
    else if (meal_name == "Спагетти")
    {
        description = "Старая классика.";
        price = 120;
        ingredients << Ingredient {"Tomato", "Помидор", "Растение", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"};
        energy = "75";
        health = "33";
        stores = "• Готовка<br>• Салун — 240" + coin;
        sources = person("Lewis", "Льюис", 3);
    }
    else if (meal_name == "Стир-фрай")
    {
        description = "Обжаренные овощи с рисом.";
        price = 335;
        ingredients << Ingredient {"Cave_Carrot", "Пещерная морковка", "Продукт", "1"}
                    << Ingredient {"Common_Mushroom", "Шампиньон", "Собирательство", "1"}
                    << Ingredient {"Kale", "Кудрявая капуста", "Растение", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "90";
        health = "200";
        stores = "• Готовка";
        sources = queenOfSauce("весна 7", 1);
    }
    else if (meal_name == "Странная булочка")
    {
        description = "Что там внутри?";
        price = 225;
        ingredients << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Periwinkle", "Литорина", "Рачок", "1"}
                    << Ingredient {"Void_Mayonnaise", "Майонез пустоты", "Продукт", "1"};
        energy = "100";
        health = "45";
        stores = "• Готовка<br>• Теневой воротила<br>• Странствующий торговец";
        sources = person("Shane", "Шейн", 7);
    }
    else if (meal_name == "Суп из водрослей")
    {
        description = "В нём есть немного слизи.";
        price = 100;
        ingredients << Ingredient {"Green_Algae", "Зелёная водоросль", "Продукт", "4"};
        energy = "75";
        health = "33";
        stores = "• Готовка<br>• Личинка-мутант (10%)";
        sources = person("Clint", "Клинт", 3);
    }
    else if (meal_name == "Суп из пастернака")
    {
        description = "Свежий и сытный.";
        price = 120;
        ingredients << Ingredient {"Parsnip", "Пастернак", "Растение", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Животный продукт", "1"}
                    << Ingredient {"Vinegar", "Уксус", "Кулинария", "1"};
        energy = "85";
        health = "38";
        stores = "• Готовка";
        sources = person("Caroline", "Кэролайн", 3);
    }
    else if (meal_name == "Суп том кха")
    {
        description = "Невероятное смешение вкусов!";
        price = 250;
        ingredients << Ingredient {"Coconut", "Кокос", "Собирательство", "1"}
                    << Ingredient {"Shrimp", "Креветка", "Рачок", "1"}
                    << Ingredient {"Common_Mushroom", "Шампиньон", "Собирательство", "1"};
        energy = "175";
        health = "78";
        effects << Effect {"Farming", "Фермерство (+2)"}
                << Effect {"Max_Energy", "Максимальная<br>энергия (+30)"};
        time_impact[0] = 7;
        stores = "• Готовка";
        sources = person("Sandy", "Сэнди", 7);
    }
    else if (meal_name == "Суши-роллы")
    {
        description = "Рис с рыбой, завёрнутые в водоросли.";
        price = 220;
        ingredients << Ingredient {"Fish", "Любая рыба", "Любая рыба", "1"}
                    << Ingredient {"Seaweed", "Морская водоросль", "Продукт", "1"}
                    << Ingredient {"Rice", "Рис", "Продукт", "1"};
        energy = "100";
        health = "45";
        stores = "• Готовка";
        sources = queenOfSauce("лето 21", 1) + "• Салун — 1500" + coin;
    }
    else if (meal_name == "Тортилья")
    {
        description = "Можно положить в неe что-то ещё,\nа можно съесть просто так.";
        price = 50;
        ingredients << Ingredient {"Corn", "Кукуруза", "Растение", "1"};
        energy = "50";
        health = "22";
        recipes << Recipe {"Fish_Taco", "Рыбные тако"}
                << Recipe {"Lucky_Lunch", "Обед на удачу"};
        stores = "• Готовка";
        sources = queenOfSauce("осень 7", 1) + "• Салун — 100" + coin;
    }
    else if (meal_name == "Тройной эспрессо")
    {
        description = "Намного крепче обычного кофе!";
        price = 450;
        ingredients << Ingredient {"Coffee", "Кофе", "Продукт", "3"};
        energy = "8";
        health = "3";
        effects << Effect {"Speed", "Скорость (+1)"};
        time_impact[0] = 4;
        time_impact[1] = 12;
        stores = "• Готовка<br>• Музей<br>• Меняла";
        sources = "• Салун — 5000" + coin;
    }
    else if (meal_name == "Тропическое карри")
    {
        description = "Экзотическое ароматное карри\nв миске из ананаса.";
        price = 500;
        ingredients << Ingredient {"Coconut", "Кокос", "Собирательство", "1"}
                    << Ingredient {"Pineapple", "Ананас", "Растение", "1"}
                    << Ingredient {"Hot_Pepper", "Жгучий перец", "Растение", "1"};
        energy = "150";
        health = "67";
        effects << Effect {"Foraging", "Собирательство (+4)"};
        time_impact[0] = 5;
        stores = "• Готовка";
        sources = "• Пляжный курорт";
    }
    else if (meal_name == "Тыквенный пирог")
    {
        description = "Шёлковый тыквенный крем\nпод корочкой с хлопьями.";
        price = 385;
        ingredients << Ingredient {"Pumpkin", "Тыква", "Растение", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "225";
        health = "101";
        stores = "• Готовка";
        sources = queenOfSauce("зима 21", 1);
    }
    else if (meal_name == "Тыквенный суп")
    {
        description = "Любимое блюдо сезона.";
        price = 300;
        ingredients << Ingredient {"Pumpkin", "Тыква", "Растение", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"};
        energy = "90";
        health = "200";
        effects << Effect {"Defense", "Защита (+2)"}
                << Effect {"Luck", "Удача (+2)"};
        time_impact[0] = 7;
        time_impact[1] = 41;
        stores = "• Готовка";
        sources = person("Robin", "Робин", 7);
    }
    else if (meal_name == "Ужин из лосося")
    {
        description = "Немного лимонного сока\nпридаёт ему особенный вкус.";
        price = 300;
        ingredients << Ingredient {"Salmon", "Лосось", "Рыба", "1"}
                    << Ingredient {"Amaranth", "Амарант", "Растение", "1"}
                    << Ingredient {"Kale", "Кудрявая капуста", "Растение", "1"};
        energy = "125";
        health = "56";
        stores = "• Готовка";
        sources = person("Gus", "Гас", 3);
    }
    else if (meal_name == "Уха")
    {
        description = "Она пахнет совсем как море.\nНа вкус, правда, гораздо лучше.";
        price = 175;
        ingredients << Ingredient {"Crayfish", "Рак", "Рачок", "1"}
                    << Ingredient {"Mussel", "Мидия", "Рачок", "1"}
                    << Ingredient {"Periwinkle", "Литорина", "Рачок", "1"}
                    << Ingredient {"Tomato", "Помидор", "Растение", "1"};
        energy = "225";
        health = "101";
        effects << Effect {"Fishing", "Рыбная ловля (+3)"};
        time_impact[0] = 16;
        time_impact[1] = 47;
        stores = "• Готовка";
        sources = person("Willy", "Вилли", 7);
    }
    else if (meal_name == "Уха из форели")
    {
        description = "Солёная.";
        price = 100;
        ingredients << Ingredient {"Rainbow_Trout", "Радужная форель", "Рыба", "1"}
                    << Ingredient {"Green_Algae", "Зелёная водоросль", "Продукт", "1"};
        energy = "100";
        health = "45";
        stores = "• Готовка<br>• Рыбацкий магазин — 250" + coin;
        sources = queenOfSauce("осень 14", 1);
    }
    else if (meal_name == "Фруктовый салат")
    {
        description = "Вкусное сочетание летних фруктов.";
        price = 450;
        ingredients << Ingredient {"Blueberry", "Черника", "Растение", "1"}
                    << Ingredient {"Apricot", "Абрикос", "Растение", "1"}
                    << Ingredient {"Melon", "Дыня", "Растение", "1"};
        energy = "263";
        health = "118";
        stores = "• Готовка";
        sources = queenOfSauce("осень 7", 2);
    }
    else if (meal_name == "Хашбраун")
    {
        description = "Золотистые и хрустящие!";
        price = 120;
        ingredients << Ingredient {"Potato", "Картофель", "Растение", "1"}
                    << Ingredient {"Oil", "Масло", "Продукт", "1"};
        energy = "90";
        health = "40";
        effects << Effect {"Farming", "Фермерство (+1)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        recipes << Recipe {"Complete_Breakfast", "Полный завтрак"}
                << Recipe {"Sea_Dish", "«Морское блюдо»"};
        stores = "• Готовка";
        sources = queenOfSauce("весна 14", 2) + "<br>• Салун — 50" + coin;
    }
    else if (meal_name == "Хлеб")
    {
        description = "Хрустящий багет.";
        price = 60;
        ingredients << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"};
        energy = "50";
        health = "22";
        recipes << Recipe {"Bruschetta", "Брускетта"}
                << Recipe {"Stuffing", "«Объеденье»"}
                << Recipe {"Survival_Burger", "«Бургер для выживания»"};
        stores = "• Готовка<br>• Салун — 120" + coin;
        sources = queenOfSauce("лето 28", 2) + "<br>• Салун — 100" + coin;
    }
    else if (meal_name == "Цветная капуста\n   с сыром")
    {
        meal_name = "Цветная капуста\nс сыром";
        description = "Пахнет восхитительно!";
        price = 300;
        ingredients << Ingredient {"Cauliflower", "Цветная капуста", "Растение", "1"}
                    << Ingredient {"Cheese", "Сыр", "Продукт", "1"};
        energy = "138";
        health = "62";
        stores = "• Готовка";
        sources = person("Pam", "Пэм", 3);
    }
    else if (meal_name == "Черничный коблер")
    {
        description = "Другого такого вкуса не бывает.";
        price = 260;
        ingredients << Ingredient {"Blackberry", "Ежевика", "Растение", "2"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"};
        energy = "175";
        health = "78";
        stores = "• Готовка";
        sources = queenOfSauce("осень 14", 2);
    }
    else if (meal_name == "Черничный пирог")
    {
        description = "Тонкий, освежающий вкус.";
        price = 150;
        ingredients << Ingredient {"Blueberry", "Черника", "Растение", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"}
                    << Ingredient {"Egg", "Яйцо", "Животный продукт", "1"};
        energy = "125";
        health = "56";
        stores = "• Готовка";
        sources = person("Pierre", "Пьер", 3);
    }
    else if (meal_name == "Чудо-блюдо")
    {
        description = "Это блюдо даёт много энергии.";
        price = 220;
        ingredients << Ingredient {"Bok_Choy", "Бок-чой", "Растение", "1"}
                    << Ingredient {"Cranberries", "Клюква", "Растение", "1"}
                    << Ingredient {"Artichoke", "Артишок", "Растение", "1"};
        energy = "160";
        health = "72";
        effects << Effect {"Max_Energy", "Максимальная<br>энергия (+40)"}
                << Effect {"Speed", "Скорость (+1)"};
        time_impact[0] = 3;
        time_impact[1] = 30;
        stores = "• Готовка";
        sources = person("Kent", "Кент", 7);
    }
    else if (meal_name == "«Шахтерский леденец»")
    {
        description = "С ним вам хватит энергии.";
        price = 200;
        ingredients << Ingredient {"Cave_Carrot", "Пещерная морковка", "Продукт", "2"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"}
                    << Ingredient {"Milk", "Ведро молока", "Продукт", "1"};
        energy = "125";
        health = "56";
        effects << Effect {"Mining", "Горное дело (+3)"}
                << Effect {"Magnetism", "Магнетизм (+32)"};
        time_impact[0] = 5;
        time_impact[1] = 35;
        stores = "• Готовка<br>• Дварф<br>• Мумия (4%)";
        sources = tableItem(image("Mining_Skill_Icon", 38 * y), "☐ Горное дело уровень 2", true);
    }
    else if (meal_name == "Шоколадный торт")
    {
        description = "Влажный, насыщенный, с толстым\nслоем фаджа сверху.";
        price = 200;
        ingredients << Ingredient {"Egg", "Яйцо", "Животный продукт", "1"}
                    << Ingredient {"Wheat_Flour", "Пшеничная мука", "Продукт", "1"}
                    << Ingredient {"Sugar", "Сахар", "Продукт", "1"};
        energy = "150";
        health = "67";
        stores = "• Готовка<table><tr><td>" + image("Bundle_Red", 38 * y) +
                  "</td><td>2500</td><td>" + coin + "</td><td>узелок</td></tr></table>";
        sources = queenOfSauce("зима 14", 1);
    }
    else if (meal_name == "Эскарго")
    {
        description = "Идеально приготовленные\nулитки в масле.";
        price = 125;
        ingredients << Ingredient {"Snail", "Улитка", "Рачок", "1"}
                    << Ingredient {"Garlic", "Чеснок", "Растение", "1"};
        energy = "50";
        health = "22";
        effects << Effect {"Fishing", "Рыбная ловля (+2)"};
        time_impact[0] = 16;
        time_impact[1] = 47;
        stores = "• Готовка";
        sources = person("Willy", "Вилли", 5);
    }
    else if (meal_name == "Яичница")
    {
        description = "Глазунья.";
        price = 35;
        ingredients << Ingredient {"Egg", "Яйцо", "Животный продукт", "1"};
        energy = "50";
        health = "22";
        recipes << Recipe {"Complete_Breakfast", "Полный завтрак"};
        stores = "• Готовка<br>• Салун — 70" + coin;
        sources = "Первое расширение дома";
    }

    ui->meal_name->setText(meal_name);
    ui->meal_icon->setPixmap(QPixmap(":/res/resourses/" + image_path + ".png").scaled(100 * y, 100 * y, Qt::KeepAspectRatio));
    ui->meal_description->setText(description);
    ui->meal_price->setText(QString("Цена продажи: %1").arg(price) + coin);

    QString ingredient_label = "<table>";
    for (Ingredient &ingredient : ingredients)
    {
        QString href = link(ingredient.icon, ingredient.name, ingredient.type);
        ingredient_label += tableItem(image(ingredient.icon, 46 * y), "☐ %1 (%2)").arg(href).arg(ingredient.count);
    }
    ui->ingredients->setText(ingredient_label + "</table>");

    ui->energy->setText(energy);
    ui->health->setText(health);

    if (effects.isEmpty())
    {
        ui->effectsWidget->close();
        ui->line_4->close();
    }
    else
    {
        ui->effectsWidget->show();
        ui->line_4->show();
        QString effects_label = "<table>";
        for (Effect &effect : effects)
            effects_label += tableItem(image(effect.icon, 38 * y), "☐" + effect.impact);
        QString duration = time_impact[1] == 0 ? QString("%1 мин").arg(time_impact[0])
                                               : QString("%1 мин %2 сек").arg(time_impact[0]).arg(time_impact[1]);
        ui->effects->setText(effects_label + "</table>");
        ui->duration->setText(tableItem(image("Time_Icon", 38 * y), "☐" + duration, true));
    }

    if (recipes.isEmpty())
    {
        ui->meal_recipesWidget->close();
        ui->line_6->close();
    }
    else
    {
        ui->meal_recipesWidget->show();
        ui->line_6->show();
        QString recipes_label = "<table>";
        for (Recipe &recipe : recipes)
            recipes_label += tableItem(image(recipe.icon, 50 * y), "☐" + link(recipe.icon, recipe.name, "Еда"));
        ui->meal_recipes->setText(recipes_label + "</table>");
    }
    ui->stores->setText("<p style=\"line-height:1.2;text-align:left\">" + stores + "</p");
    ui->sources->setText("<p style=\"line-height:1.2;text-align:left\">" + sources + "</p");

    ui->stackedWidget->setCurrentWidget(ui->cooking_info);
}


QString MainWindow::queenOfSauce(const QString &day, quint8 year) {
    return tableItem(image("Cooking_Channel", 50 * y), "☐«Королева соуса»<br>лето %1, год %2", true).arg(day).arg(year);
}

QString MainWindow::person(const QString &icon, const QString &name, const quint8 val) {
    return iconLabel(image(icon + "_Icon", 46), emoji(name + " (письмо — %1☐)", "❤"), 12 * y).arg(val);
}
