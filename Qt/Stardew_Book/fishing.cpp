#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::fishPageInit()
{
    const QString fish[][2] = {
        {"Альбакор", "Albacore"},
        {"Анчоус", "Anchovy"},
        {"Барабулька", "Red_Mullet"},
        {"Берикс", "Red_Snapper"},
        {"Большеротый окунь", "Largemouth_Bass"},
        {"Голавль", "Chub"},
        {"Голубой дискус", "Blue_Discus"},
        {"Дорада", "Dorado"},
        {"Жёлтый судак", "Walleye"},
        {"Жуть-рыба", "Spook_Fish"},
        {"Змеезуб", "Lingcod"},
        {"Кальмар", "Squid"},
        {"Камбала", "Halibut"},
        {"Камень-рыба", "Stonefish"},
        {"Карп", "Carp"},
        {"Лавовый угорь", "Lava_Eel"},
        {"Ледорыб", "Ice_Pip"},
        {"Лесоплюх", "Woodskip"},
        {"Лещ", "Bream"},
        {"Лосось", "Salmon"},
        {"Лосось пустоты", "Void_Salmon"},
        {"Малоротый окунь", "Smallmouth_Bass"},
        {"Мелкая камбала", "Flounder"},
        {"Морской ёрш", "Lionfish"},
        {"Морской огурец", "Sea_Cucumber"},
        {"Ночной кальмар", "Midnight_Squid"},
        {"Окунь", "Perch"},
        {"Осётр", "Sturgeon"},
        {"Осьминог", "Octopus"},
        {"Песковик", "Sandfish"},
        {"Полуночный карп", "Midnight_Carp"},
        {"Пятнистый криворот", "Ghostfish"},
        {"Радужная форель", "Rainbow_Trout"},
        {"Рыба-ёж", "Pufferfish"},
        {"Рыба-пузырь", "Blobfish"},
        {"Санфиш", "Sunfish"},
        {"Сардина", "Sardine"},
        {"Сельдь", "Herring"},
        {"Скат", "Stingray"},
        {"Скорпионовый карп", "Scorpion_Carp"},
        {"Слаймджек", "Slimejack"},
        {"Сом", "Som"},
        {"Сомик", "Bullhead"},
        {"Тигровая форель", "Tiger_Trout"},
        {"Тиляпия", "Tilapia"},
        {"Тунец", "Tuna"},
        {"Угорь", "Eel"},
        {"Чудо-огурец", "Super_Cucumber"},
        {"Шэд", "Shad"},
        {"Щука", "Pike"},
    };
    buttonList(fish, sizeof(fish), &MainWindow::setFishPage);
    ui->stackedWidget->setCurrentWidget(ui->list_page);
}

void MainWindow::setFishPage()
{
    ListButton *button = qobject_cast<ListButton*>(sender());
    fishInfoOpen(button->text().remove(0, 3), button->image_path);
}

void MainWindow::fishInfoOpen(const QString &fish_name, const QString &image_path)
{
    QString areal;
    QString biting_time;
    QStringList season;
    QStringList weather;
    QString difficulty;
    QString behavour;
    QString size;
    quint16 price = 0;
    QStringList roes;

    if (fish_name == "Альбакор")
    {
        areal = "Океан";
        biting_time = "06:00 — 11:00\n18:00 — 02:00";
        season << "Осень" << "Зима";
        weather << "Любая";
        difficulty = "60";
        behavour = "Гибридная";
        size = "51 — 104 см";
        price = 75;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Анчоус")
    {
        areal = "Океан";
        biting_time = "Любое";
        season << "Весна" << "Осень";
        weather << "Любая";
        difficulty = "30";
        behavour = "Дротиковая";
        size = "3 — 43 см";
        price = 30;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Барабулька")
    {
        areal = "Океан";
        biting_time = "06:00 — 19:00";
        season << "Лето" << "Зима";
        weather << "Любая";
        difficulty = "55";
        behavour = "Гладкая";
        size = "20 — 58 см";
        price = 75;
        roes << "Икра красная" << "Вяленая икра красная";
    }
    else if (fish_name == "Берикс")
    {
        areal = "Океан";
        biting_time = "06:00 — 19:00";
        season << "Лето" << "Осень";
        weather << "Дождь";
        difficulty = "40";
        behavour = "Гибридная";
        size = "20 — 66 см";
        price = 50;
        roes << "Икра красная" << "Вяленая икра красная";
    }
    else if (fish_name == "Большеротый окунь")
    {
        areal = "Горное озеро,\nФерма в глуши";
        biting_time = "06:00 — 19:00";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гибридная";
        size = "28 — 79 см";
        price = 100;
        roes << "Икра зелёная" << "Вяленая икра зелёная";
    }
    else if (fish_name == "Голавль")
    {
        areal = "Горное озеро,\nЛесной пруд,\nРека";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "35";
        behavour = "Дротиковая";
        size = "30 — 64 см";
        price = 50;
        roes << "Икра серая" << "Вяленая икра серая";
    }
    else if (fish_name == "Голубой дискус")
    {
        areal = "Имбирный остров";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "60";
        behavour = "Дротиковая";
        size = "5 — 25 см";
        price = 120;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Дорада")
    {
        areal = "Лесная река";
        biting_time = "06:00 — 19:00";
        season << "Лето";
        weather << "Любая";
        difficulty = "78";
        behavour = "Гибридная";
        size = "61 — 84 см";
        price = 100;
        roes << "Икра оранжевая" << "Вяленая икра оранжевая";
    }
    else if (fish_name == "Жёлтый судак")
    {
        areal = "Река,\nГорное озеро,\nЛесной пруд";
        biting_time = "12:00 — 02:00";
        season << "Осень";
        weather << "Дождь";
        difficulty = "45";
        behavour = "Гладкая";
        size = "25 — 104 см";
        price = 105;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }
    else if (fish_name == "Жуть-рыба")
    {
        areal = "Подводная лодка";
        biting_time = "17:00 — 02:00";
        season << "Зима";
        weather << "Любая";
        difficulty = "60";
        behavour = "Дротиковая";
        size = "20 — 51 см";
        price = 220;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Змеезуб")
    {
        areal = "Река,\nГорное озеро";
        biting_time = "Любое";
        season << "Зима";
        weather << "Любая";
        difficulty = "85";
        behavour = "Гибридная";
        size = "76 — 130 см";
        price = 120;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Кальмар")
    {
        areal = "Океан";
        biting_time = "18:00 — 02:00";
        season << "Зима";
        weather << "Любая";
        difficulty = "75";
        behavour = "Грузиловая";
        size = "30 — 124 см";
        price = 80;
    }
    else if (fish_name == "Камбала")
    {
        areal = "Океан";
        biting_time = "06:00 — 11:00\n19:00 — 02:00";
        season << "Весна" << "Лето" << "Зима";
        weather << "Любая";
        difficulty = "50";
        behavour = "Грузиловая";
        size = "25 — 86 см";
        price = 80;
        roes << "Икра серая" << "Вяленая икра серая";
    }
    else if (fish_name == "Камень-рыба")
    {
        areal = "Шахта 20-ый ур.";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "65";
        behavour = "Грузиловая";
        size = "36 — 41 см";
        price = 300;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Карп")
    {
        areal = "Горное озеро,\nТайный лес,\nЛогово жуков\nмутантов";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "15";
        behavour = "Гибридная";
        size = "38 — 130 см";
        price = 30;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }
    else if (fish_name == "Лавовый угорь")
    {
        areal = "Шахта 100-ый ур.";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "90";
        behavour = "Гибридная";
        size = "79 — 84 см";
        price = 700;
        roes << "Икра красная" << "Вяленая икра красная";
    }
    else if (fish_name == "Ледорыб")
    {
        areal = "Шахта 60-ый ур.";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "85";
        behavour = "Дротиковая";
        size = "18 — 23 см";
        price = 500;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Лесоплюх")
    {
        areal = "Тайный лес,\nЛесная ферма";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гибридная";
        size = "28 — 79 см";
        price = 75;
        roes << "Икра оранжевая" << "Вяленая икра оранжевая";
    }
    else if (fish_name == "Лещ")
    {
        areal = "Река";
        biting_time = "18:00 — 02:00";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "35";
        behavour = "Гладкая";
        size = "30 — 79 см";
        price = 45;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Лосось")
    {
        areal = "Река";
        biting_time = "06:00 — 19:00";
        season << "Осень";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гибридная";
        size = "61 — 168 см";
        price = 75;
        roes << "Икра красная" << "Вяленая икра красная";
    }
    else if (fish_name == "Лосось пустоты")
    {
        areal = "Болото ведьмы";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "80";
        behavour = "Гибридная";
        size = "61 — 168 см";
        price = 150;
        roes << "Икра фиолетовая" << "Вяленая икра фиолетовая";
    }
    else if (fish_name == "Малоротый окунь")
    {
        areal = "Река,\nЛесной пруд";
        biting_time = "Любое";
        season << "Весна" << "Осень";
        weather << "Любая";
        difficulty = "28";
        behavour = "Гибридная";
        size = "30 — 64 см";
        price = 50;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Мелкая камбала")
    {
        areal = "Океан";
        biting_time = "06:00 — 20:00";
        season << "Весна" << "Лето";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гибридная";
        size = "10 — 43 см";
        price = 100;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Морской ёрш")
    {
        areal = "Имбирный остров";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гладкая";
        size = "8 — 33 см";
        price = 100;
        roes << "Икра оранжевая" << "Вяленая икра оранжевая";
    }
    else if (fish_name == "Морской огурец")
    {
        areal = "Океан";
        biting_time = "06:00 — 19:00";
        season << "Осень" << "Зима";
        weather << "Любая";
        difficulty = "40";
        behavour = "Грузиловая";
        size = "8 — 53 см";
        price = 75;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }
    else if (fish_name == "Ночной кальмар")
    {
        areal = "Подводная лодка";
        biting_time = "17:00 — 02:00";
        season << "Зима";
        weather << "Любая";
        difficulty = "55";
        behavour = "Грузиловая";
        size = "20 — 51 см";
        price = 100;
    }
    else if (fish_name == "Окунь")
    {
        areal = "Река,\nГорное озеро,\nЛесной пруд";
        biting_time = "Любое";
        season << "Зима";
        weather << "Любая";
        difficulty = "35";
        behavour = "Дротиковая";
        size = "25 — 64 см";
        price = 55;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }
    else if (fish_name == "Осётр")
    {
        areal = "Горное озеро";
        biting_time = "06:00 — 19:00";
        season << "Лето" << "Зима";
        weather << "Любая";
        difficulty = "78";
        behavour = "Гибридная";
        size = "30 — 155 см";
        price = 200;
        roes << "Осетровая икра" << "Чёрная икра";
    }
    else if (fish_name == "Осьминог")
    {
        areal = "Океан";
        biting_time = "06:00 — 13:00";
        season << "Лето";
        weather << "Любая";
        difficulty = "95";
        behavour = "Грузиловая";
        size = "30 — 124 см";
        price = 150;
        roes << "Икра оранжевая" << "Вяленая икра оранжевая";
    }
    else if (fish_name == "Пятнистый криворот")
    {
        areal = "Шахта";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гибридная";
        size = "25 — 91 см";
        price = 45;
        roes << "Икра белая" << "Вяленая икра белая";
    }
    else if (fish_name == "Песковик")
    {
        areal = "Пустыня";
        biting_time = "06:00 — 20:00";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "65";
        behavour = "Гибридная";
        size = "20 — 64 см";
        price = 75;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }
    else if (fish_name == "Полуночный карп")
    {
        areal = "Горное озеро,\nЛесной пруд";
        biting_time = "22:00 — 02:00";
        season << "Осень" << "Зима";
        weather << "Любая";
        difficulty = "55";
        behavour = "Гибридная";
        size = "30 — 135 см";
        price = 150;
        roes << "Икра фиолетовая" << "Вяленая икра фиолетовая";
    }
    else if (fish_name == "Радужная форель")
    {
        areal = "Река,\nГорное озеро";
        biting_time = "06:00 — 19:00";
        season << "Лето";
        weather << "Ясно";
        difficulty = "45";
        behavour = "Гибридная";
        size = "25 — 66 см";
        price = 65;
        roes << "Икра оранжевая" << "Вяленая икра оранжевая";
    }
    else if (fish_name == "Рыба-ёж")
    {
        areal = "Океан";
        biting_time = "12:00 — 16:00";
        season << "Лето";
        weather << "Ясно";
        difficulty = "80";
        behavour = "Поплавковая";
        size = "3 — 94 см";
        price = 200;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }
    else if (fish_name == "Рыба-пузырь")
    {
        areal = "Подводная лодка";
        biting_time = "17:00 — 02:00";
        season << "Зима";
        weather << "Любая";
        difficulty = "75";
        behavour = "Поплавковая";
        size = "20 — 51 см";
        price = 500;
        roes << "Икра бежевая" << "Вяленая икра бежевая";
    }
    else if (fish_name == "Санфиш")
    {
        areal = "Река";
        biting_time = "06:00 — 19:00";
        season << "Весна" << "Лето";
        weather << "Ясно" << "Ветер";
        difficulty = "30";
        behavour = "Гибридная";
        size = "13 — 41 см";
        price = 30;
        roes << "Икра оранжевая" << "Вяленая икра оранжевая";
    }
    else if (fish_name == "Сардина")
    {
        areal = "Океан";
        biting_time = "06:00 — 19:00";
        season << "Весна" << "Осень" << "Зима";
        weather << "Любая";
        difficulty = "30";
        behavour = "Дротиковая";
        size = "3 — 33 см";
        price = 40;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Сельдь")
    {
        areal = "Океан";
        biting_time = "Любое";
        season << "Весна" << "Зима";
        weather << "Любая";
        difficulty = "25";
        behavour = "Дротиковая";
        size = "20 — 53 см";
        price = 30;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Скат")
    {
        areal = "Пещера пиратов";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "80";
        behavour = "Грузиловая";
        size = "46 — 155 см";
        price = 180;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Скорпионовый карп")
    {
        areal = "Пустыня";
        biting_time = "06:00 — 20:00";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "90";
        behavour = "Дротиковая";
        size = "30 — 84 см";
        price = 150;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Слаймджек")
    {
        areal = "Логово\nжуков-мутантов";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "55";
        behavour = "Дротиковая";
        size = "20 — 66 см";
        price = 100;
        roes << "Икра зелёная" << "Вяленая икра зелёная";
    }
    else if (fish_name == "Сом")
    {
        areal = "Река,\nТайный лес,\nБолото ведьмы";
        biting_time = "06:00 — 00:00";
        season << "Весна" << "Лето" << "Осень";
        weather << "Дождь";
        difficulty = "75";
        behavour = "Гибридная";
        size = "30 — 185 см";
        price = 200;
        roes << "Икра серая" << "Вяленая икра серая";
    }
    else if (fish_name == "Сомик")
    {
        areal = "Горное озеро";
        biting_time = "Любое";
        season << "Все сезоны";
        weather << "Любая";
        difficulty = "46";
        behavour = "Гладкая";
        size = "30 — 79 см";
        price = 75;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Тигровая форель")
    {
        areal = "Река";
        biting_time = "06:00 — 19:00";
        season << "Осень" << "Зима";
        weather << "Любая";
        difficulty = "60";
        behavour = "Дротиковая";
        size = "25 — 53 см";
        price = 150;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Тиляпия")
    {
        areal = "Океан";
        biting_time = "06:00 — 14:00";
        season << "Лето" << "Осень";
        weather << "Любая";
        difficulty = "50";
        behavour = "Гибридная";
        size = "28 — 79 см";
        price = 75;
        roes << "Икра серая" << "Вяленая икра серая";
    }
    else if (fish_name == "Тунец")
    {
        areal = "Океан";
        biting_time = "06:00 — 19:00";
        season << "Лето" << "Зима";
        weather << "Любая";
        difficulty = "70";
        behavour = "Гладкая";
        size = "30 — 155 см";
        price = 100;
        roes << "Икра синяя" << "Вяленая икра синяя";
    }
    else if (fish_name == "Угорь")
    {
        areal = "Океан";
        biting_time = "16:00 — 02:00";
        season << "Весна" << "Осень";
        weather << "Дождь";
        difficulty = "70";
        behavour = "Гладкая";
        size = "30 — 206 см";
        price = 85;
        roes << "Икра коричневая" << "Вяленая икра коричневая";
    }
    else if (fish_name == "Чудо-огурец")
    {
        areal = "Океан";
        biting_time = "18:00 — 02:00";
        season << "Лето" << "Осень";
        weather << "Любая";
        difficulty = "80";
        behavour = "Грузиловая";
        size = "30 — 94 см";
        price = 250;
        roes << "Икра фиолетовая" << "Вяленая икра фиолетовая";
    }
    else if (fish_name == "Шэд")
    {
        areal = "Океан";
        biting_time = "09:00 — 02:00";
        season << "Весна" << "Лето" << "Осень";
        weather << "Дождь";
        difficulty = "45";
        behavour = "Гладкая";
        size = "51 — 124 см";
        price = 60;
        roes << "Икра зелёная" << "Вяленая икра зелёная";
    }
    else if (fish_name == "Щука")
    {
        areal = "Река,\nЛесной пруд";
        biting_time = "Любое";
        season << "Лето" << "Зима";
        weather << "Любая";
        difficulty = "60";
        behavour = "Дротиковая";
        size = "38 — 155 см";
        price = 100;
        roes << "Икра жёлтая" << "Вяленая икра жёлтая";
    }

    ui->fish_name->setText(fish_name);
    ui->fish_picture->setPixmap(QPixmap(":/res/resourses/" + image_path + ".png").
                                scaled(100 * y, 100 * y, Qt::KeepAspectRatio));
    ui->areal_info->setText(areal);
    ui->time_info->setText(biting_time);
    setSeason(ui->fish_seasons, season);
    setWeather(ui->fish_weather, weather);
    ui->difficulty->setText(difficulty);
    ui->behavour_info->setText(behavour);
    ui->size_info->setText(size);
    ui->fish_priceWidget->setItem(image_path, price);
    ui->fish_productList->clear();
    if (roes.isEmpty())
        ui->fish_products->close();
    else
    {
        ui->fish_products->show();
        ui->fish_productList->setItems(roes, price);
    }
    ui->stackedWidget->setCurrentWidget(ui->fish_info);
}

void MainWindow::setWeather(QLabel *label, const QStringList &report)
{
    if (report.at(0) == "Любая")
        label->setText("Любая");
    else
    {
        QString str;
        for (QString weather : report)
        {
            const QString icon = weather == "Ясно" ? "Sunny" : weather == "Дождь" ? "Rain" : "Wind";
            str += image(icon, 36) + "  " + weather + "<br>";
        }
        str.chop(4);
        ui->fish_weather->setText(str);
    }
}