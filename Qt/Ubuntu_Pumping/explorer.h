#ifndef EXPLORER_H
#define EXPLORER_H

#include <QDialog>
#include <QDir>

namespace Ui {
class Explorer;
}

class Explorer : public QDialog
{
    Q_OBJECT

public:
    explicit Explorer(QWidget *parent = nullptr);
    void settings(const QString&);
    ~Explorer();

private:
    QString mode;
    Ui::Explorer *ui;
    QFileInfoList dirList;

public slots:
    void accepting();

private slots:
    void dirContent(const QString&);
    void openFolder(const QModelIndex&);
    void selectFile(const QModelIndex&);

signals:
    void importing(const QString&);
    void exporting(const QString&);
};

#endif // EXPLORER_H
