#include "explorer.h"
#include "ui_explorer.h"
#include <QDebug>

Explorer::Explorer(QWidget *parent) : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint |
                                              Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint), ui(new Ui::Explorer)
{
    ui->setupUi(this);
    connect(ui->ok_button, &QPushButton::clicked, this, &Explorer::accepting);
    connect(ui->cancel_button, &QPushButton::clicked, this, &QWidget::close);
    connect(ui->path, &QLineEdit::textEdited, this, &Explorer::dirContent);

    ui->browser->setIconSize(QSize(46, 46));
    connect(ui->browser, &QListWidget::doubleClicked, this, &Explorer::openFolder);
    connect(ui->browser, &QListWidget::clicked, this, &Explorer::selectFile);
}

void Explorer::settings(const QString &value)
{
    show();
    ui->fileName->clear();
    ui->ok_button->setFocus();
    mode = value;
    dirContent("/");
    ui->ok_button->setText(value);
}

void Explorer::dirContent(const QString &path)
{
    QDir dir = QDir(path);
    QDir::Filters flag;

    if (path == "/")
        flag = QDir::NoDotAndDotDot;
    else
        flag = QDir::NoDot;
    dirList = dir.entryInfoList(QStringList("*.pum"), flag | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
    ui->browser->clear();
    ui->path->setText(path);
    for (int i = 0; i < dirList.size(); i++)
    {
        QIcon icon;
        if (dirList.at(i).isDir())
            icon = QIcon(":/exp/folder.png");
        else
            icon = QIcon(":/exp/file.png");
        ui->browser->addItem(new QListWidgetItem(icon, dirList.at(i).fileName()));
        ui->browser->item(i)->setSizeHint(QSize(1, 52));
    }
}

void Explorer::openFolder(const QModelIndex &index)
{
    if (dirList.at(index.row()).isDir())
        dirContent(dirList.at(index.row()).absoluteFilePath());
}

void Explorer::selectFile(const QModelIndex &index)
{
    if (dirList.at(index.row()).isFile())
    {
        ui->path->setText(dirList.at(index.row()).absolutePath());
        ui->fileName->setText(dirList.at(index.row()).fileName());
    }
}

void Explorer::accepting()
{
    QString path = ui->path->text() + "/" + ui->fileName->text();
    if (!path.contains(".pum"))
        path += ".pum";
    if (QFileInfo(path).isFile() && mode == "Открыть")
        emit importing(path);
    else if (mode == "Сохранить")
        emit exporting(path);
    close();
}

Explorer::~Explorer()
{
    delete ui;
}
