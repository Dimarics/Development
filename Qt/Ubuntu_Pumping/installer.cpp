#include "installer.h"
#include "ui_installer.h"

#include <QTextStream>
#include <QDebug>

Installer::Installer(QWidget *parent) : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint |
                                                Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint), ui(new Ui::Installer)
{
    ui->setupUi(this);
    process = new QProcess(this);
    setAttribute(Qt::WA_DeleteOnClose);
    connect(process, &QProcess::readyReadStandardOutput, this, &Installer::readOutput);
    connect(process, &QProcess::readyReadStandardError, this, &Installer::processError);
    connect(ui->cancelButton, &QPushButton::clicked, this, &QObject::deleteLater);
}

void Installer::setCommandList(const QString &password, QStringList &commandList)
{
    process->start("bash");
    process->waitForStarted();
    process->write("sudo -S su\n");
    process->write(password.toUtf8() + "\n");
    size = commandList.size();
    ui->progressBar->setMaximum(size);
    for (int i = 0; i < size; i++)
    {
        process->write(commandList.at(i).toUtf8() + "\n");
        process->write("echo Command was executed\n");
    }
}

void Installer::readOutput()
{
    QTextStream out(stdout);
    //out.setCodec("UTF-8");
    QString str = process->readAllStandardOutput();
    if (str == "Command was executed\n")
    {
        ++count;
        ui->progressBar->setValue(ui->progressBar->value() + 1);
        if (count == size)
            emit installFinished();
    }
    else
    {
        out << str;
        ui->textBrowser->setPlainText(str);
    }
}

void Installer::processError()
{
    QTextStream out(stdout);
    //out.setCodec("UTF-8");
    QString str = process->readAllStandardError();
    out << str;
    ui->textBrowser->setHtml("<font color=\"red\">" + str + "</h1>");
}

Installer::~Installer()
{
    delete ui;
}
