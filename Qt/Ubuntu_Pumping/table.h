#ifndef TABLE_H
#define TABLE_H

#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QWheelEvent>
#include <QCheckBox>
#include <QMenu>
#include <QAction>
#include <QStyledItemDelegate>

class Table : public QTableWidget
{
public:
    Table(QWidget *parent = 0);
    QCheckBox *selector;
    QMenu *option;

protected:
    void focusOutEvent(QFocusEvent *event);
    void wheelEvent(QWheelEvent *event);

private slots:
    void contextMenu(const QPoint&);
};

class NoSelectDelegate : public QStyledItemDelegate
{
protected:
    void paint(QPainter* painter, const QStyleOptionViewItem &option, const QModelIndex& index) const;
};

#endif // TABLE_H
