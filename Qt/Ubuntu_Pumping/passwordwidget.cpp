#include "passwordwidget.h"
#include "ui_passwordwidget.h"
//#include <QTextStream>
//#include <QDebug>
PasswordWidget::PasswordWidget(QWidget *parent) : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint |
                                                          Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint), ui(new Ui::PasswordWidget)
{
    ui->setupUi(this);
    ui->errorMessage->close();
    setAttribute(Qt::WA_DeleteOnClose);
    connect(ui->showBtn, &QPushButton::toggled, this, &PasswordWidget::passwordVision);
    connect(ui->lineEdit, &QLineEdit::returnPressed, this, &PasswordWidget::enterPassword);
    connect(ui->lineEdit, &QLineEdit::textChanged, this, &PasswordWidget::hideError);
    passwordVision(false);

    process = new QProcess(this);
    process->start("bash");
    process->waitForStarted();
    process->write("sudo -S ls\n");
    //process->write("echo 123 | sudo -S su\n");
    connect(process, &QProcess::readyReadStandardError, this, &PasswordWidget::passwordError);
    connect(process, &QProcess::readyReadStandardOutput, this, &PasswordWidget::acceptPassword);
    //connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &PasswordWidget::acceptPassword);
}

void PasswordWidget::passwordVision(bool show)
{
    ui->lineEdit->setFocus();
    if (show)
        ui->lineEdit->setEchoMode(QLineEdit::Normal);
    else
        ui->lineEdit->setEchoMode(QLineEdit::Password);
}

void PasswordWidget::enterPassword()
{
    if (ui->errorMessage->isVisible())
        ui->errorMessage->close();
    process->write(ui->lineEdit->text().toUtf8()+"\n");
}

void PasswordWidget::passwordError()
{
    if (!reset)
        reset = true;
    else
    {
        ++count;
        reset = false;
        if (count == 3)
        {
            count = 0;
            process->write("sudo -S ls\n");
        }
        ui->errorMessage->show();
    }
    /*QTextStream out(stdout);
    out.setCodec("UTF-8");
    out << process->readAllStandardError();*/
}

void PasswordWidget::acceptPassword()
{
    /*QTextStream out(stdout);
    out.setCodec("UTF-8");
    out << process->readAllStandardOutput();*/
    emit passwordAccepted(ui->lineEdit->text());
    //ui->lineEdit->clear();
    close();
}

void PasswordWidget::hideError()
{
    if (ui->errorMessage->isVisible())
        ui->errorMessage->close();
}

PasswordWidget::~PasswordWidget()
{
    delete ui;
}
