#ifndef EDITOR_H
#define EDITOR_H

#include <QDialog>

namespace Ui {
class Editor;
}

class Editor : public QDialog
{
    Q_OBJECT

public:
    explicit Editor(QWidget *parent = nullptr);
    void getData(const QStringList&);
    ~Editor();

private:
    Ui::Editor *ui;
    QString status;

private slots:
    void add_command();
    void delete_command();
    void up_command();
    void down_command();
    void endEdit();

signals:
    void returnData(const QStringList&);
};

#endif // EDITOR_H
