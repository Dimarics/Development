#include "window.h"
#include "ui_window.h"
#include "file.cpp"
#include "passwordwidget.h"
#include "installer.h"

#include <QDebug>

Window::Window(QMainWindow *parent) : QMainWindow(parent), ui(new Ui::Window)
{
    ui->setupUi(this);
    connect(ui->add_button, &QPushButton::clicked, this, &Window::addPressed);
    connect(ui->install_button, &QPushButton::clicked, this, &Window::installPressed);
    connect(ui->open, &QAction::triggered, this, &Window::import);
    connect(ui->save, &QAction::triggered, this, &Window::save);
    connect(ui->saveAs, &QAction::triggered, this, &Window::saveAs);
    connect(ui->startSize, &QAction::triggered, this, &Window::defaultWindowSize);
    connect(ui->deleteSelected, &QAction::triggered, this, &Window::deleteSelected);

    ui->table->horizontalHeader()->resizeSection(0, 320);
    ui->table->horizontalHeader()->resizeSection(1, 520);
    ui->table->option->addAction("Редактировать", this, &Window::openEditor);
    ui->table->option->addAction("Удалить", this, &Window::deleteContent);
    connect(ui->table->selector, &QCheckBox::stateChanged, this, &Window::checkState);
    connect(ui->table, &QTableWidget::cellDoubleClicked, this, &Window::openEditor);

    QFileInfo check_file("data.pum");
    if (check_file.exists())
        database = read_file("data.pum");
    fillTable();

    editor = new Editor(this);
    connect(editor, &Editor::returnData, this, &Window::addProgram);

    explorer = new Explorer(this);
    connect(explorer, &Explorer::importing, this, &Window::importFile);
    connect(explorer, &Explorer::exporting, this, &Window::exportFile);
}

void Window::fillTable()
{
    ui->table->clearContents();
    int size = database.size();
    if (size > 18)
    {
        ui->table->setRowCount(size);
        ui->table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    }
    else
    {
        ui->table->setRowCount(18);
        ui->table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
    std::sort(database.begin(), database.end(), compare);
    for (int row = 0; row < size; row++)
    {
        QCheckBox *checkbox = new QCheckBox(database.at(row).at(0));
        checkbox->setStyleSheet("color: rgb(20, 70, 70); font-size: 12pt");
        ui->table->setCellWidget(row, 0, checkbox);
        ui->table->setItem(row, 1, new QTableWidgetItem(database.at(row).at(1)));
        ui->table->setItem(row, 2, new QTableWidgetItem(database.at(row).at(2)));
    }
}

bool Window::compare(const QStringList &list_1, const QStringList &list_2)
{
    return list_1.at(0).toLower() < list_2.at(0).toLower();
}

void Window::addPressed()
{
    row = -1;
    editor->getData(QStringList());
}

void Window::openEditor()
{
    int currow = ui->table->currentRow();
    if (currow < database.size())
    {
        row = currow;
        editor->getData(database.at(row));
    }
    else
    {
        row = -1;
        editor->getData(QStringList());
    }
}

void Window::addProgram(const QStringList &data)
{
    if (row == -1)
        database << data;
    else
        database[row] = data;
    ui->table->clearContents();
    fillTable();
    write_file("data.pum", database);
}

void Window::deleteContent()
{
    int currow = ui->table->currentRow();
    if (currow < database.size() && currow > -1)
    {
        database.removeAt(currow);
        fillTable();
        write_file("data.pum", database);
    }
}

void Window::deleteSelected()
{
    for (int i = database.size() - 1; i >= 0; i--)
        if (((QCheckBox*)ui->table->cellWidget(i, 0))->checkState() == Qt::Checked)
            database.removeAt(i);
    fillTable();
    write_file("data.pum", database);
}

void Window::checkState(int value)
{
    for (int i = 0; i < database.size(); i++)
        ((QCheckBox*)ui->table->cellWidget(i, 0))->setCheckState((Qt::CheckState)value);
}

void Window::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Delete)
        deleteContent();
}

void Window::import()
{
    explorer->settings("Открыть");
}

void Window::importFile(const QString &path)
{
    database = read_file(path);
    fillTable();
    write_file("data.pum", database);
    savepath = path;
}

void Window::save()
{
    write_file(savepath, database);
}

void Window::saveAs()
{
    explorer->settings("Сохранить");
}

void Window::exportFile(const QString &path)
{
    write_file(path, database);
    write_file("data.pum", database);
    savepath = path;
}

void Window::defaultWindowSize()
{
    resize(1300, 812);
}

void Window::installPressed()
{
    PasswordWidget *passwordWidget = new PasswordWidget(this);
    connect(passwordWidget, &PasswordWidget::passwordAccepted, this, &Window::startInstall);
    passwordWidget->show();
}

void Window::startInstall(const QString &password)
{
    QStringList commandList;
    Installer *installer = new Installer(this);
    connect(installer, &Installer::installFinished, this, &Window::setStatus);
    installer->show();
    for (int i = 0; i < database.size(); i++)
    {
        if (((QCheckBox*)ui->table->cellWidget(i, 0))->checkState() == Qt::Checked)
        {
            database[i][2] = "Установлено";
            for (int j = 3; j < database.at(i).size(); j++)
            {
                commandList << database.at(i).at(j);
            }
        }
    }
    installer->setCommandList(password, commandList);
}

void Window::setStatus()
{
    fillTable();
    ui->table->selector->setCheckState(Qt::Unchecked);
}

Window::~Window()
{
    delete ui;
}
