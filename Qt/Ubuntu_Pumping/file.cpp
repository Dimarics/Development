#include <QFile>
#include <QTextStream>

QList<QStringList> read_file(const QString &path)
{
    QFile file(path);
    QList<QStringList> data {QStringList(QString())};

    if (file.open(QIODevice::ReadOnly))
    {
        //QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
        //QString text = codec->toUnicode(file.readAll());
        //text = QString::fromUtf8(file.readAll());
        QString text = file.readAll();
        int row = 0;
        int column = 0;

        for (QChar &c : text)
        {
            if (c == '~')
            {
                data[row] << QString();
                ++column;
            }
            else if (c == '\n')
            {
                ++row;
                data << QStringList(QString());
                column = 0;
            }
            else
                data[row][column] += c;
        }
    }
    return data;
}

void write_file(const QString &path, const QList<QStringList> &data)
{
    QFile file(path);
    QTextStream out(&file);
    //out.setCodec("UTF-8");
    //out.setGenerateByteOrderMark(false);
    if (file.open(QIODevice::WriteOnly))
    {
        for (int i = 0; i < data.size(); i++)
        {
            for (int j = 0; j < data.at(i).size(); j++)
            {
                out << data.at(i).at(j);
                if (j != data.at(i).size() - 1)
                    out << "~";
            }
            if (i != data.size() - 1)
                out << "\n";
        }
    }
}
