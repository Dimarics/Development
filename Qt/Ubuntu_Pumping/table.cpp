#include "table.h"
#include <QPainter>

const char *const tableHeader =
        "QHeaderView::section {"
        "background-color: rgb(70, 210, 155);"
        "color: white;"
        "font: 14pt;"
        "border: none;"
        "border-right: 1px solid white;"
        "border-left: 1px solid white;"
        "border-bottom: 1px solid white;"
        "}";

const char *const scrollBarVertical =
        "QScrollBar::vertical {"
        "background-color: rgb(233, 247, 238);"
        "border: none;"
        "margin: 19px 0 19px 0;"
        "}"
        "QScrollBar::handle:vertical {"
        "background-color: rgb(120, 230, 170);"
        "}"
        "QScrollBar::sub-line:vertical {"
        "background-color: rgb(233, 247, 238);"
        "height: 19px;"
        "subcontrol-position: top;"
        "subcontrol-origin: margin;"
        "}"
        "QScrollBar::add-line:vertical {"
        "background-color: rgb(233, 247, 238);"
        "height: 19px;"
        "subcontrol-position: bottom;"
        "subcontrol-origin: margin;"
        "}"
        "QScrollBar::handle:vertical:hover, QScrollBar::sub-line:vertical:hover, QScrollBar::add-line:vertical:hover {"
        "background-color: rgb(100, 200, 160);"
        "}"
        "QScrollBar::handle:vertical:pressed, QScrollBar::sub-line:vertical:pressed, QScrollBar::add-line:vertical:pressed {"
        "background-color: rgb(50, 180, 140);"
        "}"
        "QScrollBar::up-arrow:vertical {"
        "image: url(:/res/up_arrow.png);"
        "}"
        "QScrollBar::down-arrow:vertical {"
        "image: url(:/res/down_arrow.png);"
        "}"
        "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {"
        "background: none;"
        "}";

const char *const scrollBarHorizontal =
        "QScrollBar::horizontal {"
        "background-color: rgb(233, 247, 238);"
        "border: none;"
        "margin: 0 19px 0 19px;"
        "}"
        "QScrollBar::handle:horizontal {"
        "background-color: rgb(120, 230, 170);"
        "}"
        "QScrollBar::sub-line:horizontal {"
        "background-color: rgb(233, 247, 238);"
        "width: 19px;"
        "subcontrol-position: left;"
        "subcontrol-origin: margin;"
        "}"
        "QScrollBar::add-line:horizontal {"
        "background-color: rgb(233, 247, 238);"
        "width: 19px;"
        "subcontrol-position: right;"
        "subcontrol-origin: margin;"
        "}"
        "QScrollBar::handle:horizontal:hover, QScrollBar::sub-line:horizontal:hover, QScrollBar::add-line:horizontal:hover {"
        "background-color: rgb(100, 200, 160);"
        "}"
        "QScrollBar::handle:horizontal:pressed, QScrollBar::sub-line:horizontal:pressed, QScrollBar::add-line:horizontal:pressed {"
        "background-color: rgb(50, 180, 140);"
        "}"
        "QScrollBar::left-arrow:horizontal {"
        "image: url(:/res/left_arrow.png);"
        "}"
        "QScrollBar::right-arrow:horizontal {"
        "image: url(:/res/right_arrow.png);"
        "}"
        "QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {"
        "background: none;"
        "}";

Table::Table(QWidget *parent) : QTableWidget(parent)
{
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    horizontalHeader()->setStyleSheet(tableHeader);
    horizontalHeader()->setFixedHeight(34);
    horizontalHeader()->setSectionsClickable(false);
    verticalScrollBar()->setStyleSheet(scrollBarVertical);
    horizontalScrollBar()->setStyleSheet(scrollBarHorizontal);
    setItemDelegate(new NoSelectDelegate);

    selector = new QCheckBox(this);
    selector->setGeometry(10, 11, 17, 17);

    option = new QMenu(this);
    option->setStyleSheet("font-size: 12pt");
    connect(this, &QWidget::customContextMenuRequested, this, &Table::contextMenu);
}

void Table::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    clearSelection();
}

void Table::wheelEvent(QWheelEvent* event)
{
    int step;
    event->angleDelta().y() > 0 ? step = -1 : step = 1;
    verticalScrollBar()->setValue(verticalScrollBar()->value() + step);
}

void Table::contextMenu(const QPoint &pos)
{
    option->popup(viewport()->mapToGlobal(pos));
}

void NoSelectDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const
{
    QStyleOptionViewItem itemOption(option);
    if (itemOption.state & QStyle::State_HasFocus)
        itemOption.state = itemOption.state ^ QStyle::State_HasFocus;
    QStyledItemDelegate::paint(painter, itemOption, index);
}
