#ifndef WIDGET_H
#define WIDGET_H

#include <QMainWindow>
#include <QCheckBox>
#include <QMenu>
#include <QAction>
#include <algorithm>
#include <QKeyEvent>

#include "table.h"
#include "editor.h"
#include "explorer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QMainWindow *parent = nullptr);
    ~Window();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    int row;
    Ui::Window *ui;
    Table *table;
    Editor *editor;
    Explorer *explorer;
    QList<QStringList> database;
    QString savepath;
    void fillTable();
    static bool compare(const QStringList&, const QStringList&);

private slots:
    void addPressed();
    void openEditor();
    void addProgram(const QStringList&);
    void deleteContent();
    void deleteSelected();
    void checkState(int);
    void import();
    void importFile(const QString&);
    void save();
    void saveAs();
    void exportFile(const QString&);
    void defaultWindowSize();
    void installPressed();
    void startInstall(const QString&);
    void setStatus();
};
#endif // WIDGET_H
